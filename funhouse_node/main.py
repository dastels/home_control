# -*- mode: python -*-

# The MIT License (MIT)

# Copyright (c) 2021 Dave Astels

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import time
import sys
import gc
import json
import board
import adafruit_logging as logging
from screen_handler import ScreenHandler
from digitalio import DigitalInOut, Direction, Pull
from adafruit_debouncer import Debouncer
import displayio
import terminalio
from adafruit_bitmap_font import bitmap_font
from adafruit_display_text import label
import adafruit_dps310
import adafruit_ahtx0
from adafruit_funhouse import FunHouse
from hue_bridge import Bridge
from group import Group
from light import Light
from light_meter import measure_light
from control_state_machine import ControlStateMachine

# get configuration information
try:
    from config import config
except ImportError:
    raise

# get secret settings
try:
    from secrets import secrets
except ImportError:
    raise

logger = logging.getLogger("RoomNode")

funhouse = FunHouse()
funhouse.peripherals.dotstars.fill(0x000000)

# ===============================================================================
# Handle mode switches

# config mode switch
if config["config_mode"] or funhouse.peripherals.button_down:
    import config_mode

    config_mode.run_config(config, funhouse)

    # ===============================================================================
# Setup the display

display = board.DISPLAY

funhouse.display.show(None)
logging_message_label = funhouse.add_text(
    text_position=(0, 40),
    text_font=terminalio.FONT,
    text=(" " * 35),
    text_color=0xFFFFFF,
)
time_label = funhouse.add_text(
    text_position=(120, 80),
    text_anchor_point=(0.5, 0.5),
    text_color=0xFFFFFF,
    text_font="fonts/Arial-Bold-24.pcf",
)
temp_label = funhouse.add_text(
    text_position=(120, 120),
    text_anchor_point=(0.5, 0.5),
    text_color=0xFFFF00,
    text_font="fonts/Arial-Bold-24.pcf",
)
hum_label = funhouse.add_text(
    text_position=(120, 160),
    text_anchor_point=(0.5, 0.5),
    text_color=0xFFFF00,
    text_font="fonts/Arial-Bold-24.pcf",
)
pres_label = funhouse.add_text(
    text_position=(120, 200),
    text_anchor_point=(0.5, 0.5),
    text_color=0xFFFF00,
    text_font="fonts/Arial-Bold-24.pcf",
)
funhouse.display.show(funhouse.splash)

# debug mode switch
debug_mode = config["debug_mode"] or funhouse.peripherals.button_up
if debug_mode:
    logger.setLevel(logging.DEBUG)
    funhouse._debug = True
else:
    logger.setLevel(logging.ERROR)
    logger.addHandler(ScreenHandler(logging_message_label))

if funhouse.peripherals._buttons[1].value:
    measure_light(funhouse, hum_label)  # never returns

funhouse.network.connect()
wifi = funhouse.network._wifi

# ===============================================================================
# Set up buttons
#
# With no buttons pushed during startup we're in production/deployed mode:
#   loggering level is ERROR
#   logging to screen
#
# With UP held during startup we're in debug mode:
#   DEBUG level logging
#   logger output to serial

down_button = Debouncer(funhouse.peripherals._buttons[0])
select_button = Debouncer(funhouse.peripherals._buttons[1])
up_button = Debouncer(funhouse.peripherals._buttons[2])

logger.debug("***** Free mem: %d", gc.mem_free())

i2c = board.I2C()
dps310 = adafruit_dps310.DPS310(i2c)
aht20 = adafruit_ahtx0.AHTx0(i2c)

funhouse.peripherals.dotstars.fill(0x000000)

# Attempt to connect to the bridge
try:
    username = secrets["hue_username"]
    bridge_ip = secrets["bridge_ip"]
    hue_bridge = Bridge(wifi, bridge_ip, username)
except:
    # Perform first-time bridge setup
    hue_bridge = Bridge(wifi)
    ip = hue_bridge.discover_bridge()
    print(
        "Attempting to register username, press the link button on your Hue Bridge now!"
    )
    username = hue_bridge.register_username()
    print(
        'ADD THESE VALUES TO SECRETS.PY: \
                   \n\t"bridge_ip":"{0}", \
                   \n\t"hue_username":"{1}"'.format(
            ip, username
        )
    )
    raise

group_json = hue_bridge.get_groups()
groups = [
    Group(
        int(number), data["name"], [Light(hue_bridge, int(l)) for l in data["lights"]]
    )
    for (number, data) in group_json.items()
    if data["name"] == config["group_name"]
]
if len(groups) > 0:
    hue_group = groups[0]
else:
    logger.critical("No group named '{0}'".format(config["group_name"]))
    sys.exit()


# wrap the PIR in a debouncer to detect the PIR signal edges...
# it's binary already so doesn't need a long interval

pir = Debouncer(funhouse.peripherals._pir, interval=0.001)

# ===============================================================================
# WiFi/MQTT support

# pylint: disable=unused-argument
# def connected(client):
#     logger.debug("Connected to Adafruit IO! Subscribing...")


# def subscribe(client, userdata, topic, granted_qos):
#     logger.debug("Subscribed to {0} with QOS level {1}".format(topic, granted_qos))


# def disconnected(client):
#     logger.debug("Disconnected from Adafruit IO!")


# def message(client, feed_id, payload):
#     logger.debug("Feed {0} received new value: {1}".format(feed_id, payload))


# pylint: enable=unused-argument

# Initialize a new MQTT Client object
# funhouse.network.init_io_mqtt()
# funhouse.network.on_mqtt_connect = connected
# funhouse.network.on_mqtt_disconnect = disconnected
# funhouse.network.on_mqtt_subscribe = subscribe
# funhouse.network.on_mqtt_message = message

# funhouse.network._mqtt_client._client.enable_logger(logging, logging.DEBUG)


# logger.debug("Connecting to Adafruit IO...")
# funhouse.network.mqtt_connect()


def update(now, fetch_time):
    # try:
    #     funhouse.network.mqtt_loop()
    # except OSError as ex:
    #     logger.error("MQTT Loop: %s", str(ex))
    pir.update()
    up_button.update()
    select_button.update()
    down_button.update()
    if now >= fetch_time:
        funhouse.get_local_time(secrets["timezone"])
        return now + config["time_refresh_interval"]
    return fetch_time


def update_environment_display():
    global environment

    temp = funhouse.peripherals.temperature
    unit = "C"
    if config["use_fahrenheit"]:
        temp = temp * (9 / 5) + 32
        unit = "F"

    environment["temperature"] = temp
    environment["pressure"] = funhouse.peripherals.pressure
    environment["humidity"] = funhouse.peripherals.relative_humidity

    funhouse.set_text("{:.1f}{}".format(environment["temperature"], unit), temp_label)
    funhouse.set_text("{:.1f}%".format(environment["humidity"]), hum_label)
    funhouse.set_text("{:.1f}kPa".format(environment["pressure"]), pres_label)


environment = {}
fetch_time = 0
gc_report_time = 0
environment_update_time = 0
time_update_time = 0
machine = ControlStateMachine(funhouse, hue_group)
tick_time = 0
TICK_INTERVAL = 1.00

while True:
    # Get iteration values
    now = time.monotonic()

    fetch_time = update(now, fetch_time)

    # ----------------------------------------------------------------
    # handle motion sensor

    if pir.rose:
        machine.pir_rise()

    if pir.fell:  # motion ended
        machine.pir_fall()

    # ----------------------------------------------------------------
    # send the machine a time update periodically

    if now >= tick_time:
        tick_time = now + TICK_INTERVAL
        machine.tick(now)

    # ----------------------------------------------------------------
    # update environment display periodically

    if not environment or (now >= environment_update_time):
        update_environment_display()
        environment_update_time = now + config["environment_interval"]

    # ----------------------------------------------------------------
    # update time display every minute

    if now >= time_update_time:
        time_update_time = now + 60
        tm = time.localtime()
        funhouse.set_text("{:02d}:{:02d}".format(tm.tm_hour, tm.tm_min), time_label)

    # ----------------------------------------------------------------
    # periodicly report free mem

    if now > gc_report_time:
        gc_report_time = now + 60
        logger.debug("***** Free mem: %d", gc.mem_free())
