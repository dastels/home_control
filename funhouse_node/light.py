# -*- mode: python -*-

# The MIT License (MIT)

# Copyright (c) 2021 Dave Astels

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import json

import adafruit_logging as logging

logger = logging.getLogger("RoomNode")


class Light:
    def __init__(self, bridge, number):
        data = bridge.get_light(number)
        self._bridge = bridge
        self._number = number
        self._type = data["type"]
        self._name = data["name"]
        self._coloured = "color" in self._type.lower()

    @property
    def name(self):
        return self._name

    @property
    def number(self):
        return self._number

    @property
    def type(self):
        return self._type

    @property
    def is_on(self):
        json = self._bridge.get_light(self._number)
        reachable = json["state"]["reachable"]
        state = json["state"]["on"]
        logger.debug(
            "Light %d is %s",
            self._number,
            ("on" if state else "off") if reachable else "offline",
        )
        return state if reachable else None

    @property
    def is_reachable(self):
        json = self._bridge.get_light(self._number)
        return json["state"]["reachable"]

    def turn_on(self, brightness=100, colour=0xFFFFFF):
        result = None
        if self.is_reachable:
            if self._coloured:
                if isinstance(colour, int):
                    colour = [(colour >> x) & 0xFF for x in [16, 8, 0]]
                logger.debug(
                    "Setting light %d to RGB (%d %d %d)", self._number, *colour
                )
                hsb = self._bridge.rgb_to_hsb([(c * brightness) // 100 for c in colour])
                logger.debug("Setting light %d to HSB (%d %d %d)", self._number, *hsb)
                result = self._bridge.set_light(
                    self._number, on=True, hue=hsb[0], sat=hsb[1], bri=hsb[2]
                )
            else:
                logger.debug("Setting light %d to %d", self._number, brightness)
                result = self._bridge.set_light(
                    self._number, on=True, bri=int(brightness * 2.54)
                )
            return "success" in result[0]
        else:
            return False

    def turn_off(self):
        if self.is_reachable:
            result = self._bridge.set_light(self._number, on=False)
            return "success" in result[0]
        else:
            return False
