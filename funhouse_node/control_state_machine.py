# -*- mode: python -*-

# The MIT License (MIT)

# Copyright (c) 2021 Dave Astels

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

# The machine that controls regular operation

from state_machine import StateMachine
import adafruit_logging as logging
from idle_state import IdleState
from lights_on_state import LightsOnState
from lights_off_timer_state import LightsOffTimerState

logger = logging.getLogger("RoomNode")


class ControlStateMachine(StateMachine):
    def __init__(self, funhouse, group):
        super().__init__(funhouse)
        IdleState(funhouse, self, group)
        LightsOnState(funhouse, self, group)
        LightsOffTimerState(funhouse, self)

        self.goto("idle")

    def pir_rise(self):
        """Handle a rising PIR signal"""
        self._funhouse.peripherals.dotstars.fill(0x000044)
        super().pir_rise()

    def pir_fall(self):
        """Handle a falling PIR signal"""
        self._funhouse.peripherals.dotstars.fill(0x000000)
        super().pir_fall()
