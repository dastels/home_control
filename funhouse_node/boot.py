"""CircuitPython Essentials Storage logging boot.py file"""
import board
import digitalio
import storage

try:
    from config import config
except ImportError:
    raise

# For Gemma M0, Trinket M0, Metro M0/M4 Express, ItsyBitsy M0/M4 Express
switch = digitalio.DigitalInOut(board.BUTTON_DOWN)

# For Feather M0/M4 Express
# switch = digitalio.DigitalInOut(board.D5)

# For Circuit Playground Express, Circuit Playground Bluefruit
# switch = digitalio.DigitalInOut(board.D7)

switch.direction = digitalio.Direction.INPUT
switch.pull = digitalio.Pull.DOWN
read_write = config["config_mode"] or switch.value
print("Mounting {0}".format("read-write" if read_write else "readonly"))

# If config mode is set or the down switch is pressed, CircuitPython can write to the drive
storage.remount("/", not read_write)
