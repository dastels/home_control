# -*- mode: python -*-

# The MIT License (MIT)

# Copyright (c) 2021 Dave Astels

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

# Navigate and select from a menu
import adafruit_logging as logging
from state import State

logger = logging.getLogger("RoomNode")


class MenuState(State):
    def __init__(self, funhouse, machine, name, items, lines, initial=0):
        """Set up a menu
        :param Funhouse funhouse: a funhouse object
        "param StateMachine machine: the owning state machine
        :param string name: the name of the state
        :param list[(string, string)] items: (item text, target state name) pairs
        :param list(Label) lines: the displayio Labels to display items in
        :param int initial: the initially highlighted item
        """
        super().__init__(funhouse, machine, name, lines)
        self._items = items
        self._index = initial
        self._top = max([0, initial - len(lines)])

    def up(self):
        """Handle the up button being pressed"""
        if self._index > 0:
            if self._index == self._top:
                self._top -= 1
            self._index -= 1
            self.update_display()

    def down(self):
        """Handle the down button being pressed"""
        if self._index < len(self._items) - 1:
            if self._index == (self._top + len(self._lines)) - 1:
                self._top += 1
            self._index += 1
            self.update_display()

    def select(self):
        """Handle the select button being pressed"""
        self.goto(self._items[self._index][1])

    def enter(self, config):
        """Perform any entry tasks, usually pulling an initial setting from config
        :param Dictionary config: the configuration data
        """
        self.clear_display()
        self.update_display()

    def update_display(self):
        """Update the display contents"""
        for i in range(min(len(self._items), len(self._lines))):
            self._funhouse.set_text_color(
                0xFF0000 if (self._top + i == self._index) else 0xFFFFFF, i
            )
            self._funhouse.set_text(self._items[self._top + i][0], self._lines[i])
