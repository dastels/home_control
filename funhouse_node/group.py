# -*- mode: python -*-

# The MIT License (MIT)

# Copyright (c) 2021 Dave Astels

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import time
import adafruit_logging as logging
from config import config

logger = logging.getLogger("RoomNode")


class Group:
    def __init__(self, number, name, lights):
        self._number = number
        self._name = name
        self._lights = lights

    def is_named(self, name):
        return self._name == name

    @property
    def name(self):
        return self._name

    @property
    def number(self):
        return self._number

    @property
    def _light_status(self):
        return list([l.is_on for l in self._lights])

    def non_none(self, l):
        return [x for x in l if not x is None]

    def are_all_none(self, l):
        return all([x is None for x in l])

    @property
    def are_all_on(self):
        """Return
          True - all lights report as on
          False - at least one light reports as off
          None - at least one light doesn't respond
          """
        status = self._light_status
        if self.are_all_none(status):
            return None
        else:
            return all(self.non_none(status))

    @property
    def are_all_off(self):
        """Return
          True - all lights report as off
          False - at least one light reports as on
          None - at least one light doesn't respond
          """
        status = self._light_status
        if self.are_all_none(status):
            return None
        else:
            return not any(self.non_none(status))

    def turn_on(self, dimmed):
        count = 0
        repeat = not self.are_all_on
        while repeat:
            logger.debug("Trying to turn on group %s - attempt %d", self._name, count)
            desired_brightness = config["dim_brightness"] if dimmed else 100
            desired_colour = config["dim_colour"] if dimmed else 0xFFFFFF
            results = [
                l.turn_on(brightness=desired_brightness, colour=desired_colour)
                for l in self._lights
            ]
            logger.debug("Turn on lights results: %s", list(results))
            count += 1
            if count > config["light_repeat_fail_count"]:
                logger.error("Failed to turn on the %s group", self._name)
                return False
            elif count > config["light_repeat_warning_count"]:
                logger.warning("Trouble turning on the %s group", self._name)
            logger.debug("Are all on? %s", self.are_all_on)
            repeat = self.are_all_on is False  # it may return None
        return True

    def turn_off(self):
        count = 0
        repeat = not self.are_all_off
        while repeat:
            logger.debug("Trying to turn off group %s - attempt %d", self._name, count)
            results = [l.turn_off() for l in self._lights]
            logger.debug("Turn off lights results: %s", list(results))
            count += 1
            if count > config["light_repeat_fail_count"]:
                logger.error("Failed to turn off the %s group", self._name)
                return False
            elif count > config["light_repeat_warning_count"]:
                logger.warning("Trouble turning off the %s group", self._name)
            logger.debug("Are all off? %s", self.are_all_off)
            repeat = self.are_all_off is False  # it may return None
        return True

    def print(self):
        print("{0}-{1}: {2}".format(self._number, self._name, self._lights))
