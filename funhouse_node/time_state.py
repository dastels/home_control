# -*- mode: python -*-

# The MIT License (MIT)

# Copyright (c) 2021 Dave Astels

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

from state import State


class TimeState(State):
    def __init__(self, funhouse, machine, name, lines):
        """Set up a state.
        :param string name: the name of the state
        """
        super().__init__(funhouse, machine, name, lines)
        self._hour = 0
        self._minute = 0
        self._mode = False  # False - hour, True - minute

    def up(self):
        """Handle the up button being pressed"""
        if self._mode:
            self._minute = (self._minute + 1) % 60
        else:
            self._hour = (self._hour + 1) % 24
        self.update_display()

    def down(self):
        """Handle the down button being pressed"""
        if self._mode:
            self._minute = (self._minute - 1) % 60
        else:
            self._hour = (self._hour - 1) % 24
        self.update_display()

    def select(self):
        """Handle the select button being pressed.
        The first press moves from editing hour to editing minute.
        The second press returns to main menu"""
        if self._mode:
            self.goto("main_menu")
        else:
            self._mode = True
            self.update_display()

    def update_display(self):
        self._funhouse.set_text_color(0xFF0000 if not self._mode else 0xFFFFFF, 2)
        self._funhouse.set_text("Hour:   {0:02d}".format(self._hour), self._lines[2])
        self._funhouse.set_text_color(0xFF0000 if self._mode else 0xFFFFFF, 3)
        self._funhouse.set_text("Minute: {0:02d}".format(self._minute), self._lines[3])

    def enter(self, config):
        """Perform any entry tasks, usually pulling an initial setting from config
        :param Dictionary config: the configuration data
        """
        self._hour, self._minute = [int(part) for part in config[self._name].split(":")]
        self.clear_display()
        title = " ".join(
            [w[0].upper() + w[1:] for w in self._name.replace("_", " ").split()]
        )
        self._funhouse.set_text(title, self._lines[0])
        self.update_display()

    def exit(self, config):
        """Perform any exit tasks, usually updating a setting in config
        :param Dictionary config: the configuration data
        """
        config[self._name] = "{0:02d}:{1:02d}".format(self._hour, self._minute)
