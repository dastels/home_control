# -*- mode: python -*-

# The MIT License (MIT)

# Copyright (c) 2021 Dave Astels

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

# The state machine that orchestrates the configuration mode

from state_machine import StateMachine
from menu_state import MenuState
from turn_off_state import TurnOffState
from light_level_state import LightLevelState
from time_state import TimeState
from dim_brightness_state import DimBrightnessState
from dim_colour_state import DimColourState
from fc_state import FCState
from save_state import SaveState

import adafruit_logging as logging

logger = logging.getLogger("RoomNode")


class ConfigStateMachine(StateMachine):
    def __init__(self, funhouse, lines, groups):
        """:param list(Label) lines: the display lines in which to display things
        """
        super().__init__(self, funhouse, lines)
        MenuState(
            funhouse,
            self,
            "main_menu",
            [
                ("Group", "group_name"),
                ("Lights off In", "turn_off_interval"),
                ("Dark Level", "light_threshold"),
                ("Sleep time", "quiet_time_start"),
                ("Wake time", "quiet_time_end"),
                ("Dim brightness", "dim_brightness"),
                ("Dim colour", "dim_colour"),
                ("F/C", "use_fahrenheit"),
                ("Save", "save"),
            ],
            lines,
        )
        MenuState(
            funhouse,
            self,
            "group_name",
            [(group, "main_menu") for group in groups],
            lines,
            groups.index(config["group_name"]),
        )
        TurnOffState(funhouse, self, lines)
        LightLevelState(funhouse, self, lines)
        TimeState(funhouse, self, "quiet_time_start", lines)
        TimeState(funhouse, self, "quiet_time_end", lines)
        DimBrightnessState(funhouse, self, lines)
        DimColourState(funhouse, self, lines)
        FCState(funhouse, self, lines)
        SaveState(funhouse, self, lines)

        self.goto("main_menu")
