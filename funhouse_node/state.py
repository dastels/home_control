# -*- mode: python -*-

# The MIT License (MIT)

# Copyright (c) 2021 Dave Astels

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


class State:
    def __init__(self, funhouse, machine, name, lines=None):
        """Set up a state and register it with the machine.
        :param Funhouse funhouse: a funhouse object
        "param StateMachine machine: the owning state machine
        :param string name: the name of the state
        :param list(Label) lines: screen labels for display lines
        """
        self._funhouse = funhouse
        self._machine = machine
        self._name = name
        self._lines = lines
        machine.register_state(name, self)

    @property
    def name(self):
        return self._name

    def up(self):
        """Handle the up button being pressed"""
        pass

    def down(self):
        """Handle the down button being pressed"""
        pass

    def select(self):
        """Handle the select button being pressed. Default to returning to the main menu"""
        self.goto("main_menu")

    def pir_rise(self):
        """Handle a rising PIR signal"""
        pass

    def pir_fall(self):
        """Handle a falling PIR signal"""
        pass

    def tick(self, now):
        """Handle a regular time tick"""
        pass

    def update_display(self):
        """Update the display contents"""
        pass

    def clear_display(self):
        """Clear the display contents"""
        for i in range(len(self._lines)):
            self._funhouse.set_text_color(0xFFFFFF, i)
            self._funhouse.set_text("", self._lines[i])

    def goto(self, target):
        self._machine.goto(target)

    def enter(self, config):
        """Perform any entry tasks, usually pulling an initial setting from config
        :param Dictionary config: the configuration data
        """
        pass

    def exit(self, config):
        """Perform any exit tasks, usually updating a setting in config
        :param Dictionary config: the configuration data
        """
        pass
