# Home control node for the Adafruit Funhouse #

This code is to make a home control node out of an Adafruit Funhouse board and
the supported PIR sensor.

The Phillips HUE lighting system is supported.

## Boot options ##

As the system boots, the three buttons are used to set the operational
mode. Each button needs to be held until the operational display is show.

UP: Holding the UP button places the software into debug mode. Connecting with
to the serial/USB output will show a running debug log.

SELECT: Holding the SELECT button places the software into light meter
mode. This causes the raw light reading to be displayed (updated ten times per
second). The purpose is to check light levels in the device's home room to
determine the light threshold to set in configuration mode. If the room is
darker (lower reading0 than this threshold light will be turned on when motion
is sensed. Reset or power cycle to get out of this mode.

DOWN: Holding the DOWN button puts the software into configuration mode. Reset
or power cycle to exit config mode.

## Configuration ##

### Main menu ###

The top level menu is displayed, listing the supported settings. The active
selection is in red, the remainder in white.  UP and DOWN are used to navigate
and SELECT is used to edit the highlighted setting.

### Group ###

This presents a menu of the groups defined in the HUE system. UP/DOWN navigate
as before, and SELECT chooses a group and returns to the main menu. As before,
the active selection is in red, the remainder in white. The selected group is
the one that the device will have control of.

### Lights off in ###

This sets the number of minutes between motion stopping and the light being
turned off. UP/DOWN change the value and SELECT locks it in and returns to the
main menu.

### Dark level ###

The light level (as a raw sensor reading) below which light is deemed
required. This prevents turning lights on unneccessarily during the day. UP/DOWN
change the value and SELECT locks it in and returns to the main menu.

### Sleep time ###

The day is separated (naively) into "sleep time" and "wake time". During sleep
time lights are turned on dimly and/or with a less obtrusive colour. These are
set below.

The sleep time setting is the hour and minute (24 hr time) in the evening that
sleep time starts.

UP/DOWN changes the value (hour or minute) in red. When this mode is entered the
hour setting is active. Pressing SELECT switches to MINUTE. Pressing SELECT a
second time locks in the setting and returns to the main menu.

### Wake time ###

The wake time setting is the hour and minute in the morning at which sleep time
ends. Editing is as above.

### Dim brightness ###

The brightness (as a percentage of full brightness) to use during sleep
time. During awake time brightness of 100% is used. UP/DOWN changes the value
and SELECT locks it in and returns to the main menu.

### Dim colour ###

The colour selected from a menu to use during sleep time. During awake time
white is used. The selected colour is displayed init's colour, the remainder in
white. UP/DOWN change the selection and SELECT locks it in and returns to the
main menu.

### F/C ###

This sets the units used for temperature display. UP selects Fahrenheit, DOWN
selects CELCIUS, and SELECT locks it in and returns to the main mneu.

### Save ###

Selecting this writes the current config settings to config.py; there is no
interaction mode/screen.

## Operation ##

The operational mode presents a display of time, temperature, humidity, and
pressure. Time is updated every minute, and the rest based on the
"environment_interval" config setting (set my editing config.py.

When motion is detected lights are turned on if the light level is lower than
the light threshold config setting. If it's during sleep time the dim brightness
and colour settings are used, otherwise full brightness white is used. Note that
colour is only relevant with colour capable lighting devices.

When motion stops being detected a timer is started of a length set by the
lights off interval config setting. When that timer completes, lights are turned
off. If motion is again detected while the timer is running, it is cancelled.

When the associated group is turned on or off, the change is confirmed. I.e. if
the group is commanded to turn on, the state of each light in the group is
checked to ensure it is either on or unreachable (e.g. it's not connected or its
power is switched off). Similarly when it is commanded to turn off. If things
are not as expected the command is issued again.
