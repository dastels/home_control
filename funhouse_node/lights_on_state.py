# -*- mode: python -*-

# The MIT License (MIT)

# Copyright (c) 2021 Dave Astels

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import time
from state import State

import adafruit_logging as logging

logger = logging.getLogger("RoomNode")

# get configuration information
try:
    from config import config
except ImportError:
    raise


def is_dark_enough(light_level):
    return light_level < config["light_threshold"]


def to_minutes(a_time):
    return a_time.tm_hour * 60 + a_time.tm_min


def is_before(time_a, time_b):
    return to_minutes(time_a) < to_minutes(time_b)


def is_after(time_a, time_b):
    return to_minutes(time_a) >= to_minutes(time_b)


def extract_time(time_string):
    hour, minute = [int(x) for x in time_string.split(":")]
    return time.struct_time(0, 0, 0, hour, minute, 0, 0, 0, None)


class LightsOnState(State):
    def __init__(self, funhouse, machine, group):
        """Set up a state and register it with the machine.
        :param Funhouse funhouse: a funhouse object
        "param StateMachine machine: the owning state machine
        :param Group group: the Group object being controlled
        """
        super().__init__(funhouse, machine, "lights_on")
        self._group = group
        self._quiet_time_start = extract_time(config["quiet_time_start"])
        self._quiet_time_end = extract_time(config["quiet_time_end"])

    def _should_be_dim(self, time):
        return is_after(time, self._quiet_time_start) or is_before(
            time, self._quiet_time_end
        )

    def enter(self, config):
        """Perform any entry tasks
        :param Dictionary config: the configuration data
        """
        light_level = self._funhouse.peripherals.light
        logger.debug("Light level: %d", light_level)
        if is_dark_enough(light_level):  # motion started & dark
            logger.debug("Dark enough (%d). Turning light on", light_level)
            make_it_dim = self._should_be_dim(time.localtime())
            logger.debug("Dim light" if make_it_dim else "Bright light")
            if not self._group.turn_on(make_it_dim):
                logger.error("Couldn't turn on group %s", config["group_name"])
            else:
                logger.debug("Group %s is on", config["group_name"])
        else:
            logger.debug("Not dark enough to need light")
            self.goto("idle")

    def pir_fall(self):
        """Handle a rising PIR signal"""
        self.goto("lights_off_timer")
