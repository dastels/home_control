# -*- mode: python -*-

# The MIT License (MIT)

# Copyright (c) 2021 Dave Astels

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

from state import State


class DimBrightnessState(State):
    def __init__(self, funhouse, machine, lines):
        """Set up a state.
        :param string name: the name of the state
        """
        super().__init__(funhouse, machine, "dim_brightness", lines)
        self._value = 0

    def up(self):
        """Handle the up button being pressed"""
        self._value = min(self._value + 1, 100)
        self.update_display()

    def down(self):
        """Handle the down button being pressed"""
        self._value = max(self._value - 1, 0)
        self.update_display()

    def select(self):
        """Handle the select button being pressed. Default to returning to the main menu"""
        self.goto("main_menu")

    def update_display(self):
        self._funhouse.set_text("     {0:3d}%".format(self._value), self._lines[2])

    def goto(self, target):
        self._machine.goto(target)

    def enter(self, config):
        """Perform any entry tasks, usually pulling an initial setting from config
        :param Dictionary config: the configuration data
        """
        self._value = config["dim_brightness"]
        self.clear_display()
        self._funhouse.set_text("Dim Brightness", self._lines[0])
        self.update_display()

    def exit(self, config):
        """Perform any exit tasks, usually updating a setting in config
        :param Dictionary config: the configuration data
        """
        config["dim_brightness"] = self._value
