# -*- mode: python -*-

# The MIT License (MIT)

# Copyright (c) 2021 Dave Astels

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

# Configuration mode
# Let user edit the config.py file

import board
import adafruit_logging as logging
from digitalio import DigitalInOut, Direction, Pull
from adafruit_debouncer import Debouncer
import displayio
import terminalio
from adafruit_bitmap_font import bitmap_font
from adafruit_display_text import label
from adafruit_funhouse import FunHouse
from hue_bridge import Bridge
from config_state_machine import ConfigStateMachine


# get secret settings
try:
    from secrets import secrets
except ImportError:
    raise

logger = logging.getLogger("RoomNode")


def run_config(config, funhouse):
    logger.debug("Entering config mode")
    funhouse.peripherals.dotstars.fill(0x888888)

    funhouse.display.show(None)
    down_button = Debouncer(funhouse.peripherals._buttons[0])
    select_button = Debouncer(funhouse.peripherals._buttons[1])
    up_button = Debouncer(funhouse.peripherals._buttons[2])

    # Attempt to connect to the bridge
    funhouse.network.connect()
    wifi = funhouse.network._wifi

    try:
        username = secrets["hue_username"]
        bridge_ip = secrets["bridge_ip"]
        hue_bridge = Bridge(wifi, bridge_ip, username)
    except:
        # Perform first-time bridge setup
        hue_bridge = Bridge(wifi)
        ip = hue_bridge.discover_bridge()
        print(
            "Attempting to register username, press the link button on your Hue Bridge now!"
        )
        username = hue_bridge.register_username()
        print(
            'ADD THESE VALUES TO SECRETS.PY: \
            \n\t"bridge_ip":"{0}", \
            \n\t"hue_username":"{1}"'.format(
                ip, username
            )
        )
        raise

    group_json = hue_bridge.get_groups()
    groups = [data["name"] for (number, data) in group_json.items()]

    lines = [
        funhouse.add_text(
            text_position=(0, i * 25),
            text_color=0xFFFFFF,
            text_font="fonts/Roboto-Medium-16.bdf",
        )
        for i in range(1, 9)
    ]
    funhouse.display.show(funhouse.splash)

    machine = ConfigStateMachine(funhouse, lines, groups)

    # Stay looping in config mode until reset
    while True:
        up_button.update()
        select_button.update()
        down_button.update()

        if up_button.rose:
            funhouse.peripherals.dotstars.fill(0x000000)
            machine.up()
        elif down_button.rose:
            funhouse.peripherals.dotstars.fill(0x000000)
            machine.down()
        elif select_button.rose:
            funhouse.peripherals.dotstars.fill(0x000000)
            machine.select()
        funhouse.peripherals.dotstars.fill(0x004400)
