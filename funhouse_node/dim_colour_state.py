# -*- mode: python -*-

# The MIT License (MIT)

# Copyright (c) 2021 Dave Astels

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

from menu_state import MenuState


class ColourState(MenuState):
    def __init__(self, funhouse, machine, lines):
        """Set up a state.
        :param string name: the name of the state
        """
        super().__init__(
            funhouse,
            machine,
            "dim_colour",
            [
                ("Red", 0xFF0000),
                ("Yellow", 0xFFFF00),
                ("Green", 0x00FF00),
                ("Cyan", 0x00FFFF),
                ("Blue", 0x0000FF),
                ("Magenta", 0xFF00FF),
            ],
            lines,
        )

    def select(self):
        """Handle the select button being pressed. Default to returning to the main menu"""
        self.goto("main_menu")

    def update_display(self):
        """Update the display contents"""
        for i in range(min(len(self._items), len(self._lines))):
            self._funhouse.set_text_color(
                self._items[self._top + i][1]
                if (self._top + i == self._index)
                else 0xFFFFFF,
                i,
            )
            self._funhouse.set_text(self._items[self._top + i][0], self._lines[i])

    def enter(self, config):
        """Perform any entry tasks, usually pulling an initial setting from config
        :param Dictionary config: the configuration data
        """
        self.clear_display()
        for i in range(len(self._items)):
            if self._items[i][1] == config["dim_colour"]:
                self._index = i
                break
        self.update_display()

    def exit(self, config):
        """Perform any exit tasks, usually updating a setting in config
        :param Dictionary config: the configuration data
        """
        config["dim_colour"] = self._items[self._index][1]
