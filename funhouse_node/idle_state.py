# -*- mode: python -*-

# The MIT License (MIT)

# Copyright (c) 2021 Dave Astels

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

from state import State
import adafruit_logging as logging

logger = logging.getLogger("RoomNode")


class IdleState(State):
    def __init__(self, funhouse, machine, group):
        """Set up a state and register it with the machine.
        :param Funhouse funhouse: a funhouse object
        "param StateMachine machine: the owning state machine
        :param Group group: the Group object being controlled
        """
        super().__init__(funhouse, machine, "idle")
        self._group = group

    def pir_rise(self):
        """Handle a rising PIR signal"""
        self.goto("lights_on")

    def enter(self, config):
        """Perform any entry tasks, usually pulling an initial setting from config
        :param Dictionary config: the configuration data
        """
        if not self._group.turn_off():
            logger.error("Couldn't turn off group %s", config["group_name"])
        else:
            logger.debug("Group %s is off", config["group_name"])
