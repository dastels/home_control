import board

import adafruit_logging as logging

ERROR_COLOR = 0xFF6600
CRITICAL_COLOR = 0xFF0000

class ScreenHandler(logging.LoggingHandler):

    """Send logging messages to the screen."""

    def __init__(self, message_area):
        self._message_area = message_area

    def emit(self, level, msg):
        """Send a message to the screen.
        :param level: the logging level
        :param msg: the message to log
        """
        if level >= logging.ERROR:
            self._message_area.color = CRITICAL_COLOR if level >= logging.CRITICAL else ERROR_COLOR
            self._message_area.text = msg
