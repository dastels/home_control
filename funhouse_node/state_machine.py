# -*- mode: python -*-

# The MIT License (MIT)

# Copyright (c) 2021 Dave Astels

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

# The abstract state machine

import adafruit_logging as logging

logger = logging.getLogger("RoomNode")

# get configuration information
try:
    from config import config
except ImportError:
    raise


class StateMachine:
    def __init__(self, funhouse):
        self._funhouse = funhouse
        self._states = {}
        self._current_state = None

    def register_state(self, name, state):
        self._states[name] = state

    def up(self):
        if self._current_state:
            logger.debug("Up in %s", self._current_state.name)
            self._current_state.up()

    def down(self):
        if self._current_state:
            logger.debug("Down in %s", self._current_state.name)
            self._current_state.down()

    def select(self):
        if self._current_state:
            logger.debug("Select in %s", self._current_state.name)
            self._current_state.select()

    def pir_rise(self):
        """Handle a rising PIR signal"""
        if self._current_state:
            logger.debug("PIRrise in %s", self._current_state.name)
            self._current_state.pir_rise()

    def pir_fall(self):
        """Handle a falling PIR signal"""
        if self._current_state:
            logger.debug("PIRfall in %s", self._current_state.name)
            self._current_state.pir_fall()

    def tick(self, now):
        if self._current_state:
            logger.debug("TICK in %s", self._current_state.name)
            self._current_state.tick(now)

    def goto(self, target_state_name):
        if self._current_state:
            logger.debug("Exiting state: %s", self._current_state.name)
            self._current_state.exit(config)
        if target_state_name in self._states:
            self._current_state = self._states[target_state_name]
            logger.debug("Entering state: %s", target_state_name)
            self._current_state.enter(config)
        else:
            logger.critical("Bad target state: %s", target_state_name)
