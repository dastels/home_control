/* -*- mode: C++;-*-
 * Mesh support
 * by Dave Astels
 * 
 *
 */


#ifndef __MESH_H__
#define __MESH_H__

#include <RF24Mesh.h>

typedef uint8_t node_id_t;

bool send_reading(const char *name, uint16_t value);
bool send_all_sensor_data(sensors_t *data);
bool initialize_mesh(node_id_t node_id);
void process_messages();

#endif
