/* -*- mode: C++;-*-
 * Control of the RGB LED Indicator
 * (c) 2016 Dave Astels
 */

#include <Arduino.h>
#include "config.h"
#include "indicator.h"


uint8_t blinking_red = 0;
uint8_t blinking_green = 0;
uint8_t blinking_blue = 0;
uint16_t off_duration = 0;
uint16_t on_duration = 0;
uint16_t turn_on_time = 0;
uint16_t turn_off_time = 0;

void show_rgb(uint8_t red, uint8_t green, uint8_t blue)
{
  if (has_common_cathode_led()) {
    analogWrite(RED_PIN, red);
    analogWrite(GREEN_PIN, green);
    analogWrite(BLUE_PIN, blue);
  }
  
  if (has_common_anode_led()) {
	analogWrite(RED_PIN, 255 - red);
	analogWrite(GREEN_PIN, 255 - green);
	analogWrite(BLUE_PIN, 255 - blue);
  }
}


void show_rgb_blinking(uint8_t red, uint8_t green, uint8_t blue, uint16_t millis_off, uint16_t millis_on)
{
  show_rgb(red, green, blue);
  off_duration = 0;
  on_duration = 0;
  turn_on_time = 0;
  turn_off_time = 0;
  if (millis_off > 0 && millis_on > 0 && (red > 0 || green > 0 || blue > 0)) {
    blinking_red = red;
    blinking_green = green;
    blinking_blue = blue;
    off_duration = millis_off;
    on_duration = millis_on;
    turn_off_time = millis() + on_duration;
  }
}


void update_led(uint16_t now)
{
  if (turn_off_time > 0 && now >= turn_off_time) {
    show_rgb(0, 0, 0);
    turn_on_time = now + off_duration;
    turn_off_time = 0;
  } else if (turn_on_time > 0 && now >= turn_on_time) {
    show_rgb(blinking_red, blinking_green, blinking_blue);
    turn_off_time = now + on_duration;
    turn_on_time = 0;
  }
}


void clear_indicator()
{
  show_rgb(0x00, 0x00, 0x00);
}
