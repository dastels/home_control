/* -*- mode: C++;-*-
 * Sensors
 * Dave Astels
 */

#ifndef __SENSORS_H__
#define __SENSORS_H__

#include <SPI.h>
#include "Adafruit_SI7021.h"
//#include "SI1145.h"

#define MAX_NUMBER_OF_SENSORS 16

typedef void (*isr)();

void initialize_sensors(isr motion_isr);

uint16_t read_motion();
//void update_motion(sensors_t *sensorData);
uint16_t read_light();
//void update_light(sensors_t *sensorData);
bool enable_prox(bool on_off);
uint16_t read_prox();
//void update_prox(sensors_t *sensorData);
//void update_temp_rh(sensors_t *sensorData);
//void update_all_sensor_data(sensors_t *data);
unsigned long update_next_sensor(unsigned long now);

#endif
