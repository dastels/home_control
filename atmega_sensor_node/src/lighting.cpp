/* -*- mode: C;-*-
 * Control of neopixel undercounter lighting
 * (c) 2016 Dave Astels
 */

#include <Arduino.h>
#include "config.h"
#include "lighting.h"
#include "Adafruit_NeoPixel.h"

Adafruit_NeoPixel pixels;

void setup_lighting()
{
  if (has_neopixels()) {
      pixels = Adafruit_NeoPixel(neopixel_size(), NEOPIXEL_PIN);
      pixels.begin();
      pixels.show();
      for (int i = 0; i < neopixel_size(); i++) {
        pixels.setPixelColor(i, 64, 64, 64);
        pixels.show();
        delay(100);
        pixels.setPixelColor(i, 0, 0, 0);
        pixels.show();
      }
    }
}

void set_lighting(uint8_t red, uint8_t green, uint8_t blue)
{
  if (has_neopixels()) {
    for (int i = 0; i < neopixel_size(); i++) { 
        pixels.setPixelColor(i, red, green, blue);
      }
      pixels.show();
    }
}
