/* -*- mode: C++;-*-
 * Mesh support
 * (c) 2016 Dave Astels
 */

#include <RF24Network.h>
#include <RF24.h>
#include <RF24Mesh.h>
#include "sensor_node.h"
#include "sensor_names.h"
#include "sensors.h"
#include "mesh.h"
#include "rf24_defs.h"
#include "indicator.h"
#include "lighting.h"
#include "extension.h"

#define RESET_COMMAND 255
#define LED_COMMAND 1
#define NEOPIXEL_COMMAND 2
#define PROXIMITY_COMMAND 3

RF24 radio(7, 8);
RF24Network network(radio);
RF24Mesh mesh(radio, network);

sensor_data_t sensor_data;
extern bool prox_enabled;
extern uint16_t prox_interval;
extern unsigned long sound_check_time;
extern unsigned long features_send_time;
extern bool extension_requested;
extern uint8_t extension_bit;

led_command_t *led_cmd;
neopixel_command_t *neopixel_cmd;
proximity_command_t *prox_cmd;
sound_command_t *sound_cmd;
ext_config_command_t *ext_config_cmd;
ext_read_command_t *ext_read_cmd;
ext_write_command_t *ext_write_cmd;


bool initialize_mesh(node_id_t node_id)
{
  //radio.setPALevel (RF24_PA_MIN);
  show_rgb(0xff, 0x00, 0xff);
  mesh.setNodeID(node_id);

#ifdef DEBUG
  Serial.print(F("Node ID: "));
  Serial.println(mesh.getNodeID());
#endif
  bool connected = mesh.begin(MESH_DEFAULT_CHANNEL, RF24_250KBPS);
  if (!connected) {
    LOG(F("Could not connect to the mesh."));
    show_rgb(0xff, 0x00, 0x00);
  } else {
    LOG(F("Connected to the mesh."));
    show_rgb(0x00, 0x00, 0xff);
  }
  return connected;
}


bool send_reading(const char *name, uint16_t value) {
  if (strlen(name) > 11) {
    return false;
  }

  sensor_data.node = mesh.getNodeID();
  strcpy(sensor_data.type, name);
  sensor_data.value = value;
  int retries = 0;
  while (retries < 5) {
	int sent = mesh.write(&sensor_data, 'S', sizeof(sensor_data));
	if (!sent) {
	  if (!mesh.checkConnection()) {
		mesh.renewAddress();
	  }
	  retries++;
	  delay(10);
	} else {
	  return true;
	}
  }
	return false;
}


void process_messages()
{
  uint8_t payload[32];
  while (network.available()) {
    RF24NetworkHeader header;
    //    network.peek(header);

    network.read(header, &payload, 32);

    switch (header.type) {

    case 'I':    // Indicator command
      LOG(F("Received LED command"));
      led_cmd = (led_command_t*)(&payload[2]);
      show_rgb_blinking(led_cmd->red, led_cmd->green, led_cmd->blue, led_cmd->millis_off, led_cmd->millis_on);
      break;

    case 'L':    // Lighting (aka neopixel) command
      LOG(F("Received NEOPIXEL command"));
      if (has_neopixels()) {
        neopixel_cmd = (neopixel_command_t*)(&payload[2]);
        set_lighting(neopixel_cmd->red, neopixel_cmd->green, neopixel_cmd->blue);
      }
      break;
      
    case 'P':    // Proximity enable/disable command
      LOG(F("Received PROXIMITY command"));
      if (has_prox()) {
        prox_cmd = (proximity_command_t*)(&payload[2]);
        prox_interval = prox_cmd->interval;
		prox_enabled = prox_interval > 0;
      }
      break;

    case 'F':                   // Node features
      LOG(F("Received FEATURES command"));
      features_send_time = (unsigned long)(random(100) * 10);
      break;

    case 'C':                   // extension port configuration
      LOG(F("Received EXT CONFIG command"));
      ext_config_cmd = (ext_config_command_t*)(&payload[2]);
      config_extension_port((uint8_t)ext_config_cmd->directions, (uint8_t)ext_config_cmd->pullups);
      break;

    case 'R':                   // read from extension port bit
      LOG(F("Received EXT READ command"));
      extension_requested = true;
      ext_read_cmd = (ext_read_command_t*)(&payload[2]); 
      extension_bit = (uint8_t)ext_read_cmd->bit;
      break;

    case 'W':                   // write to extension port bit
      LOG(F("Received EXT WRITE command"));
      ext_write_cmd = (ext_write_command_t*)(&payload[2]); 
      write_extension_port((uint8_t)ext_write_cmd->bit, (uint8_t)ext_write_cmd->value);
      break;
	}
  }
}
