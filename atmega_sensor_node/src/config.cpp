/* -*- mode: C -*-
 * Config
 * Dave Astels
 */

#include "PortExtender.h"

extern PortExtender extension_port;

#define MOTION_BIT 0x01
#define LIGHT_BIT 0x02
#define PROX_BIT 0x04
#define TEMP_BIT 0x08
#define LED_BIT 0x10
#define NEOPIXEL_BIT 0x20
#define NEOPIXEL_SIZE_BIT 0x40
#define EXTRA_BIT 0x80

uint8_t config_bits = 0;

uint8_t read_config_bits() {
  if (config_bits == 0) {
    config_bits = extension_port.read(PORT_A);
  }
  return config_bits;
}

bool has_motion() {
  return (read_config_bits() & MOTION_BIT) != 0;
}

bool has_light() {
  return (read_config_bits() & LIGHT_BIT) != 0;
}

bool has_prox() {
  return (read_config_bits() & PROX_BIT) != 0;
}

bool has_si7021() {
  return (read_config_bits() & TEMP_BIT) != 0;
}

bool has_common_cathode_led() {
  return (read_config_bits() & LED_BIT) == 0;
}

bool has_common_anode_led() {
  return (read_config_bits() & LED_BIT) != 0;
}

bool has_neopixels() {
  return (read_config_bits() & NEOPIXEL_BIT) != 0;
}

int neopixel_size() {
  return ((read_config_bits() & NEOPIXEL_SIZE_BIT) == 0) ? 8 : 60;
}

bool has_sound() {
  return NODE_ID == 7;
}
