/* -*- mode: C -*-
 * Sensor node 
 * Dave Astels
 */

#ifndef __CONFIG__
#define __CONFIG__

#ifndef NODE_ID
#define NODE_ID 1
#endif

#if NODE_ID == 7
#define HAS_SOUND
#endif

#define READING_INTERVAL 60000  // Full sensor sweep every minute

//#define DEBUG 1




//------------------------------------------------------------------------------

uint8_t read_config_bits();
bool has_motion();
bool has_light();
bool has_prox();
bool has_si7021();
bool has_common_cathode_led();
bool has_common_anode_led();
bool has_neopixels();
int neopixel_size();
bool has_sound();

// Pin definitions

#define MOTION_PIN 2
#define PHOTO_PIN 1
#ifdef HAS_SOUND
#define RED_PIN 9
#else
#define RED_PIN 3
#endif
#define GREEN_PIN 5
#define BLUE_PIN 6
#define NEOPIXEL_PIN 4
#define SOUND_PIN 3

#ifdef DEBUG
#define LOG(x) { Serial.println(F("")); Serial.println(x); Serial.flush(); }
#else
#define LOG(x)
#endif

#endif
