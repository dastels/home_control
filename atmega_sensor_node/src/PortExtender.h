/*-------------------------------------------------------------------------
 * -*- mode: C -*-
 *
 * Dual MCP23017 Port Extender Shield library.
 *
 * Copyright (c) 2016 Dave Astels
 * All rights reserved
 *-------------------------------------------------------------------------
 * This file is part of the PortExpander library.
 *
 * PortExpander is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * PortExpander is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with PortExpander.  If not, see
 * <http://www.gnu.org/licenses/>.
 *-------------------------------------------------------------------------*/

#include <Wire.h>

#ifndef __PORT_EXTENDER__
#define __PORT_EXTENDER__

#define PORT_A 0
#define PORT_B 1

class PortExtender
{
private:
  uint8_t _address;
  uint8_t _directions[2];
  uint8_t _pullups[2];
  uint8_t _outputs[2];

public:
  PortExtender(uint8_t address);

  void pinMode(uint8_t port, uint8_t pin, uint8_t direction);

  void pinModes(uint8_t port, uint8_t directions, uint8_t pullups);
  void pinModes(uint8_t port, uint8_t directions);

  void write(uint8_t port, uint8_t pin, uint8_t value);
  void write(uint8_t port, uint8_t values);

  uint8_t read(uint8_t port);
  uint8_t read(uint8_t port, uint8_t pin);
};
#endif /* ifndef __PORT_EXTENDER__ */
