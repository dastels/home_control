/* -*- mode: C++;-*-
 * Control of the RGB LED Indicator
 * (c) 2016 Dave Astels
 */

#ifndef __INDICATOR_H__
#define __INDICATOR_H__

void update_led(uint16_t now);
void show_rgb(uint8_t red, uint8_t green, uint8_t blue);
void show_rgb_blinking(uint8_t red, uint8_t green, uint8_t blue, uint16_t millis_off, uint16_t millis_on);
void clear_indicator();


#endif
