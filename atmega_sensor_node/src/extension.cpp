/* -*- mode: C -*-
 * Extension port
 * Dave Astels
 */

#include <Arduino.h>
#include "PortExtender.h"
#include "config.h"
#include "extension.h"

extern PortExtender extension_port;

void config_extension_port(uint8_t directions, uint8_t pullups)
{
#ifdef DEBUG
  Serial.print(F("Ext config: d: "));
  Serial.print(directions, BIN);
  Serial.print(F(" p: "));
  Serial.println(pullups, BIN);
#endif
  extension_port.pinModes(PORT_B, directions, pullups);
}


void write_extension_port(uint8_t bit, uint8_t value)
{
#ifdef DEBUG
  Serial.print(F("Ext write: b: "));
  Serial.print(bit);
  Serial.print(F(" v: "));
  Serial.println(value);
#endif
  extension_port.write(PORT_B, bit, value);
}


uint8_t read_extension_port(uint8_t bit)
{
#ifdef DEBUG
  Serial.print(F("Ext read: b: "));
  Serial.print(bit);
#endif
  int result = extension_port.read(PORT_B, bit);
#ifdef DEBUG
  Serial.print(F(" -> "));
  Serial.println(result);
#endif
  return result;
}
