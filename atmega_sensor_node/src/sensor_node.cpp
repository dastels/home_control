/* -*- mode: C;-*-
 * Sensor node 
 * Dave Astels
 */

#include <Arduino.h>
#include <Wire.h>
#include "PortExtender.h"
#include "sensor_node.h"
#include "sensors.h"
#include "sensor_names.h"
#include "indicator.h"
#include "lighting.h"
#include "mesh.h"
#include "extension.h"
sensors_t data;

extern RF24Mesh mesh;

bool prox_enabled = false;
uint16_t prox_interval;
unsigned long features_send_time = 0L;
bool extension_requested = false;
uint8_t extension_bit = 0;
unsigned long update_time = 0L;
unsigned long next_send_time = 0L;
unsigned long prox_update_time = 0L;
unsigned long connection_check_time = 0L;
bool motion_flag = false;
bool sound_flag = false;

uint16_t sound_interval = 250;
unsigned long sound_check_time = 0L;

PortExtender extension_port = PortExtender(0x20);

void motion_isr()
{
  motion_flag = true;
}
 
void sound_isr()
{
  sound_flag = true;
}
 
void setup()
{
  Wire.begin();
  extension_port.pinModes(PORT_A, 0xFF, 0xFF); /* Port A: inputs with pullups */
  
  extension_port.pinModes(PORT_B, 0x0F, 0x0F); /* Port B: outputs on high nybble, inputs on low */
  show_rgb(0xff, 0x00, 0x00);
     
#ifdef DEBUG
  Serial.begin(9600);
#endif
  
  while (!initialize_mesh(NODE_ID)) {
    delay(1000);
  }

  send_reading("register", NODE_ID);
  
  initialize_sensors(motion_isr, sound_isr);
  
  show_rgb(0x00, 0x00, 0x00);

  if (has_neopixels()) {
    setup_lighting();
  }
  
  if (has_motion()) {
    motion_isr();
  }
}


void loop()
{
  unsigned long now = millis();
  boolean is_time_for_update = now >= update_time;
  boolean is_time_for_prox_update = now >= prox_update_time;
  boolean is_time_for_next_send = next_send_time && now >= next_send_time;
  
  mesh.update();

#ifdef DEBUG
  Serial.print(F("."));
#endif

  //--------------------------------------------------------------------------------
  // Check the mesh connection if it's time to
  
  if (now >= connection_check_time) {
    connection_check_time = now + 30000;
    if (!mesh.checkConnection()) {
#ifdef DEBUG
      Serial.println(F("*** Renewing address ***"));
#endif
      mesh.renewAddress();
#ifdef DEBUG
    } else {
      Serial.println(F("*** Mesh OK ***"));
#endif
    }
  }

  //--------------------------------------------------------------------------------
  // Send motion start/end if the motion state changed
  
  if (has_motion()) {
    if (motion_flag) {
      motion_flag = false;
      uint16_t motion_value = read_motion();
      uint16_t light_level = read_light();
      if (motion_value != 0) {
        LOG(F("motion start"));
        send_reading(MOTION_STARTED_NAME, light_level);
      } else {
        LOG(F("motion end"));
        send_reading(MOTION_ENDED_NAME, light_level);
      }
    }
  }

  //--------------------------------------------------------------------------------
  // Send sound trigger if sound was detected
  if (has_sound()) {
	if (sound_flag) {
	  sound_flag = false;
	  send_reading(SOUND_NAME, 1);
	}
  }

  //--------------------------------------------------------------------------------
  // Send the feature bitmask if requested
  
  if (features_send_time && now >= features_send_time) {
    features_send_time = 0L;
    send_reading(FEATURES_NAME, read_config_bits());
    LOG(F("Sent features"));
  }

  //--------------------------------------------------------------------------------
  // Send the value of an extension bit if requested
  
  if (extension_requested) {
    extension_requested = false;
    send_reading(EXTENSION_NAME, read_extension_port(extension_bit));
    LOG(F("Sent extension data"));
  }

  //--------------------------------------------------------------------------------
  // Send a proximity reading if available and it's time
  
  if (has_prox() && prox_enabled && is_time_for_prox_update) {
    LOG(F("Checking proximity"));
    prox_update_time = now + prox_interval;
    uint16_t prox_reading = read_prox();
    if (prox_reading > 0) {
      send_reading(PROXIMITY_NAME, prox_reading);
    }
    LOG(F("Sent prox"));
    
  }

  //--------------------------------------------------------------------------------
  // read and send sensor values if it's time
  
  if (is_time_for_update) {
    LOG(F("Starting update"));
    update_time = now + READING_INTERVAL;
	is_time_for_next_send = true;
  }
  
  if (is_time_for_next_send) {
    LOG(F("Sending update"));
	next_send_time = update_next_sensor(now);
  }

  process_messages();
  update_led(now);
  delay(5);
}
