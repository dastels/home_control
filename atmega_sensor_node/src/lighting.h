/* -*- mode: C++;-*-
 * Control of neopixel undercounter lighting
 * (c) 2016 Dave Astels
 */

#ifndef __LIGHTING_H__
#define __LIGHTING_H__

void setup_lighting();
void set_lighting(uint8_t red, uint8_t green, uint8_t blue);

#endif
