/* -*- mode: C -*-
 * Sensor node 
 * Dave Astels
 */

#ifndef __SENSOR_NODE__
#define __SENSOR_NODE__

#include "config.h"

typedef struct {
  int16_t motion;
  int16_t light;
  int16_t prox;
  int16_t sound;
  int16_t temperature;
  int16_t humidity;
} sensors_t;

#endif
