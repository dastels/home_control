/*-------------------------------------------------------------------------
 * Dual MCP23017 Port Extender Shield library.
 *
 * Copyright (c) 2016 Dave Astels
 * All rights reserved
 *-------------------------------------------------------------------------
 * This file is part of the PortExpander library.
 *
 * PortExpander is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * PortExpander is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with PortExpander.  If not, see
 * <http://www.gnu.org/licenses/>.
 *-------------------------------------------------------------------------*/

#include <Arduino.h>
#include "PortExtender.h"

// Initialize a port extender chip
// address: the I2C address of the port extender chip

PortExtender::PortExtender(uint8_t address)
{
  _address = address;
  for (int i = 0; i < 2; i++) {
    _directions[i] = 0x00;
    _pullups[i] = 0x00;
    _outputs[i] = 0x00;
  }
}


// Set the direction of a single bit
// port: the port to use (PORTA or PORTB)
// pin: the pin to set (0-7)
// mode: OUTPUT, INPUT, or INPUT_PULLUP

void PortExtender::pinMode(uint8_t port, uint8_t pin, uint8_t mode)
{
  uint8_t bit = 0x01 << pin;
  if (mode == OUTPUT) {
    _directions[port] &= ~bit;
  } else {
    _directions[port] |= bit;
  }

  Wire.beginTransmission(_address);
  Wire.write(port);
  Wire.write(_directions[port]);
  Wire.endTransmission();

  if (mode == INPUT_PULLUP) {
    _pullups[port] |= bit;
  } else {
    _pullups[port] &= ~bit;
  }

  Wire.beginTransmission(_address);
  Wire.write(0x0C + port);
  Wire.write(_pullups[port]);
  Wire.endTransmission();
}


// Set the direction of all 8 bits of a port at once.
// port: the port to use (PORTA or PORTB)
// directions: each bit is 0 = output, 1 = input
// pullups: 0 (no pullup) or 1 (pullup) for each bit position

void PortExtender::pinModes(uint8_t port, uint8_t directions, uint8_t pullups)
{
  _directions[port] = directions;

  Wire.beginTransmission(_address);
  Wire.write(port);
  Wire.write(_directions[port]);
  Wire.endTransmission();

  _pullups[port] = pullups;

  Wire.beginTransmission(_address);
  Wire.write(0x0C + port);
  Wire.write(_pullups[port]);
  Wire.endTransmission();
}


// Set the direction of all 8 bits of a port at once.
// port: the port to use (PORTA or PORTB)
// directions: each bit is 0 = output, 1 = input
// Input pins default to no pullup

void PortExtender::pinModes(uint8_t port, uint8_t directions)
{
  pinModes(port, directions, 0x00);
}


// Write to a specific bit
// port: the port to use (PORTA or PORTB)
// pin: the pin to set (0-7)
// value: the value to set the bit to (LOW or HIGH)

void PortExtender::write(uint8_t port, uint8_t pin, uint8_t value)
{
  _outputs[port] &= ~(0x01 << pin);
  _outputs[port] |= ((value & 0x01) << pin);

  Wire.beginTransmission(_address);
  Wire.write(0x12 + port);
  Wire.write(_outputs[port]);
  Wire.endTransmission();
}


// Write to all bits of a port
// port: the port to use (PORTA or PORTB)
// values: the values to write

void PortExtender::write(uint8_t port, uint8_t values)
{
  _outputs[port] = values;

  Wire.beginTransmission(_address);
  Wire.write(0x12 + port);
  Wire.write(_outputs[port]);
  Wire.endTransmission();
}


// Read from a single bit
// port: the port to use (PORTA or PORTB)
// pin: the pin to set (0-7)
// Returns LOW or HIGH

uint8_t PortExtender::read(uint8_t port, uint8_t pin)
{
  uint8_t val = read(port);
  return (val >> pin) & 0x01;
}


// Read all bits of a port
// port: the port to use (PORTA or PORTB)
// Returns a byte containing the 8 bits of the port

uint8_t PortExtender::read(uint8_t port)
{
  Wire.beginTransmission(_address);
  Wire.write(0x12 + port);
  Wire.endTransmission();
  Wire.requestFrom(_address, 1);
  return Wire.read();
}
