/* -*- mode: C++ -*-
 * Extension port support
 * Dave Astels
 */

#ifndef __EXTENSION_H__
#define __EXTENSION_H__

void config_extension_port(uint8_t directions, uint8_t pullups);
void write_extension_port(uint8_t bit, uint8_t value);
uint8_t read_extension_port(uint8_t bit);

#endif
