/* -*- mode: C; -*-
 * Sensors
 * Dave Astels
 */

#include <Arduino.h>
#include "sensor_node.h"
#include "sensors.h"
#include "sensor_names.h"
#include "indicator.h"
#include "VL53L0X.h"
#include "mesh.h"

int sensor_state = 0;

//SI1145 si1145;
VL53L0X prox_sensor;
Adafruit_Si7021 si7021;

void initialize_sensors(isr motion_isr, isr sound_isr)
{
  if (has_motion()) {
    pinMode(MOTION_PIN, INPUT);
    attachInterrupt(digitalPinToInterrupt(MOTION_PIN), motion_isr, CHANGE);
  }

  if (has_sound()) {
	pinMode(SOUND_PIN, INPUT);
	attachInterrupt(digitalPinToInterrupt(SOUND_PIN), sound_isr, RISING);
  }
  
  if (has_prox()) {
    prox_sensor.init();
    prox_sensor.setTimeout(500);
  }
  
  if (has_si7021()) {
    si7021 = Adafruit_Si7021();
    si7021.begin();
  }
}
 
 
uint16_t read_motion()
{
  if (has_motion()) {
    return (uint16_t)digitalRead(MOTION_PIN);
  } else {
    return 0;
  }
}


/* void update_motion(sensors_t *data) */
/* { */
/*   data->motion = read_motion(); */
/* } */


uint16_t read_light()
{
  if (has_light()) {
    return (uint16_t)analogRead(PHOTO_PIN);
  } else {
    return 0;
  }
}


/* void update_light(sensors_t *data) */
/* { */
/*   uint16_t new_reading = read_light(); */
/*   data->light = new_reading; */
/* } */


uint16_t read_prox()
{
  if (has_prox()) {
    return prox_sensor.readRangeSingleMillimeters();
  } else {
    return 0;
  }
}

 
/* void update_prox(sensors_t *sensorData) */
/* { */
/*   sensorData->prox = read_prox(); */
/* } */


/* void update_temp_rh(sensors_t *data) */
/* { */
/*   if (has_si7021()) { */
/*     data->temperature = (int16_t)(si7021.readTemperature() * 100); */
/*     data->humidity = (int16_t)(si7021.readHumidity() * 100); */
/*   } */
/* } */


/* void update_all_sensor_data(sensors_t *data) */
/* { */
/*   if (has_motion()) { */
/*     update_motion(data); */
/*   } */
/*   if (has_light()) { */
/*     update_light(data); */
/*   } */
/*   if (has_si7021()) { */
/*     update_temp_rh(data); */
/*   } */
/* } */


unsigned long update_next_sensor(unsigned long now)
{
  unsigned long next_time = now + 500 + random(500);
  
  switch(sensor_state) {
  case 0: 						// motion
	if (has_motion()) {
	  send_reading(MOTION_NAME, read_motion());
	}
	sensor_state++;
	return next_time;
  case 1:						// light
	if (has_light()) {
	  send_reading(LIGHT_NAME, read_light());
	}
	sensor_state++;
	return next_time;
  case 2:						// temperature
	if (has_si7021()) {
	  send_reading(TEMPERATURE_NAME, (int16_t)(si7021.readTemperature() * 100));
	}
	sensor_state++;
	return next_time;
  case 3:						// humidity
	if (has_si7021()) {
	  send_reading(HUMIDITY_NAME, (int16_t)(si7021.readHumidity() * 100));
	}
	sensor_state = 0;
	return 0;
  }
  return 0;
}
