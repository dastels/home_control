/* -*- mode: C++ -*-
 * Definitions specific to the RF24 radio mesh structure
 * Dave Astels
 */

#ifndef __RF24_DEFS_H__
#define __RF24_DEFS_H__


typedef struct {
  uint16_t node;
  uint16_t red;
  uint16_t green;
  uint16_t blue;
  uint16_t millis_off;
  uint16_t millis_on;
} led_command_t;

typedef struct {
  uint16_t node;
  uint16_t red;
  uint16_t green;
  uint16_t blue;
} neopixel_command_t;

typedef struct {
  uint16_t node;
  uint16_t interval;
} proximity_command_t;

typedef struct {
  uint16_t node;
  uint16_t enable;
} sound_command_t;

typedef struct {
  uint16_t node;
  char type[12];
  uint16_t value;
} sensor_data_t;

typedef struct {
  uint16_t node;
  uint16_t directions;
  uint16_t pullups;
} ext_config_command_t;

typedef struct {
  uint16_t node;
  uint16_t bit;
} ext_read_command_t;

typedef struct {
  uint16_t node;
  uint16_t bit;
  uint16_t value;
} ext_write_command_t;


#endif
