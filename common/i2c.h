/* -*- mode: C++ -*-
 * I2C support 
 * Dave Astels
 */

#ifndef __I2C__
#define __I2C__


typedef struct {
  uint16_t cmd_tag;
  uint16_t node;
  uint16_t red;
  uint16_t green;
  uint16_t blue;
  uint16_t millis_off;
  uint16_t millis_on;
} i2c_led_command_t;


typedef struct {
  uint16_t cmd_tag;
  uint16_t node;
  uint16_t red;
  uint16_t green;
  uint16_t blue;
} i2c_neopixel_command_t;


typedef struct {
  uint16_t cmd_tag;
  uint16_t node;
  uint16_t enable;
} i2c_proximity_command_t;


typedef struct {
  uint16_t cmd_tag;
  uint16_t node;
} i2c_feature_command_t;


typedef struct {
  uint16_t cmd_tag;
  uint16_t node;
  uint16_t directions;
  uint16_t pullups;
} i2c_ext_config_command_t;


typedef struct {
  uint16_t cmd_tag;
  uint16_t node;
  uint16_t bit;
} i2c_ext_read_command_t;


typedef struct {
  uint16_t cmd_tag;
  uint16_t node;
  uint16_t bit;
  uint16_t value;
} i2c_ext_write_command_t;


typedef struct {
  uint16_t cmd_tag;
  uint16_t node;
} i2c_command_preamble_t;


#define BASE_NODE_ADDRESS 0x08
static const char *dev_name = "/dev/i2c-1";
int base_node_fd;

#define LED_COMMAND 1
#define NEOPIXEL_COMMAND 2
#define PROXIMITY_COMMAND 3
#define FEATURE_COMMAND 4
#define EXT_CONFIG_COMMAND 5
#define EXT_WRITE_COMMAND 6
#define EXT_READ_COMMAND 7


// For host.cpp

bool set_led(uint16_t node, uint16_t red, uint16_t green, uint16_t blue, uint16_t millis_off, uint16_t millis_on);
bool set_neopixel(uint16_t node, uint16_t red, uint16_t green, uint16_t blue);
bool set_proximity(uint16_t node, uint16_t enable);
bool request_features(uint16_t node);
bool config_ext(uint16_t node, uint16_t directions, uint16_t pullups);
bool read_ext(uint16_t node, uint16_t bit);
bool write_ext(uint16_t node, uint16_t bit, uint16_t value);

#endif
