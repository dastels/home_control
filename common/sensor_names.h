/* -*- mode: C++;-*-
 * Sensor names
 * Dave Astels
 */

#ifndef __SENSOR_NAMES_H__
#define __SENSOR_NAMES_H__

#define MOTION_NAME "motion"
#define MOTION_STARTED_NAME "motionstart"
#define MOTION_ENDED_NAME "motionend"
#define BATTERY_NAME "battery"
#define LIGHT_NAME "light"
#define PROXIMITY_NAME "proximity"
#define SOUND_NAME "sound"
#define TEMPERATURE_NAME "temperature"
#define HUMIDITY_NAME "humidity"
#define FEATURES_NAME "features"
#define EXTENSION_NAME "extension"

#endif
