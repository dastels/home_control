;;; -*- scheme -*-
;;; Copyright 2017 Dave Astels. All rights reserved.
;;; Use of this source code is governed by a BSD-style
;;; license that can be found in the LICENSE file.
;;;
;;; MQTT support

(declare (uses srfi-13))
(declare (unit mqtt))


;; Strips apart a sensor data message topic shc/sensors/data/<node>/<sensor> to extract node & sensor info
(define (topic->sensor-info topic)
  (let ((topic-parts (string-split topic "/")))
	(cons (string->number (fourth topic-parts)) (fifth topic-parts))))


;; Strips apart a command (lighting, fan, etc) message topic shc/<command>/<node> to extract node info
(define (topic->command-info topic)
  (let ((topic-parts (string-split topic "/")))
	(cons (string->number (third topic-parts)) "")))


;; Extract the node from a sensor related topic
;; shc/sensors/<style>/<node>/...
(define (sensor-topic->node topic)
  (string->number (fourth (string-split topic "/"))))


;; Extract the name of the sensor from a sensor related topic
;; shc/sensors/<style>/<node>/<sensor>
(define (sensor-topic->name topic)
  (string-downcase (fifth (string-split topic "/"))))


;; Extract the node number from a command topic
;; shc/lighting/<node>
(define (control-topic->node topic)
  (string->number (third (string-split topic "/"))))

