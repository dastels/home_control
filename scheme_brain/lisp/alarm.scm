;;; -*- scheme -*-
;;; Copyright 2019 Dave Astels. All rights reserved.
;;; Use of this source code is governed by a BSD-style
;;; license that can be found in the LICENSE file.
;;;
;;; Support for the alarm panel

(require-extension srfi-1 medea http-client intarweb uri-common irregex posix log5scm syslog)
(declare (uses logging util))
(declare (unit alarm))

(define (panel/button-handler topic payload)
  (log-for (debug) "In panel handler: ~S ~S" topic payload)
  ;; (let* ((value (read-json payload)))
  ;;   )
  )



(initializers/register (lambda ()
                         (log-for (info) "  alarm panel")
                         (mqtt-mux/register "shc/panel/button" panel/button-handler)
                         ))
