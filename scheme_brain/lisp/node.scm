;;; -*- scheme -*-
;;; Copyright 2017 Dave Astels. All rights reserved.
;;; Use of this source code is governed by a BSD-style
;;; license that can be found in the LICENSE file.

;;; This file handles nodes

(require-extension log5scm syslog medea extras posix-extras) 
(declare (uses util logging mqtt-mux))
(declare (unit node))

;; --------------------------------------------------------------------------------
;; Node feature management

(define **node/config** '((nodes . #())
                          (groups . #())))

(define (node-config/nodes)
  (alist-ref 'nodes **node/config**))


(define (node-config/nodes! new-nodes)
  (alist-update! 'nodes new-nodes **node/config** ))


(define (node-config/node node-id)
  (let ((nodes (node-config/nodes)))
    (if nodes
        (vector-ref nodes node-id)
        (error "Missing nodes config"))))


(define (node-config/groups)
  (alist-ref 'groups **node/config**))


(define (node-config/groups! new-groups)
  (alist-update! 'groups new-groups **node/config**))


(define (node-config/group group-id)
  (let ((groups (node-config/groups)))
    (if groups
        (vector-ref groups group-id)
        (error "Missing groups config"))))


(define (node-config/node-features node-id)
  (let ((node-data (node-config/node node-id)))
    (alist-ref 'features node-data)))


(define (node-config/update-node node-id new-node-data)
  (let ((nodes (node-config/nodes)))
    (if nodes
        (vector-set! nodes node-id new-node-data)
        (error "Missing nodes config"))))


(define (node-config/node-features! new-features node-id)
  (node-config/update-node node-id (alist-update! 'features new-features (node-config/node node-id))))

;; Feature bit masks

(define node/motion-mask #x01)
(define node/light-mask #x02)
(define node/prox-mask #x04)
(define node/temp-rh-mask #x08)
(define node/neopixel-mask #x20)

(define (node/dump-features n)
  (format #t " Features:")
  (when (node/check-feature-bit n node/motion-mask)
    (format #t " motion"))
  (when (node/check-feature-bit n node/light-mask)
    (format #t " light"))
  (when (node/check-feature-bit n node/prox-mask)
    (format #t " proximity"))
  (when (node/check-feature-bit n node/temp-rh-mask)
    (format #t " temp/rh"))
  (when (node/check-feature-bit n node/neopixel-mask)
    (format #t " neopixels"))
  (format #t "~%"))


;; Capture and record the feature bits sent by a node
(define (node/record-features node-id features)
  (node-config/node-features! features node-id))


;;Check whether or not a node has a given feature
(define (node/check-feature-bit node-id mask)
  (let ((features (node-config/node-features node-id)))
    (not (or (not features)
             (zero? (bitwise-and features mask))))))


;; Ask a node to send its feature bits
(define (node/request-features node-id)
  (mqtt-queue/send-now (format #f "shc/sensors/command/~A/features" node-id) ""))


(define (node/has-motion? node-id)
  (node/check-feature-bit node-id node/motion-mask))


(define (node/has-light? node-id)
  (node/check-feature-bit node-id node/light-mask))


(define (node/has-proximity? node-id)
  (node/check-feature-bit node-id node/prox-mask))


(define (node/has-temperature? node-id)
  (node/check-feature-bit node-id node/temp-rh-mask))


(define (node/has-neopixels? node-id)
  (node/check-feature-bit node-id node/neopixel-mask))


(define (node/motion-detected? node-id)
  (let ((data (space/read **state-space** (make-alist 'node node-id 'type "motion"))))
    (if (null? data)
      #f
      (alist-ref 'value (car data)))))


(define (node/use-light? node-id)
  (let ((node-data (node-config/node node-id)))
    (and (not (null? node-data))
         (assv 'light node-data)
         (alist-ref 'light node-data))))


(define (node/lights-off-time node-id)
  (let ((node-data (node-config/node node-id)))
    (if (and (not (null? node-data))
             (assv 'lights-off-in node-data))
      (minutes->seconds (alist-ref 'lights-off-in node-data))
      0)))


;; --------------------------------------------------------------------------------
;; Extension port

(define (node/extension-named node-id extension-name)
  (let ((node-record (node-config/node node-id)))
    (and (assv 'extensions node-record)
         (vector-find (lambda (ext)
                        (equal? (alist-ref 'name ext) extension-name))
                      (alist-ref 'extensions node-record)))))


(define (node/has-extension-named? node-id ext-name)
  (not (not (node/extension-named node-id ext-name))))


(define (node/extension-bit node-id ext-name)
  (let ((ext-data (node/extension-named node-id ext-name)))
    (and ext-data
         (alist-ref 'bit ext-data))))


(define (node/extension-direction node-id ext-name)
  (let ((ext-data (node/extension-named node-id ext-name)))
    (and ext-data
         (alist-ref 'direction ext-data))))


(define (node/configure-extension-port node-id directions pullups)
  (and (byte? directions)
       (byte? pullups)
       (mqtt-queue/send-now (format #f "shc/sensors/command/~A/ext-config" node-id) (format #f "~A ~A" directions pullups))))


(define (node/read-extension-bit node-id bit)
  (mqtt-queue/send-now (format #f "shc/sensors/command/~A/ext-read" node-id) (number->string bit)))


(define (node/write-extension-bit node-id bit value)
    (if (bit-number? bit)
      (mqtt-queue/send-now (format #f "shc/sensors/command/~A/ext-write" node-id) (format #f "~A ~A" bit (if value "1" "0")))))


;; ;; --------------------------------------------------------------------------------
;; ;; Fan control

(define node/fan-bit 7)

(define (node/has-fan? node-id)
  (node/has-extension-named? node-id "fan"))


(define (node/fan-on-off node-id flag)
  (if (node/has-fan? node-id)
    (let ((fan-bit (node/extension-bit node-id "fan")))
      (node/write-extension-bit node-id fan-bit (not flag))))) ; negated because relay control is active low


;; --------------------------------------------------------------------------------
;; Lighting control

;; Set the colour of a nodes neopixel array
(define (node/set-neopixels node-id red green blue)
  (if (node/has-neopixels? node-id)
    (mqtt-queue/send-now (format #f "shc/sensors/command/~A/neopixel" node-id) (format #f "~A ~A ~A" red green blue))))


;; Set the colour of a nodes LED, and optionally blink it (optional off/on millisecond timings)
(define (node/set-led node-id red green blue . blinking)
  (let ((off (if (null? blinking) 0 (car blinking)))
        (on (if (null? blinking) 0 (cadr blinking))))
    (mqtt-queue/send-now (format #f "shc/sensors/command/~A/led" node-id) (format #f "~A ~A ~A ~A ~A" red green blue off on))))


;; ;; --------------------------------------------------------------------------------
;; ;; Managing all nodes

;; Return a vector of all known node ids
(define (node/ids)
  (if (zero? (vector-length (node-config/nodes)))
      (begin (format #t "NO NODES CONFIGURED!!!")
             #())
      (vector-map (lambda (node-def)
                    (alist-ref 'id node-def))
                  (subvector (node-config/nodes) 1))))


(define (node/dump n)
  (let ((data (space/read **state-space** (make-alist 'node n))))
    (format #t "Node ~A: ~A~%" n (node/node-id->node-name n))
    (node/dump-features n)
    (if (null? data)
        (format #t " No data~%")
        (begin
          (format #t " Data:~%")
          (for-each (lambda (tuple)
                      (format #t "  ~A: ~A~%" (alist-ref 'type tuple) (alist-ref 'value tuple)))
                    data)))))

;; --------------------------------------------------------------------------------
;; Node group handling

;; Given the id of a node that just changed it's motion-detected state,
;; see if the group that node is in (could be an implied singleton group) is satisfied based on its combinator.

(define (node/group-satisfied? node-id)
  (let ((group-id (node/node-id->group-id node-id)))
    (if group-id
        (let* ((group (node-config/group group-id))
               (combiner (alist-ref 'combine-using group))
               (nodes (alist-ref 'nodes group))
               (node-motion (map node/motion-detected? (vector->list nodes))))
          (cond ((equal? combiner "or")
                 (eval `(or ,@node-motion)))
                ((equal? combiner "and")
                 (eval `(and ,@node-motion)))
                (else
                 #f)))
        (node/motion-detected? node-id))))



;; ;; --------------------------------------------------------------------------------
;; ;; Handle incoming sensor date from nodes

;; (define (node/save-to-dynamo node-id type value)
;;   (let ((timestamp (format #f "~A ~A" (date->string (date-today)) (time->24hr-string (time-now))))
;;         (sort-key (format #f "~A ~A" node-id type)))
;;     (dynamo/add {timestamp: timestamp sort-key: sort-key node: node-id type: type value: value})
;;     )
;;   )


(define (node/is-moving-at? node)
  (let ((entries (space/read **state-space** (make-alist 'type "motion" 'node node))))
    (and (not (null? entries))
         (alist-ref 'value (car entries)))))


(define (node/update-state node-id type value)
  (space/take! **state-space** (make-alist 'node node-id 'type type))
  (space/write! **state-space** (make-alist 'timestamp (format #f "~A ~A" (date->string (date-today)) (time->24hr-string-with-seconds (time-now)))
                                        'node node-id
                                        'type type
                                        'value value))
  ;; (if (not (equal? type "proximity"))
  ;;   (node/save-to-dynamo node-id type value))
  )


(define (node/get-state node-id type)
  (car (space/read **state-space** (make-alist 'node node-id 'type type))))

(define node/valid-types '("motionstart" "motionend" "motion" "light" "temperature" "humidity" "proximity" "sound" "features" "register"))

(define (node/valid? type)
  (find (lambda (valid-type)
          (equal? type valid-type))
        node/valid-types))


(define (node/capture-state topic payload) 
  (let ((type (sensor-topic->name topic)))
    (if (node/valid? type)
        (let* ((node-id (sensor-topic->node topic))
               (event-topic (format #f "shc/events/data/~A/~A" node-id type)))
          (log-for (debug) "node/capture-state received <~A> on <~A>" payload topic)
          (if (equal? type "register")
              (node/request-features node-id)
              (begin
                (cond ((equal? type "motion")
                       ;; catch motion stopping in the event a motionend got lost
                       (if (and (equal? payload "0")
                                (node/is-moving-at? node-id))
                           (mqtt-queue/send-now (format #f "shc/events/data/~A/motionend" node-id) "0"))
                       ;; catch motion starting in the event a motionstart got lost
                       (if (and (equal? payload "1")
                                (not (node/is-moving-at? node-id)))
                           (mqtt-queue/send-now (format #f "shc/events/data/~A/motionstart" node-id) "0"))
                       (node/update-state node-id "motion" (equal? payload "1")))
                      ((equal? type "motionstart")
                       (node/update-state node-id "motion" #t)
                       (node/update-state node-id "light" (string->number payload)))
                      ((equal? type "motionend")
                       (node/update-state node-id "motion" #f))
                      ((equal? type "features")
                       (node/record-features node-id (string->number payload)))
                      (else
                       (node/update-state node-id type (string->number payload))))
                (mqtt-queue/send-now event-topic payload))))
        (log-for (error) "Invalid sensor report topic: ~A" topic))))


(define (node/node-id->group-name node-id)
  (let ((node-record (node-config/node node-id)))
    (if (assv 'group node-record)
        (alist-ref 'name (node-config/group (alist-ref 'group node-record)))
        (alist-ref 'name node-record))))


(define (node/node-id->node-name node-id)
  (let ((node-record (node-config/node node-id)))
    (alist-ref 'name node-record)))


(define (node/node-id->group-id node-id)
  (let ((node-record (node-config/node node-id)))
    (if (assv 'group node-record)
      (alist-ref 'group node-record)
      #f)))


(define (node/load-config-from-string s)
      (let ((data (read-json s)))
        (let* ((highest-node-id (foldl (lambda (h n)
                                         (let ((i (alist-ref 'id n)))
                                           (if (and i (> i h))
                                               i
                                               h)))
                                       0
                                       (vector->list (alist-ref 'nodes data))))
               (highest-group-id (foldl (lambda (h g)
                                         (let ((i (alist-ref 'id g)))
                                           (if (and i (> i h))
                                               i
                                               h)))
                                       0
                                       (vector->list (alist-ref 'groups data))))
               (node-vector (make-vector (add1 highest-node-id) '()))
               (group-vector (make-vector (add1 highest-group-id) '())))
          (vector-for-each (lambda (n)
                             (or (null? n)
                                 (vector-set! node-vector (alist-ref 'id n) n)))
                           (alist-ref 'nodes data))
          (vector-for-each (lambda (g)
                             (if (not (null? g))
                                 (begin (vector-set! group-vector (alist-ref 'id g) g)
                                        (vector-for-each (lambda (node-id)
                                                           (vector-set! node-vector node-id (alist-update 'group (alist-ref 'id g) (vector-ref node-vector node-id))))
                                                         (alist-ref 'nodes g)))))
                           (alist-ref 'groups data))
          (node-config/nodes! node-vector)
          (node-config/groups! group-vector))))


(define (node/load-config data-dir)
    (let ((in (open-input-file (format #f "~A/node-config.json" data-dir))))
      (if (null? in)
          (error "Invalid Node configuration file.")
          (let ((s (read-string #f in)))
            (close-input-port in)
            (node/load-config-from-string s)))))


(define (node/get-features)
  (vector-for-each (lambda (node-id)
                     (node/request-features node-id))
                   (node/ids)))


(define (node/reset topic payload)
  (vector-for-each (lambda (node-id)
                    (node/set-led node-id 0 0 0)
                    (node/set-neopixels node-id 0 0 0))
                  (node/ids)))


(initializers/register (lambda ()
                         (log-for (info) "  node interaction")
                         (node/load-config (get-environment-variable "SHCPATH"))
                         (mqtt-mux/register "shc/sensors/data/#" node/capture-state)
                         (mqtt-mux/register "shc/reset" node/reset)
                         (node/get-features)))
