;;; -*- scheme -*-
;;; Copyright 2017 Dave Astels. All rights reserved.
;;; Use of this source code is governed by a BSD-style
;;; license that can be found in the LICENSE file.

;;; This file handles fan control

(require-extension srfi-1 medea irregex posix log5scm syslog)
(declare (uses logging))
(declare (unit fan-control))

(define (node->fan-topic node)
  (format #f "shc/fan/~A" node))

(define (fan-off-in node minutes)
  (mqtt-queue/send-in (minutes->seconds minutes) (node->fan-topic node) (json->string #f)))

(define (fan-control/fan-handler topic payload)
  (let ((node (control-topic->node topic))
        (value (read-json payload)))
    (node/fan-on-off node value)))

(define (fan-control/motion-started-handler topic payload)
  (let ((node (sensor-topic->node topic)))
    (if (node/has-fan? node)
        (begin (log-for (debug) "Motion started on sensor ~A, turning on fan" node)
               (node/fan-on-off node #t)))))

(define (fan-control/motion-ended-handler topic payload)
  (let ((node (sensor-topic->node topic)))
    (if (node/has-fan? node)
      (fan-off-in node 5))))

(define node-3-humidity '())

(define (fan-control/humidity-handler topic payload)
  (let ((node (sensor-topic->node topic))
        (humidity (string->number payload)))
    (if (and (node/has-fan? node)
             (> humidity 4000))    ; arbitrarily pic 40% RH as the threshold
        (begin (log-for (debug) "Node ~A humidity is high, fan should be/remain on" node)
               (node/fan-on-off node #t)
               (mqtt-queue/unschedule (node->fan-topic node))
               (fan-off-in node 5)))))


(initializers/register (lambda ()
                         (log-for (info) "  fan control")
                         (mqtt-mux/register "shc/fan/+" fan-control/fan-handler)
                         (mqtt-mux/register "shc/events/data/+/humidity" fan-control/humidity-handler)
                         (mqtt-mux/register "shc/events/data/+/motionstart" fan-control/motion-started-handler)
                         (mqtt-mux/register "shc/events/data/+/motionend" fan-control/motion-ended-handler)))
