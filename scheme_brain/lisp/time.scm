;;; -*- scheme -*-
;;; Copyright 2017 Dave Astels. All rights reserved.
;;; Use of this source code is governed by a BSD-style
;;; license that can be found in the LICENSE file.

;;; This file contains time related utility functions and constants

(require-extension posix log5scm syslog numbers)
(declare (uses logging util))
(declare (unit time))

(define **weekdays** '#("sunday" "monday" "tuesday" "wednesday" "thursday" "friday" "saturday"))

(define seconds-per-day 86400)
(define seconds-per-hour 3600)
(define seconds-per-halfhour 1800)
(define seconds-per-minute 60)

(define millis-per-day (* seconds-per-day 1000))
(define millis-per-hour (* seconds-per-hour 1000))
(define millis-per-minute (* seconds-per-minute 1000))

(define **awake** #f)


(define (round-to-half-hour seconds)
  (floor-quotient seconds seconds-per-halfhour))


(define (seconds-today)
  (let* ((current-time (seconds->local-time (current-seconds)))
         (seconds (vector-ref current-time 0))
         (minutes (vector-ref current-time 1))
         (hours (vector-ref current-time 2)))
    (+ (* hours seconds-per-hour)
       (* minutes seconds-per-minute)
       seconds)))


(define (millis)
  (* (seconds-today) 1000))


(define (hours->millis h)
  (* h millis-per-hour))


(define (hours->seconds h)
  (* h seconds-per-hour))


(define (minutes->millis m)
  (* m 60000))


(define (minutes->seconds m)
  (* m 60))


(define (seconds->millis s)
  (* s 1000))


(define (hms->seconds time)
  (let ((h (car time))
        (m (cadr time))
        (s (caddr time)))
    (+ (* h seconds-per-hour) (* m seconds-per-minute) s)))


(define (seconds->hms seconds)
  (let* ((h (integer (/ seconds seconds-per-hour)))
         (m (integer (/ (- seconds (* h seconds-per-hour)) 60)))
         (s (- seconds (+ (* h seconds-per-hour) (* m seconds-per-minute)))))
    (list h m s)))


(define (start-of-day day)
  (let* ((data (space/read **persistent-space** (make-alist 'type "start-of-day" 'day day))))
    (if (null? data)
        25200
        (alist-ref 'time (car data)))))


(define (end-of-day day)
  (let ((data (space/read **persistent-space** (make-alist 'type "end-of-day" 'day day))))
    (if (null? data)
      84600
      (alist-ref 'time (car data)))))


(define (after? t1 t2)
  (> t1 t2))


(define (before? t1 t2)
  (< t1 t2))


(define (date->string d)
  (string-join (map number->string d) "/"))


(define (hms->string hms)
  (let* ((hour (first hms))
         (pm? (> hour 12))
         (hm (list (if pm? (- hour 12) hour)
                    (second hms))))
    (format #f "~A ~A"
            (string-join (map (lambda (n) (string-pad (number->string n) 2 #\0)) hm) ":")
            (if pm? "pm" "am"))))


(define (time->24hr-string t)
  (string-join (map (lambda (n)
                      (string-pad (number->string n) 2 #\0))
                    (list-head t 2))
               ":"))


(define (time->24hr-string-with-seconds t)
  (string-join (map (lambda (n)
                      (string-pad (number->string n) 2 #\0))
                    t)
               ":"))


(define (dark-out?)
  (let ((now (seconds-today)))
    (or (> now (alist-ref 'time (car (space/read **state-space** (make-alist 'type 'sunset)))))
        (< now (alist-ref 'time (car (space/read **state-space** (make-alist 'type 'sunrise))))))))


;;; This does not work when end of day is >= midnight

(define (usually-awake-at? t)
  (or **awake**
      (let ((day (day-of-week)))
        (and (after? t (start-of-day day))
             (before? t (end-of-day day))))))

(define (set-awake a)
  (set! **awake** a))

(define (extract-time date-time-vector)
  (list (vector-ref date-time-vector 2) (vector-ref date-time-vector 1) (vector-ref date-time-vector 0)))


(define (time-now)
  (extract-time (seconds->local-time)))


(define (extract-date date-time-vector)
  (list (+ 1900 (vector-ref date-time-vector 5)) (vector-ref date-time-vector 4) (vector-ref date-time-vector 3)))


(define (date-in-days n)
  (extract-date (seconds->local-time (+ (current-seconds) (* n seconds-per-day)))))


(define (date-today)
  (date-in-days 0))


(define (date-tomorrow)
  (date-in-days 1))


(define (date-yesterday)
  (date-in-days -1))


(define (day-of-week . maybe-day-offset)
  (let ((seconds (if (not (null? maybe-day-offset))
                     (+ (current-seconds) (* seconds-per-day (car maybe-day-offset)))
                     (current-seconds))))
    (vector-ref **weekdays** (vector-ref (seconds->local-time seconds) 6))))
