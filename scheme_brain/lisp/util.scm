;;; -*- scheme -*-
;;; Copyright 2017 Dave Astels. All rights reserved.
;;; Use of this source code is governed by a BSD-style
;;; license that can be found in the LICENSE file.
;;;
;;; This file provides some utilities

(require-extension srfi-1)
(declare (unit util))

;; --------------------------------------------------------------------------------
;; General utilities


(define (byte? n)
  (and (number? n)
       (>= n 0)
       (< n 256)))


(define (bit-number? n)
  (and (number? n)
       (>= n 0)
       (< n 8)))

(define (make-alist key value . more-keys-and-values)
  (if (null? more-keys-and-values)
      (alist-cons key value '())
      (alist-cons key value (apply make-alist more-keys-and-values))))

(define (vector-for-each proc vec)
  (let ((len (vector-length vec)))
    (do ((i 0 (fx+ i 1)))
        ((fx>= i len))
      (proc (vector-ref vec i)))))

(define (vector-map proc vec)
  (let* ((len (vector-length vec))
         (result (make-vector len)))
    (do ((i 0 (fx+ i 1)))
        ((fx>= i len) result)
      (vector-set! result i (proc (vector-ref vec i))))))

(define (vector-find f v)
  (let loop ((index 0)
             (limit (vector-length v)))
    (cond ((eqv? index limit)
           #f)
          ((f (vector-ref v index))
           (vector-ref v index))
          (else
           (loop (add1 index) limit)))))
