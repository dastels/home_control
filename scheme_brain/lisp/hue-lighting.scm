;;; -*- scheme -*-
;;; Copyright 2017 Dave Astels. All rights reserved.
;;; Use of this source code is governed by a BSD-style
;;; license that can be found in the LICENSE file.
;;;
;;; Hue lighting device

(require-extension srfi-1 medea http-client intarweb uri-common irregex posix log5scm syslog)
(declare (uses logging util))
(declare (unit hue-lighting))

;; --------------------------------------------------------------------------------
;;  Config functions

(define hue/config '())
(define hue/hue-bridge-ip "10.215.222.117")


(define (hue/group->lights group-name)
  (let ((group (vector-find (lambda (g)
                              (equal? (alist-ref 'name g) group-name))
                            (alist-ref 'groups hue/config))))
    (if group
      (alist-ref 'lights group)
      #())))


(define (hue/node->lights node-id)
  (let ((group-name (node/node-id->group-name node-id)))
    (if group-name
      (hue/group->lights group-name))))


;; --------------------------------------------------------------------------------
;; Utility functions

(define (hue/unitize rgb)
  (map (lambda (p) (/ p 255.0))
       rgb))


(define (hue/gamma-correct rgb)
  (map (lambda (colour-part)
         (if (> colour-part 0.04045)
           (expt (/ (+ colour-part 0.055) 1.055) 2.4)
           (/ colour-part 12.92)))
       (hue/unitize rgb)))


(define (hue/rgb->xy r g b)
  (let* ((corrected-rgb (hue/gamma-correct (list r g b)))
         (r (first corrected-rgb))
         (g (second corrected-rgb))
         (b (third corrected-rgb))
         (x (+ (* r 0.664511) (* g 0.154324) (* b 0.162028)))
         (y (+ (* r 0.283881) (* g 0.668433) (* b 0.047685)))
         (z (+ (* r 0.000088) (* g 0.072310) (* b 0.986039)))
         (sum-xyz (+ x y z)))
    (list (/ x sum-xyz)
          (/ y sum-xyz))))

(define (hue/fetch-hue-bridge-ip)
  (let-values (((json b c) (with-input-from-request "https://www.meethue.com/api/nupnp" #f read-string)))
    (set! hue/hue-bridge-ip (alist-ref 'internalipaddress (vector-ref (read-json json) 0)))))


(define (hue/make-lights-url . args)
  (let ((id (if (null? args)
              ""
              (format #f "/~A" (car args))))
        (command (if (or (null? args) (null? (cdr args)))
                   ""
                   (format #f "/~A" (cadr args)))))
    (format #f "http://~A/api/~A/lights~A~A"
            hue/hue-bridge-ip
            (alist-ref 'user hue/config)
            id
            command)))


(define (hue/make-groups-url)
  (format #f "http://~A/api/~A/groups" hue/hue-bridge-ip (alist-ref 'user hue/config)))


(define (hue/colour-light? light-id)
  (let ((found (vector-find (lambda (l)
                              (eqv? (alist-ref 'number l) light-id))
                            (alist-ref 'lights hue/config))))
	(and found
		 (irregex-match '(w/nocase (: (* any) "color" (* any))) (alist-ref 'type found)))))


(define (hue/colour-group? group-name)
  (let ((lights (hue/group->lights group-name)))
    (if (and lights
             (vector? lights))
        (every hue/colour-light? (vector->list lights))
        #f)))


;; --------------------------------------------------------------------------------
;; Public functions

(define (hue/light-id->name light-id)
  (let ((found (vector-find (lambda (l)
                              (eqv? (alist-ref 'number l) light-id))
                            (alist-ref 'lights hue/config))))
    (if (not found)
        ""
        (alist-ref 'name found))))

;; Single light control

(define (hue/percent->brightness p)
  (fx/ (* p 254) 100))


(define (net/put uri json)
  (log-for (debug) "Sending ~S to ~A" json uri)
  (let* ((uri (uri-reference uri))
         (req (make-request method: 'PUT
                            uri: uri
                            headers: (headers '((content-type application/json))))))
    (with-input-from-request req json read-string))  )


(define (hue/on-off light-id on-off)
  (log-for (debug) "HUE: Turning ~A light ~A" (if on-off "on" "off") (hue/light-id->name light-id))
  (net/put (hue/make-lights-url light-id 'state)
           (json->string (make-alist 'on on-off))))


(define (hue/off light-id)
  (log-for (debug) "HUE: Turning off light ~A" (hue/light-id->name light-id))
  (net/put (hue/make-lights-url light-id 'state)
           (json->string (make-alist 'on #f))))


(define (hue/on light-id percent-level)
  (log-for (debug) "HUE: Turning on light ~A" (hue/light-id->name light-id))
  (net/put (hue/make-lights-url light-id 'state)
           (json->string (make-alist'on #t 'bri (hue/percent->brightness percent-level)))))


(define (hue/colour light-id r g b percent-brightness)
  (let ((brightness (hue/percent->brightness percent-brightness)))
	(log-for (debug) "HUE: Setting colour on light ~A at brightness ~A" (hue/light-id->name light-id) brightness)
    (net/put (hue/make-lights-url light-id 'state)
             (json->string (make-alist 'on #t 'bri brightness 'xy (list->vector (hue/rgb->xy r g b)))))))


(define (hue/on? light-id)
  (let-values (((json b c) (with-input-from-request (hue/make-lights-url light-id) #f read-string)))
    (alist-ref 'on (alist-ref 'state (read-json json)))))

;; --------------------------------------------------------------------------------
;; Group control

(define (hue/group-off group-name)
  (vector-for-each (lambda (light-id)
                     (hue/off light-id))
                   (hue/group->lights group-name)))


(define (hue/group-on group-name brightness)
  (vector-for-each (lambda (light-id)
                     (hue/on light-id brightness))
                   (hue/group->lights group-name)))


(define (hue/group-colour group-name r g b p)
  (vector-for-each (lambda (light-id)
                     (hue/colour light-id r g b p))
                   (hue/group->lights group-name)))


(define (hue/handler topic payload)
  (log-for (debug) "In HUE handler: ~S ~S" topic payload)
  (let* ((node (control-topic->node topic))
         (group-name (node/node-id->group-name node))
         (value (read-json payload)))
    (log-for (debug) "Setting ~A (~A) to ~A" node group-name value)
    (if (eqv? (vector-length value) 1)
        (if (zero? (vector-ref value 0))
            (hue/group-off group-name)
            (hue/group-on group-name (vector-ref value 0)))
        (let ((r (vector-ref value 0))
              (g (vector-ref value 1))
              (b (vector-ref value 2))
              (p (vector-ref value 3)))
          (log-for (debug) "Setting color group ~A" group-name)
          (if (or (zero? p)
                  (zero? (+ r g b)))
              (hue/group-off group-name)
              (hue/group-colour group-name r g b p))))))


(define (hue/node-lights-on? node)
  (let ((lights (hue/node->lights node)))
    (if (null? lights)
      #f
      (every hue/on? (vector->list lights)))))


;; --------------------------------------------------------------------------------
;; Intialization


(define (hue/scan-lights)
   (let-values (((json b c) (with-input-from-request (hue/make-lights-url) #f read-string)))
     (let ((response (read-json json)))
       (list->vector (map (lambda (key)
                            (let ((light (alist-ref key response))
                                  (id (string->number (symbol->string key))))
                              (make-alist 'number id
                                          'modelid (alist-ref 'modelid light)
                                          'type (alist-ref 'type light)
                                          'name (alist-ref 'name light))))
                          (map car response))))))


(define (hue/scan-groups)
  (let-values (((json b c) (with-input-from-request (hue/make-groups-url) #f read-string)))
    (let ((response (read-json json)))
      (list->vector (map (lambda (key)
                           (let ((g (alist-ref key response)))
                             (make-alist 'name (alist-ref 'name g)
                                         'lights (vector-map string->number (alist-ref 'lights g)))))
                         (map car response))))))


(define (hue/load-config)
  (let* ((data-dir (get-environment-variable "SHCPATH"))
         (in (open-input-file (format #f "~A/hue-config.json" data-dir))))
    (if (null? in)
      (error "Can't open hue-config.json.")
      (let ((data (read-json in)))
        (close-input-port in)
        (if (null? data)
            (error (format #f "Error parsing in hue-config.json: ~A" data))
            (set! hue/config data))))))


(define (hue/reset topic payload)
  (vector-for-each (lambda (light)
                    (hue/off (alist-ref 'number light)))
                  (alist-ref 'lights hue/config)))


(initializers/register (lambda ()
                         (log-for (info) "  hue")
                         (hue/fetch-hue-bridge-ip)
                         (log-for (info) "    bridge-ip is ~A" hue/hue-bridge-ip)
                         (hue/load-config)
                         (set! hue/config (alist-update! 'lights (hue/scan-lights) hue/config))
                         (set! hue/config (alist-update! 'groups (hue/scan-groups) hue/config))
                         (lighting/register-light-state-function hue/node-lights-on?)
                         (mqtt-mux/register "shc/lighting/#" hue/handler)
                         ;(mqtt-mux/register "shc/reset" hue/reset)
                         ))

