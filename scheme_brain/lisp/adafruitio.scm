;;; Copyright 2016 Dave Astels. All rights reserved.
;;; Use of this source code is governed by a BSD-style
;;; license that can be found in the LICENSE file.

;;; This file provides some helpful functions for supporting the AdaFruit IO service

(require-extension srfi-1 medea http-client intarweb uri-common posix log5scm syslog)
(declare (uses logging util mqtt mqtt-mux))
(declare (unit adafruitio))

(define **AIO-KEY** (get-environment-variable "AIO_KEY"))
(define **AIO-USER** (get-environment-variable "AIO_USER"))
(define **AIO-URI** "https://io.adafruit.com/api/v2")

;; Convert a date (year month day) to an adafruit io search timestamp
(define (aio/date-time->adatime date time)
  (format #f "~A-~A-~AT~A:~A:~A.000Z" (first date) (second date) (third date) (first time) (second time) (third time)))

;; Convert an adafruit io timestamp to the number of seconds since midnith this morning
(define (aio/adatime->seconds adatime)
  (let* ((time-string (cadr (string-split adatime "T")))
         (time-parts (map string->number (string-split (car (string-split time-string ".")) ":"))))
    (+ (* (first time-parts) 3600)
       (* (second time-parts) 60)
       (third time-parts))))

;; Convert an adafruit io timestamp to a date (year month day)
(define (aio/adatime->date adatime)
  (let* ((date-string (car (string-split adatime "T"))))
    (map string->number (string-split date-string "-"))))


;; Send data to Adafruit IO
(define (aio/send user feed value)
  (let* ((payload (json->string (make-alist 'value value)))
         (uri (uri-reference (format #f "~A/~A/feeds/~A/data" **AIO-URI** user feed)))
         (req (make-request method: 'POST
                            uri: uri
                            headers: (headers (list '(content-type application/json)
                                                    (list 'X-AIO-Key **AIO-KEY**))))))
    (with-input-from-request req payload read-string)))


(define (aio/send-motion start-end node-id)
  (aio/send **AIO-USER** "motion" (format #f "~A|~A" start-end (node/node-id->node-name node-id))))


(define (aio/motionstart-handler topic payload)
  (aio/send-motion "start" (sensor-topic->node topic)))


(define (aio/motionend-handler topic payload)
  (aio/send-motion "end" (sensor-topic->node topic)))


;; (initializers/register (lambda ()
;;                          (log-for (info) "  Adafruit IO")
;;                          (mqtt-mux/register "shc/events/data/+/motionstart" aio/motionstart-handler)
;;                          (mqtt-mux/register "shc/events/data/+/motionend" aio/motionend-handler)))

