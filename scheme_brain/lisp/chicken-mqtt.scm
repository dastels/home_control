;;; -*- scheme -*-
;;; Copyright 2017 Dave Astels. All rights reserved.
;;; Use of this source code is governed by a BSD-style
;;; license that can be found in the LICENSE file.
;;;
;;; Chicken Scheme interface to MQTT

(require-extension srfi-18 posix medea log5scm syslog)
(declare (uses logging))
(declare (unit chicken-mqtt))

(require-extension foreign)

#>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "MQTTClient.h"

#define ADDRESS     "tcp://localhost:1883"
#define CLIENTID    "SmartHomeMQTT"
#define QOS         1
#define TIMEOUT     10000L


MQTTClient client = NULL;

typedef struct {
  char *topic;
  int callback_fd;
} subscription_t;

subscription_t subscriptions[32];
int number_of_subscriptions = 0;


int find_subscription_for(char *pattern)
{
  for (int subscription_number = 0; subscription_number < number_of_subscriptions; subscription_number++) {
	if (strcmp(subscriptions[subscription_number].topic, pattern) == 0) {
	  return subscription_number;
	}
  }
  return -1;
}


int find_matching_subscription(char *topic, int start_at)
{
  for (int subscription_number = start_at; subscription_number < number_of_subscriptions; subscription_number++) {
	char *topic_ptr;
	char *template_ptr;
	for (topic_ptr = topic, template_ptr = subscriptions[subscription_number].topic; *topic_ptr && *template_ptr; topic_ptr++, template_ptr++) {
	  if (*topic_ptr == *template_ptr) {                  // same chars, keep going
		continue;
	  } else if (*template_ptr == '#') {                  // match: the "rest of the topic" wildcard
		return subscription_number;
	  } else if (*template_ptr == '+') {                  // consume one part of the topic: the "one section" wildcard
		while (*topic_ptr && *topic_ptr != '/') {
		  topic_ptr++;
		}
		topic_ptr--;
	  } else {                                            // not a match: different chars, no wildcard
		break;
	  }
	}
	if (*topic_ptr == 0 && *template_ptr == 0) {          // match: both hit the end
	  return subscription_number;
	}
  }
  return -1;                                              // couldn't find a match
}


int message_received(void *context, char *topicName, int topicLen, MQTTClient_message *message)
{
  //  printf("Message received on %s\n", topicName);
  if (number_of_subscriptions > 0) {
	int subscription_id = find_matching_subscription(topicName, 0);
	while (subscription_id >= 0) {
	  int len = strlen(topicName) + message->payloadlen + 6;  // "topic" "payload"\n
	  char buf[len];
	  sprintf(buf, "\"%s\" \"", topicName);
	  char *payloadptr = message->payload;
	  for (int i = 0; i < message->payloadlen; i++) {
		buf[strlen(topicName) + 4 + i] = *payloadptr++;
	  }
	  buf[len - 2] = 34; // "
	  buf[len - 1] = 0; // NULL

	  int ignore = write(subscriptions[subscription_id].callback_fd, buf, len-1);
	  subscription_id = find_matching_subscription(topicName, subscription_id + 1);
	}
  }

  MQTTClient_freeMessage(&message);
  MQTTClient_free(topicName);
  return 1;
}


int mqtt_setup()
{
  if (client == NULL) {
    int rc;
    MQTTClient_connectOptions conn_opts = MQTTClient_connectOptions_initializer;
    MQTTClient_create(&client, ADDRESS, CLIENTID, MQTTCLIENT_PERSISTENCE_NONE, NULL);
    conn_opts.keepAliveInterval = 20;
    conn_opts.cleansession = 1;
    MQTTClient_setCallbacks(client, NULL, NULL, message_received, NULL);
    if ((rc = MQTTClient_connect(client, &conn_opts)) != MQTTCLIENT_SUCCESS) {
      C_return(MQTTCLIENT_FAILURE);
    } else {
      C_return(MQTTCLIENT_SUCCESS);
    }
  } else {
    C_return(MQTTCLIENT_FAILURE);
  }
}


int mqtt_shutdown()
{
  if (client != NULL) {
    MQTTClient_disconnect(client, 10000);
    MQTTClient_destroy(&client);
    C_return(MQTTCLIENT_SUCCESS);
  } else {
    C_return(MQTTCLIENT_FAILURE);
  }
}


int mqtt_publish(char *topic,  char *message)
{
  if (client != NULL) {
    MQTTClient_message pubmsg = MQTTClient_message_initializer;
    MQTTClient_deliveryToken token;
    pubmsg.payload = message;
    pubmsg.payloadlen = strlen(message);
    pubmsg.qos = QOS;
    pubmsg.retained = 0;
    MQTTClient_publishMessage(client, topic, &pubmsg, &token);
    C_return(MQTTClient_waitForCompletion(client, token, TIMEOUT));
  } else {
    C_return(MQTTCLIENT_FAILURE);
  }
}


int mqtt_internal_subscribe(char *topic, int handler_fd)
{
  if (client != NULL) {
	if (find_subscription_for(topic) == -1) {
	  subscriptions[number_of_subscriptions].topic = strdup(topic);
	  subscriptions[number_of_subscriptions].callback_fd = handler_fd;
	  number_of_subscriptions++;

	  MQTTClient_subscribe(client, topic, QOS);
	  C_return(MQTTCLIENT_SUCCESS);
	} else {
	  C_return(MQTTCLIENT_FAILURE);
	}
  } else {
    C_return(MQTTCLIENT_FAILURE);
  }
}


int mqtt_unsubscribe(char *topic)
{
  if (client != NULL) {
	int sub_number = find_subscription_for(topic);
	if (sub_number > -1) {
	  C_return(MQTTClient_unsubscribe(client, topic));
	  free(subscriptions[sub_number].topic);
	  subscriptions[sub_number].topic = NULL;
	  subscriptions[sub_number].callback_fd = -1;
	} else {
	  C_return(MQTTCLIENT_SUCCESS);
	}
  } else {
    C_return(MQTTCLIENT_FAILURE);
  }
}

<#

(define mqtt/setup
  (foreign-lambda int "mqtt_setup"))

(define mqtt/shutdown
  (foreign-lambda int "mqtt_shutdown"))


(define mqtt/publish
  (foreign-lambda int "mqtt_publish" c-string c-string))


(define mqtt-internal/subscribe
  (foreign-lambda int "mqtt_internal_subscribe" c-string int))


(define mqtt/unsubscribe
  (foreign-lambda int "mqtt_unsubscribe" c-string))


(define (read-a-string-from p)
  (let ((s (read p)))
    (if (or (eof-object? s) (string? s))
        s
        (read-a-string-from p))))

(define (mqtt/subscribe topic handler-f)
 (log-for (mqtt-debug) "Subscribing to ~A" topic)
 (let-values (((read-end write-end) (create-pipe)))
  (if (zero? (mqtt-internal/subscribe topic write-end))
	(let* ((p (open-input-file* read-end))
		   (mqtt-thread (make-thread (lambda ()
									  (let loop ()
									   (or (char-ready? p)
										(thread-wait-for-i/o! read-end #:input))
									   (let ((concrete-topic (read p)))
										(if (not (eof-object? concrete-topic))
										  (begin (if (string? concrete-topic)
													(begin
													 (let ((payload (read p)))
													  (handler-f concrete-topic payload))))
										   (loop)))
										(close-input-port p)))))))
	 (thread-start! mqtt-thread)))))
