;;; -*- scheme -*-
;;; Copyright 2017 Dave Astels. All rights reserved.
;;; Use of this source code is governed by a BSD-style
;;; license that can be found in the LICENSE file.

;;; This file handles learning the overall activity pattern to predict whether everyone has gone to bed

(require-extension log5scm syslog medea extras posix-extras numbers) 
(declare (uses util logging mqtt-mux time))
(declare (unit awake-time-tracker))


(define (awake-time-tracker/motion-handler topic payload)
  (let* ((template (make-alist 'type 'motion-tracking 'dow (day-of-week)))
         (current-data (space/read **state-space** template)))
    (unless (null? current-data)
      (space/take! **state-space** template)
      (let* ((count-data (car current-data))
             (counts (alist-ref 'counts count-data))
             (half-hour-interval (round-to-half-hour (seconds-today)))
             (current-count (vector-ref counts half-hour-interval)))
        (vector-set! counts half-hour-interval (add1 current-count))
        (space/write! **state-space** (alist-update! 'counts counts count-data))))))


(define (awake-time-tracker/motion-file-name date dow)
  (let ((data-dir (get-environment-variable "SHCPATH"))
        (date-string (string-join (map number->string date) "-")))
    (format #f "~A/~A_~A_motion-data.txt" data-dir date-string dow)))


(define (awake-time-tracker/write-to-file fname data)
  (let ((out (open-output-file fname)))
    (write-json data out)
    (close-output-port out)))

;;; At the moment this is stored as a motion count per half hour increment
;;; Moving to a per node count

;; For now just save yesterday's data to a file and clear it out
(define (awake-time-tracker/daily-processing topic payload)
  (let* ((dow-yesterday (day-of-week -1))
         (template (make-alist 'type 'motion-tracking 'dow dow-yesterday))
         (data (car (space/read **state-space** template)))
         (filename (awake-time-tracker/motion-file-name (date-yesterday) dow-yesterday)))
    (awake-time-tracker/reset)
    (format #t "Dumping motion history to ~A" filename)
    (format #t "~A~%" (string-join (map number->string (vector->list (alist-ref 'counts data))) " "))
    (awake-time-tracker/write-to-file filename (alist-ref 'counts data))))


(define (awake-time-tracker/reset)
  (let ((dow-yesterday (day-of-week -1))
        (template (make-alist 'type 'motion-tracking 'dow (day-of-week -1)))
        (data (list->vector (map (lambda (x)
                                   0)
                                 (iota 48)))))
    (space/take! **state-space** template)
    (space/write! **state-space** (make-alist 'type 'motion-tracking
                                              'dow dow-yesterday
                                              'counts data))))


(initializers/register (lambda ()
                         (log-for (info) "  awake time tracker")
                         (mqtt-mux/register "shc/events/data/+/motionstart" awake-time-tracker/motion-handler)
                         (mqtt-mux/register "shc/daily" awake-time-tracker/daily-processing)))

