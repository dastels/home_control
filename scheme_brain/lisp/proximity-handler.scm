;;; -*- scheme -*-
;;; Copyright 2017 Dave Astels. All rights reserved.
;;; Use of this source code is governed by a BSD-style
;;; license that can be found in the LICENSE file.

;; This file handles proximity detector processing

(require-extension log5scm syslog) 
(declare (uses logging space initializers mqtt-queue node))
(declare (unit proximity-handler))


;; Shut down all proximity measurement on reset
(define (proximity/reset topic payload)
  (vector-for-each (lambda (node-id)
                     (if (node/has-proximity? node-id)
                         (proximity/interval node-id 0)))
                   (node/ids)))


;; Send a command to the host to start/stop measuring proximity on the specified node
(define (proximity/interval node interval)
  (log-for (prox-debug) "Setting proximity report interval on node ~A to ~A" node interval)
  (mqtt-queue/send-now (format #f "shc/sensors/command/~A/proximity" node)
                       (format #f "~A" interval)))


;; If motion is detected on a node have it start watching proximity and cancel any schedule command to turn of the proximity lighting
(define (proximity/motionstart-handler topic payload)
  (let ((node (sensor-topic->node topic)))
    (if (node/has-proximity? node)
      (begin (log-for (prox-debug) "Starting to watch proximity on node ~A" node)
             (mqtt-queue/unschedule (proximity/node->lights-off-topic node))
             (proximity/interval node 300)))))


;; If motion is no longer detected on a node have it stop watching proximity
(define (proximity/motionend-handler topic payload)
  (let ((node (sensor-topic->node topic)))
    (if (node/has-proximity? node)
	  (proximity/interval node 2000))))


(define (proximity/light-off node)
  (if (node/has-neopixels? node)
      (begin (log-for (prox-debug) "Turning off node ~A neopixels" node)
             (node/set-neopixels node 0 0 0)
             (proximity/interval node 0)
             (space/take! **state-space** (make-alist 'type "prox-light-on" 'node node)))))


;; Turn off the spot lighting because the timeout expired
(define (proximity/lights-off-handler topic payload)
  (let ((node (string->number (third (string-split topic "/")))))
    (proximity/light-off node)))


(define (proximity/node->lights-off-topic node)
  (format #f "shc/events/~A/proximity-lights-off" node))


;; Turn off the spot lighting when motion stops... this is a failsafe
;; (define (proximity/motion-end-handler topic payload)
;;   (let ((node (string->number (fourth (string-split topic "/")))))
;;     (mqtt-queue/unschedule (proximity/node->lights-off-topic node))
;;     (proximity/light-off node)))


;; check if something is in proximity
(define (proximity/close-enough proximity-value)
  (<= proximity-value 500))


;; Handle a proximity measurement
;;  - turn on the spot lighting if in proximity
;;  - schedule turning spot lighting off and stopping proximity monitoring
(define (proximity/handler topic payload)
  (let* ((node (sensor-topic->node topic))
         (distance (string->number payload))
         (close (proximity/close-enough distance))
         (lights-off-topic (proximity/node->lights-off-topic node))
         (light-on-template (make-alist 'type "prox-light-on" 'node node))
         (light-on? (not (null? (space/read **state-space** light-on-template)))))
    (if (< distance 8000) (log-for (prox-debug) "Distance from node ~A is ~Amm" node distance))
    (cond (close
           (if (not light-on?)
               (begin (log-for (prox-debug) "Turning on node ~A neopixels" node)
                      (node/set-neopixels node 255 255 255)
                      (space/write! **state-space** light-on-template)))
           (proximity/interval node 2000)
           (mqtt-queue/unschedule lights-off-topic))
          ((and (not close)
                light-on?
                (not (mqtt-queue/scheduled? lights-off-topic)))
           (log-for (prox-debug) "Scheduling neopixel turn-off for node ~A" node)
           (mqtt-queue/send-in 60 lights-off-topic "0")))))



;; --------------------------------------------------------------------------------
;; Initialization

(initializers/register (lambda ()
                         (log-for (info) "  proximity handling")
                         (mqtt-mux/register "shc/events/data/+/motionstart" proximity/motionstart-handler)
                         (mqtt-mux/register "shc/events/data/+/motionend" proximity/motionend-handler)
                         (mqtt-mux/register "shc/events/+/proximity-lights-off" proximity/lights-off-handler)
                         ;; (mqtt-mux/register "shc/events/data/+/motionend" proximity/motion-end-handler)
                         (mqtt-mux/register "shc/events/data/+/proximity" proximity/handler)
                         (mqtt-mux/register "shc/reset" proximity/reset)))

