;;; -*- scheme -*-
;;; Copyright 2017 Dave Astels. All rights reserved.
;;; Use of this source code is governed by a BSD-style
;;; license that can be found in the LICENSE file.

;;; This file handles the system heartbeat

(require-extension log5scm syslog medea extras posix-extras) 
(declare (uses util logging mqtt-mux))
(declare (unit heartbeat))


(define (heartbeat/handler topic payload)
  (log-for (info) "~A: Heartbeat!" (time->string (seconds->local-time))))

(initializers/register (lambda ()
                         (log-for (info) "  setting up heartbeat")
                         (mqtt-mux/register "shc/heartbeat" heartbeat/handler)
                         (mqtt-queue/send-every-in 60
                                                   60
                                                   "shc/heartbeat" "")))
