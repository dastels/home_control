;;; -*- scheme -*-
;;; Copyright 2017 Dave Astels. All rights reserved.
;;; Use of this source code is governed by a BSD-style
;;; license that can be found in the LICENSE file.

(declare (unit ui-support))

(define (string->naked-sym s)
  (intern (format #f "~a:" s)))


(define (node-value-query-handler topic payload)
  (let ((command payload)
		(node-id (command-topic->node (topic->sensor-info topic))))
	(if (equal? command "get-values")
        (let* ((entries (space/read state-space (make-alist 'node node-id)))
               (json (lisp->json (fold-left (lambda (f e)
                                              (set-slot! f (string->naked-sym (alist-ref 'type e)) (alist-ref 'value e))
                                              f)
                                            '()
                                            entries))))
          (mqtt/publish (format #f "shc/node/~A/values" node-id) 0 json)))))


(initializers/register (lambda ()
                         (log-for (info) "  ui support")
                         (mqtt-mux/register "shc/node/+" node-value-query-handler)))
