;;; -*- scheme -*-
;;; Copyright 2017 Dave Astels. All rights reserved.
;;; Use of this source code is governed by a BSD-style
;;; license that can be found in the LICENSE file.

;;; This file handles motion based lighting control

(require-extension log5scm syslog)
(declare (uses logging))
(declare (unit lighting-control))

(define (lighting-control/appropriate-light-level)
  (if (usually-awake-at? (seconds-today))
    100
    5))


(define (lighting-control/appropriate-colour)
  (if (usually-awake-at? (seconds-today))
	  '(#xff #xff #xc0 100)
	  '(#x1f #x00 #x00 5)))


(define (lighting-control/appropriate-time-until-off node-id)
  (if (usually-awake-at? (seconds-today))
      (node/lights-off-time node-id)
      60))


(define (lighting-control/light-at-last-turnon node)
  (let ((light-level (space/read **state-space** (make-alist 'type "light-at-last-turnon" 'node node))))
    (if (null? light-level)
        0
        (alist-ref 'value (car light-level)))))


(define (lighting-control/light-needed-threshold node)
  (let ((threshold (space/read **persistent-space** (make-alist 'type "light-needed-threshold" 'node node))))
    (if (null? threshold)
        200
        (alist-ref 'value (car threshold)))))



(define (lighting-control/light-needed? node light-level)
  (or (not (node/use-light? node))
      (dark-out?)
      (let ((threshold (lighting-control/light-needed-threshold node)))
        (if (lighting/node-lights-on? node)
            (< (lighting-control/light-at-last-turnon node) threshold)
            (< light-level threshold)))))

(define (explain-why-light-needed node light-level)
  (cond ((not (node/use-light? node))
         (format #f "Node ~A always needs light" node))
        ((dark-out?)
         "It is dark out")
        (else
         (format #f "It was too dark at node ~A (~A, threshold is ~A)" node light-level (lighting-control/light-needed-threshold node)))))


;;; TODO remove direct reference to the HUE module
(define (lighting-control/motion-started-handler topic payload)
  (let* ((node (sensor-topic->node topic))
         (current-light (string->number payload))
         (need-more-light? (lighting-control/light-needed? node current-light)))
    (log-for (info) "Motion started on sensor ~A~A" node (if need-more-light? ": light is required" ""))
    (when need-more-light?
        (log-for (info) "Reason light is needed: ~A" (explain-why-light-needed node current-light)))
    (node/set-led node 32 0 0)
    ; turn on lights if required
    (when (and (node/group-satisfied? node)
               need-more-light?)
      (lighting/cancel-for node)     ; cancel the scheduled turn-off command
      (if (hue/node->lights node)
          (begin (space/take! **state-space** (make-alist 'type 'light-at-last-turnon 'node node))
                 (space/write! **state-space** (make-alist 'type 'light-at-last-turnon 'node node 'value current-light))))
      (if (hue/colour-group? (node/node-id->group-name node))
          (apply lighting/colour (cons node (lighting-control/appropriate-colour)))
          (lighting/on node (lighting-control/appropriate-light-level))))))



(define (lighting-control/motion-ended-handler topic payload)
  (let ((node (sensor-topic->node topic)))
    (node/set-led node 0 0 0)
    (log-for (info) (format #f "Motion ended on sensor ~A" node))
    (unless (node/group-satisfied? node)
      (when (lighting/lights-off-scheduled? node)
        (lighting/cancel-for node))
      (lighting/off-in node (lighting-control/appropriate-time-until-off node)))))


(define (lighting-control/sound-handler topic payload)
  (let ((node (sensor-topic->node topic)))
    (when (lighting/lights-off-scheduled? node)
      (log-for (info) "Sound detected at node ~A. Restarting turn-off timer." node)
      (lighting/cancel-for node)
      (lighting/off-in node (node/lights-off-time node)))))


(initializers/register (lambda ()
                         (log-for (info) "  light control")
                         (mqtt-mux/register "shc/events/data/+/sound" lighting-control/sound-handler)
                         (mqtt-mux/register "shc/events/data/+/motionstart" lighting-control/motion-started-handler)
                         (mqtt-mux/register "shc/events/data/+/motionend" lighting-control/motion-ended-handler)))
