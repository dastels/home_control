;;; -*- scheme -*-
;;; Copyright 2017 Dave Astels. All rights reserved.
;;; Use of this source code is governed by a BSD-style
;;; license that can be found in the LICENSE file.

;;; This file impliments a tuplespace.
;;; A tuple here is an alist.
;;; A space is stored as a list of three elements:
;;;   - the name of the space
;;;   - a list storing all entries
;;;   - the mutex for the space
;;; It's not overly efficient, so most appropriate for small spaces.
;;; Keys to the entry pairs are assumed to be symbols for efficient matching


(require-extension srfi-1 srfi-18 medea)
(declare (unit space))

(define (match-tuple entry template)
  (fold (lambda (x y) (and x y))
        #t 
        (map (lambda (pair)
               (let ((key (car pair)))
                 (and (assv key entry)
                      (equal? (cdr (assv key entry))
                              (cdr pair)))))
             template)))


(define (space/name space) (first space))
(define (space/soup space) (second space))
(define (space/soup! space new-soup) (set-car! (cdr space) new-soup))
(define (space/mutex space) (third space))


(define (make-space n)
  (list  n '() (make-mutex n)))


(define (space/clear space)
  (mutex-lock! (space/mutex space))
  (space/soup! space '())
  (mutex-unlock! (space/mutex space)))


(define (space/persist-to space fname-or-port)
  (mutex-lock! (space/mutex space))
  (let ((out (if (string? fname-or-port)
                  (open-output-file fname-or-port)
                  fname-or-port)))
    (write-json (list->vector (space/soup space)) out)
    (close-output-port out))
  (mutex-unlock! (space/mutex space)))


(define (space/restore-from space fname-or-port)
  (mutex-lock! (space/mutex space))
  (let ((in (if (string? fname-or-port)
                (open-input-file fname-or-port)
                fname-or-port)))
    (space/soup! space (vector->list (read-json in)))
    (close-input-port in))
  (mutex-unlock! (space/mutex space)))


(define (space/dump space)
  (format #t "~A~%" (space/name space))
  (mutex-lock! (space/mutex space))
  (string-join (map (lambda (f)
                      (format #f "~A" f))
                    (space/soup space))
               "\n")
  (mutex-unlock! (space/mutex space)))

;; Add a tuple to a space. Multiple copies are allowed
;; Returns nil

(define (space/write! space entry)
  (mutex-lock! (space/mutex space))
  (space/soup! space (cons entry (space/soup space)))
  (mutex-unlock! (space/mutex space)))


;; Find tuples in a space  that match template
;; Return a list of the matching entries

(define (space/read space template)
  (let ((result #f))
    (mutex-lock! (space/mutex space))
    (set! result (filter (lambda (entry)
                           (match-tuple entry template)) 
                         (space/soup space)))
    (mutex-unlock! (space/mutex space))
    result))


;; Find entries that match template and remove them from the soup
;; Returns nil

(define (space/take! space template)
  (mutex-lock! (space/mutex space))
  (space/soup! space (remove (lambda (entry)
                               (match-tuple entry template))
                             (space/soup space)))
  (mutex-unlock! (space/mutex space)))



(define **state-space** (make-space "state"))
(define **persistent-space** (make-space "persistent"))
