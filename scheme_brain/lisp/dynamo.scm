;;; -*- scheme -*-
;;; Copyright 2017 Dave Astels. All rights reserved.
;;; Use of this source code is governed by a BSD-style
;;; license that can be found in the LICENSE file.
;;;
;;; This file handles the interface to AWS DynamoDB
;;;
;;; A queue is used to absorb bursts and send entries to Dynamo in a rate controller way

(require-extension srfi-1 gochan extras)
(declare (unit dynamo))

(define **USE_DYNAMO** #f)

(define dynamo/endpoint "http://10.215.222.140:8000")
(define dynamo/tablename "AstelsSmartHome")
(define dynamo/out #f)
(define dynamo/in #f)
(define dynamo/id #f)

(define dynamo/size 60)
(define dynamo/queue (make-vector dynamo/size '()))
(define dynamo/front 0)
(define dynamo/rear 0)


(define (dynamo/equal?)
  (eqv? dynamo/front dynamo/rear))


(define (dynamo/advance-front)
  (set! dynamo/front (remainder (add1 dynamo/front) dynamo/size)))


(define (dynamo/advance-rear)
  (set! dynamo/rear (remainder (add1 dynamo/rear) dynamo/size)))


(define (dynamo/remove)
  (if (not (dynamo/equal?))
      (begin (dynamo/advance-front)
             (let ((item (vector-ref dynamo/queue dynamo/front)))
               (vector-set! dynamo/queue dynamo/front '())
               item))))


(define **DYNAMO** #f)

(define (dynamo/start)
  (set! **DYNAMO** #t))

(define (dynamo/stop)
  (set! **DYNAMO** #f))


(define (dynamo/process)
  (if **DYNAMO**
    (let ((item (dynamo/remove)))
      (when item
        (display (json->string (make-alist 'endpoint dynamo/endpoint
                                           'command "insert"
                                           'tablename dynamo/tablename
                                           'timestamp (alist-ref 'timestamp item)
                                           'node (alist-ref 'node item)
                                           'type (alist-ref 'type item)
                                           'value (alist-ref 'value item)))
                 dynamo/in)
        (newline dynamo/in)))))


(define (dynamo/list-tables)
  (display (make-alist 'endpoint dynamo/endpoint 'command "listtables") dynamo/in)
  (newline dynamo/in)
  (let ((listdata (read dynamo/out)))
    (read-json listdata)))


(define (dynamo/create-table-if-required)
  (let ((tables (dynamo/list-tables)))
    (if (not (member dynamo/tablename tables))
        (begin (display (make-alist 'endpoint dynamo/endpoint 'command "createtable" 'tablename dynamo/tablename) dynamo/in)
               (newline dynamo/in)
               (let ((response (read dynamo/out)))
                 (if (not (equal? response "OK"))
                   (format #t response)))))))


(define (dynamo/add record)
  (dynamo/advance-rear)
  (if (dynamo/equal?)
    (dynamo/advance-front))
  (vector-set! dynamo/queue dynamo/rear record)
  record)


(initializers/register (lambda ()
                         (if **USE_DYNAMO**
                             (begin (log-for (info) "  dynamodb interface")
                                    (initializers/register (lambda ()
                                                             (log-for (info) "  polly")
                                                             (let-values (((out in id) (process "bin/dynamodb")))
                                                               (set! dynamo/out out)
                                                               (set! dynamo/in in)
                                                               (set! dynamo/id id))
                                                             
                                                             (dynamo/create-table-if-required)
                                                             (dynamo/start)
                                                             (let ((ticker-thread (make-thread
                                                                                   (lambda ()
                                                                                     (let ((ticker (gochan-tick 1000)))
                                                                                       (let loop ()
                                                                                         (gochan-select ((ticker -> _)
                                                                                                         (dynamo/process)
                                                                                                         (loop))))))
                                                                                   "dynamodb queue ticker")))
                                                               (thread-start! ticker-thread))))))))
