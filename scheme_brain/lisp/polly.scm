;;; -*- scheme -*-
;;; Copyright 2017 Dave Astels. All rights reserved.
;;; Use of this source code is governed by a BSD-style
;;; license that can be found in the LICENSE file.

;;; This file handles using Polly for various tasks

(require-extension posix medea log5scm syslog)
(declare (uses logging))
(declare (unit polly))

(define polly/voice "Amy")
(define polly/out #f)
(define polly/in #f)
(define polly/id #f)

;;; Say something, optionally caching it

(define (polly/say text cache-id wait?)
  (display (json->string (make-alist 'voice polly/voice 'text text 'cacheid cache-id 'wait wait?)) polly/in)
  (newline polly/in))


(define (polly/weather wait?)
  (polly/say (describe-weather (current-weather-conditions)) "" wait?))


(define (polly/today wait?)
  (polly/say (date->string (date-today)) "" wait?))


(define (polly/now wait?)
  (polly/say (hms->string (time-now)) "" wait?))


(define (polly/timestamp wait?)
  (polly/say (format #f "It is ~A on ~A." (hms->string (time-now)) (date->string (date-today))) "" wait?))


(define (polly/temperature-report)
  (let* ((readings (read:> **state-space** (make-alist 'type "temperature")))
		 (text (string-join (map (lambda (reading)
								   (let ((temp (/ (alist-ref 'value: reading) 100.0))
										 (room (node/node-id->group-id (alist-ref 'node reading))))
									 (format #f "~A is at ~A" room temp)))
								 readings)
							". ")))
	(polly/say text "" #f)))

(define (polly/movement-report)
  (let* ((rooms (map (lambda (f)
					   (node/node-id->group-name (alist-ref 'node f)))
					 (read:> **state-space** (make-alist 'type "motion" 'value #t)))))
	(polly/say (string-append "Movement is currently detected in: "
							  (string-join rooms ", "))
			   "" #f)))

(initializers/register (lambda ()
                         (log-for (info) "  polly")
                         (let-values (((out in id) (process  (string-join (list (get-environment-variable "SHCPATH")  "bin/polly") "/"))))
                           (set! polly/out out)
                           (set! polly/in in)
                           (set! polly/id id))))
