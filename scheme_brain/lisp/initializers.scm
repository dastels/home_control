;;; -*- scheme -*-
;;; Copyright 2017 Dave Astels. All rights reserved.
;;; Use of this source code is governed by a BSD-style
;;; license that can be found in the LICENSE file.
;;;
;;; This file manages initialization
;;; ----------------------------------------------------------------------------

(declare (unit initializers))
(require-extension log5scm syslog)

(define **initializers** '())


(define (initializers/register f)
  (set! **initializers** (cons f **initializers**)))


(define (initializers/run)
  (log-for (info) "Initializing:")
  (for-each (lambda (f)
              (f))
            (reverse **initializers**))
  (log-for (info) "Initialization complete"))



