;;; Copyright 2016 Dave Astels. All rights reserved.
;;; Use of this source code is governed by a BSD-style
;;; license that can be found in the LICENSE file.

;;; This file handles motion logic

(require-extension log5scm syslog) 
(declare (unit motion-polly))

(define motion-polly/recent-motions '())


(define (motion-polly/add-motion new-motion-event)
  (set! motion-polly/recent-motions (list new-motion-event
                                          (if (null? motion-polly/recent-motions) '()
                                              (car motion-polly/recent-motions)))))


;; Convert a date (year month day) to an adafruit io search timestamp
(define (date-time->adatime date time)
  (format #f "~A-~A-~AT~A:~A:~A.000Z" (first date) (second date) (third date) (first time) (second time) (third time)))

;; Convert an adafruit io timestamp to a date (year month day)
(define (adatime->date adatime)
  (let ((date-string (car (string-split adatime "T"))))
    (map string->number (string-split date-string "-"))))


;; Convert an adafruit io timestamp to the number of seconds since midnith this morning
(define (adatime->seconds adatime)
  (let* ((time-string (cadr (string-split adatime "T")))
         (time-parts (map string->number (string-split (car (string-split time-string ".")) ":"))))
    (+ (* (first time-parts) 3600)
       (* (second time-parts) 60)
       (third time-parts))))


;; was the current event the first one "today"
;; we take that to mean the first since the usual wake-up time

;; argument is  the current and previous (can be an empty frame) motion event that have happened so far today

(define (first-motion-today? current-event previous-event)
  (let* ((current-event-time (adatime->seconds (alist-ref 'created-at current-event)))
         (previous-event-time (adatime->seconds (alist-ref 'created-at previous-event))))
    (and (> current-event-time (start-of-day (day-of-week)))
         (or (not (equal? (adatime->date (alist-ref 'created-at previous-event)) (date-today)))
             (and (< previous-event-time (start-of-day (day-of-week)))
                  (> (- current-event-time previous-event-time) (hours->seconds 4)))))))

(define (polly/motionstart-handler topic payload)
  (let* ((now (time-now))
         (now-seconds (seconds-today))
         (today (date-today))
		 (node (string->number (fourth (string-split  topic "/"))))
         (current-event (make-alist 'created-at (date-time->adatime today now)))
         (previous-event (if (null? motion-polly/recent-motions)
                             (make-alist 'created-at "2000-01-01T00:00:00.000Z")
                             (car motion-polly/recent-motions))))
    (motion-polly/add-motion current-event)
    (cond ((first-motion-today? current-event previous-event) ;if this is the first motion detected today give a more involved greeting
           (cond ((> now-seconds (hours->seconds 17)) ; after 5pm
                  (polly/say "Good evening!" "good_evening" #t))
                 ((> now-seconds (hours->seconds 12)) ; after 12 noon
                  (polly/say "Good afternoon!" "good_afternoon" #t))
                 (else                  ;before noon
                  (polly/say "Good morning!" "good_morning" #t)))
           (polly/timestamp #t)
           (polly/weather #f))
          ((> (- now-seconds (adatime->seconds (alist-ref 'created-at previous-event))) (hours->seconds 4)) ; say hi if it's been a while
           (polly/say "Hello!" "hello" #f))
          )))
                           


(define (polly/motionend-handler topic payload)
  (polly/say (format #f "Motion ended at node ~A" (string->number (fourth (string-split topic "/")))) "" #f))

(initializers/register (lambda ()
                         (log-for (info) "  motion based voice output")
                         (mqtt-mux/register "shc/events/data/+/motionstart" polly/motionstart-handler)
                         ;;(mqtt-mux/register "shc/events/data/+/motionend" polly/motionend-handler)
                         ))
