;;; -*- scheme -*-
;;; Copyright 2017 Dave Astels. All rights reserved.
;;; Use of this source code is governed by a BSD-style
;;; license that can be found in the LICENSE file.
;;;
;;; This file provides immediate and scheduled MQTT queuing.
;;;
;;; Queue entries have the form:
;;; {
;;;   time: <the time in seconds then the queued message should be sent, from midnight>
;;;   topic: <the topic to punblish to>
;;;   payload: <the message payload>
;;; }

(require-extension gochan srfi-18 log5scm syslog)
(declare (uses logging chicken-mqtt))
(declare (unit mqtt-queue))

(define queue-mutex (make-mutex 'queue))

;; The queue of message to be sent later today
(define mqtt-queue/scheduled-messages '())


;; insert a message into the schedule
(define (mqtt-queue/insert-in-place m l)
  (cond ((null? l)
         (list m))
        ((<= (alist-ref 'time m) (alist-ref 'time (car l)))
         (cons m l))
        (else
         (cons (car l) (mqtt-queue/insert-in-place m (cdr l))))))


(define (mqtt-queue/insert message)
  (mutex-lock! queue-mutex)
  (set! mqtt-queue/scheduled-messages
        (mqtt-queue/insert-in-place message mqtt-queue/scheduled-messages))
  (mutex-unlock! queue-mutex))


(define (mqtt-queue/messages-queued?)
  (let ((result #f))
    (mutex-lock! queue-mutex)
    (set! result (not (null? mqtt-queue/scheduled-messages)))
    (mutex-unlock! queue-mutex)
    result))


(define (mqtt-queue/dequeue)
  (let ((result #f))
    (mutex-lock! queue-mutex)
    (set! result (car mqtt-queue/scheduled-messages))
    (set! mqtt-queue/scheduled-messages (cdr mqtt-queue/scheduled-messages))
    (mutex-unlock! queue-mutex)
    result))


(define (mqtt-queue/time-of-first)
  (let ((result 0))
    (mutex-lock! queue-mutex)
    (set! result (alist-ref 'time (car mqtt-queue/scheduled-messages)))
    (mutex-unlock! queue-mutex)
    result))


;; process the queue, publishing any messages whose time has come
(define (mqtt-queue/process-queue)
  (cond ((and (mqtt-queue/messages-queued?)
              (<= (mqtt-queue/time-of-first) (current-seconds)))
         (let ((m (mqtt-queue/dequeue)))
           (mqtt/publish (alist-ref 'topic m) (alist-ref 'payload m))
           (mqtt-queue/process-queue)))))

;; --------------------------------------------------------------------------------
;; Public functions

;; remove all scheduled messages for a topic
(define (mqtt-queue/unschedule topic)
  (mutex-lock! queue-mutex)
  (set! mqtt-queue/scheduled-messages
        (remove (lambda (m)
                  (equal? (alist-ref 'topic m) topic))
                mqtt-queue/scheduled-messages))
  (mutex-unlock! queue-mutex))


;; check if there is a message scheduled for topic
(define (mqtt-queue/scheduled? topic)
  (let ((result #f))
    (mutex-lock! queue-mutex)
    (set! result (find (lambda (m)
                         (equal? (alist-ref 'topic m) topic))
                       mqtt-queue/scheduled-messages))
    (mutex-unlock! queue-mutex)
    result))


;; publish a message immediately
(define (mqtt-queue/send-now topic payload)
  (log-for (mqtt-debug) "Sending <~A> on <~A>~%" payload topic)
  (mqtt/publish topic payload))


;; enqueue a message in delay seconds
(define (mqtt-queue/send-in delay topic payload)
  (if (zero? delay)
      (mqtt-queue/send-now topic payload)
      (begin (mqtt-queue/unschedule topic)
             (mqtt-queue/insert (make-alist 'time (+ (current-seconds) delay) 'topic topic 'payload payload)))))


;; Set up a recurring message, starting after a given delay
(define (mqtt-queue/send-every-in period delay topic payload)
  (mqtt-queue/unschedule topic)
  (let* ((recurring-topic (format #f "~A/recurring" topic))
         (recurring-handler (lambda (t p)
                                 (mqtt-queue/send-in period recurring-topic "")
                                 (log-for (info) "Processing recurring event: ~A" topic)
                                 (mqtt-queue/send-now topic payload))))
    (mqtt-mux/register recurring-topic recurring-handler)
    (mqtt-queue/send-in delay recurring-topic "")))


;; Dump a representation of the queue to stdout
(define (mqtt-queue/dump)
  (mutex-lock! queue-mutex)
  (if (null? mqtt-queue/scheduled-messages)
      (format #t "MQTT queue is empty~%")
      (for-each (lambda (queue-entry)
                  (format #t "~A ~A ~S~%" (seconds->string (alist-ref 'time queue-entry)) (alist-ref 'topic queue-entry) (alist-ref 'payload queue-entry)))
                mqtt-queue/scheduled-messages))
  (mutex-unlock! queue-mutex))

;; --------------------------------------------------------------------------------
;; Setup and run ticker

(initializers/register (lambda ()
                         (log-for (info) "  mqtt queue")
                         (let ((ticker-thread (make-thread
                                               (lambda ()
                                                 (let ((ticker (gochan-tick 1000)))
                                                   (let loop ()
                                                     (gochan-select ((ticker -> _)
                                                                     (mqtt-queue/process-queue)
                                                                     (loop))))))
                                               "mqtt queue ticker")))
                           (thread-start! ticker-thread))))
