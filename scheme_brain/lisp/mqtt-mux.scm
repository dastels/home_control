;;; -*- scheme -*-
;;; Copyright 2017 Dave Astels. All rights reserved.
;;; Use of this source code is governed by a BSD-style
;;; license that can be found in the LICENSE file.

;;; This file provides an MQTT subscription multiplexer
;;; MUST BE LOADED FIRST

(require-extension srfi-1 log5scm syslog)
(declare (uses chicken-mqtt logging))
(declare (unit mqtt-mux))

(define **SHC_PATTERN** "shc/#")

;; An alist of handlers indexed by topic pattern.
;; Each handler is a lambda that takes an actual topic and payload as arguments.
;; ((pattern1 . (handler ... handler))
;;  .
;;  .
;;  .
;;  (patternN . (handler ... handler)))

(define mqtt-mux/handlers '())


(define (mqtt-mux/reset)
  (map mqtt/unsubscribe
       (map car
            mqtt-mix/handlers))
  (set! mqtt-mux/handlers '()))


(define (mqtt-mux/register topic-pattern handler)
  (let ((existing-handlers (assoc topic-pattern mqtt-mux/handlers)))
    (if existing-handlers
        (set-cdr! existing-handlers (cons handler (cdr existing-handlers)))
        (begin (set! mqtt-mux/handlers (alist-cons topic-pattern (list handler) mqtt-mux/handlers))
               (mqtt/subscribe topic-pattern (lambda (topic payload)
                                               (map (lambda (f)
                                                      (f topic payload))
                                                    (alist-ref topic-pattern mqtt-mux/handlers))))))))


(define (mqtt-mux/deregister topic-pattern handler)
  (let ((existing-handlers (assoc topic-pattern mqtt-mux/handlers)))
    (if existing-handlers
      (let ((new-handlers (remove (lambda (x) (equal? x handler)) (cdr existing-handlers))))
        (if (null? new-handlers)
            (begin (set! mqtt-mux/handlers (alist-delete topic-pattern mqtt-mux/handlers))
                   (mqtt/unsubscribe topic-pattern))
            (set-cdr! existing-handlers new-handlers))))))


(initializers/register (lambda ()
                         (log-for (info) "  mqtt multiplexer")
                         (if (> (mqtt/setup) 0)
                             (error "Problem setting up MQTT client"))))
