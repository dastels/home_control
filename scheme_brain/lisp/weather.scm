;;; -*- scheme -*-
;;; Copyright 2017 Dave Astels. All rights reserved.
;;; Use of this source code is governed by a BSD-style
;;; license that can be found in the LICENSE file.
;;;
;;; This file handles fetching and processing weather conditions

(require-extension srfi-1 medea http-client intarweb uri-common posix extras)
(declare (uses space))
(declare (unit weather))


(define darksky-key (get-environment-variable "DARKSKY_KEY"))

(define darksky-url (format #f "https://api.darksky.net/forecast/~A/42.9837,-81.2497?units=ca&exclude=minutely,hourly&language=en" darksky-key))


(define (update-sunrise-sunset! data)
    (space/take! **state-space** (make-alist 'type 'sunrise))
    (space/write! **state-space** (make-alist 'type 'sunrise 'time (- (alist-ref 'sunriseTime data) (alist-ref 'time data))))
    (space/take! **state-space** (make-alist 'type 'sunset))
    (space/write! **state-space** (make-alist 'type 'sunset 'time (- (alist-ref 'sunsetTime data) (alist-ref 'time data)))))

(define (get-weather-data)
  (let-values (((json b c) (with-input-from-request darksky-url #f read-string)))
    (let ((weather-data (read-json json)))
      (update-sunrise-sunset! (vector-ref (alist-ref 'data (alist-ref 'daily weather-data)) 0))
      weather-data)))


(define (current-weather-conditions)
  (alist-ref 'currently (get-weather-data)))


(define (bearing->direction bearing)
  (cond ((or (>= bearing 337) (<= bearing 23))
         'north)
        ((<= bearing 68)
         'northeast)
        ((<= bearing 113)
         'east)
        ((<= bearing 158)
         'southeast)
        ((<= bearing 203)
         'south)
        ((<= bearing 248)
         'southwest)
        ((<= bearing 293)
         'west)
        ((<= bearing 338)
         'northwest)))

(define (number->intstring n)
  (car (string-split (number->string (floor n)) ".")))


(define (naturalize-temperature v)
  (let* ((int-v (floor v))
         (int-string (number->intstring int-v))
         (leftover (- v int-v)))
    (cond ((<= leftover 0.15)
           int-string)
          ((<= leftover 0.35)
           (if (zero? int-v)
             "one quarter degree"
             (format #f "~A and a quarter" int-string)))
          ((<= leftover 0.65)
           (if (zero? int-v)
             "one half degree"
             (format #f "~A and a half" int-string)))
          ((<= leftover 0.85)
           (if (zero? int-v)
             "three quarters of a degree"
             (format #f "~A and three quarters" int-string)))
          (else
           (number->intstring (add1 int-v))))))


(define (naturalize-speed v)
  (let* ((int-v (floor v))
         (int-string (number->intstring int-v))
         (leftover (- v int-v)))
    (cond ((<= leftover 0.15)
           int-string)
          ((<= leftover 0.35)
           (if (zero? int-v)
             "one quarter"
             (format #f "~A and a quarter" int-string)))
          ((<= leftover 0.65)
           (if (zero? int-v)
             "one half"
             (format #f "~A and a half" int-string)))
          ((<= leftover 0.85)
           (if (zero? int-v)
             "three quarters"
             (format #f "~A and three quarters" int-string)))
          (else
           (number->intstring (add1 int-v))))))


(define (pluralize-degrees temp)
  (if (eqv? (abs temp) 1.0)
    "degree"
    "degrees"))


(define (describe-weather data)
  ;; extract data
  (let ((summary (alist-ref 'summary data))
        (temperature (alist-ref 'temperature data))
        (apparent-temperature (alist-ref 'apparentTemperature data))
        (humidity (alist-ref 'humidity data))
        (precip-probability (alist-ref 'precipProbability data))
        (precip-type (alist-ref 'precipType data))
        (wind-speed (alist-ref 'windSpeed data))
        (wind-bearing (alist-ref 'windBearing data)))
    ;; build phrases from the extracted data
    (let ((summary-phrase (if summary
                            (format #f "It is currently ~A." summary)
                            ""))
          (temperature-phrase (if temperature
                                (format #f "The temperature is ~A ~A celcius" (naturalize-temperature temperature) (pluralize-degrees temperature))
                                ""))
          (humidity-phrase (if humidity
                             (format #f "with a relative humidity of ~A percent" (number->intstring (* 100 humidity)))
                             ""))
          (apparent-phrase (if (or (not apparent-temperature)
                                   (< (abs (- apparent-temperature temperature)) 0.15))
                             "."
                             (format #f "~A it feels like ~A~A."
                                     (if (not humidity)
                                       (if (not temperature)
                                         ""
                                         " but ")
                                       "and")
                                     (naturalize-temperature apparent-temperature)
                                     (if (not temperature)
                                       (format #f  " ~A celcius" (pluralize-degrees apparent-temperature))
                                       ""))))
          (precip-phrase (if (or (not precip-probability)
                                 (eqv? precip-probability 0))
                           ""
                           (format #f "There is a ~A percent chance of ~A." (floor (* 100 precip-probability)) precip-type)))
          (wind-phrase (if (or (not wind-speed)
                               (eqv? wind-speed 0))
                         ""
                         (format #f "The wind is out of the ~A at ~A ~A per hour."
                                 (bearing->direction wind-bearing)
                                 (naturalize-speed wind-speed)
                                 (if (> wind-speed 1.15) "kilometers" "kilometer")))))
      (string-join (list summary-phrase temperature-phrase humidity-phrase apparent-phrase precip-phrase wind-phrase) " "))))
