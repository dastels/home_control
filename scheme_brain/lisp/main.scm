;;; -*- scheme -*-
;;; Copyright 2017 Dave Astels. All rights reserved.
;;; Use of this source code is governed by a BSD-style
;;; license that can be found in the LICENSE file.
;;;
;;; This file starts up brain
;;; ----------------------------------------------------------------------------


;; Mention modules in the order in which they need to be initialized.

;; removed fan-control for now

(declare (uses initializers util logging heartbeat space chicken-mqtt mqtt mqtt-mux mqtt-queue time
               polly dynamo hue-lighting lighting-interface lighting-control node adafruitio
               motion-polly proximity-handler alarm weather))

(require-extension srfi-1 srfi-18 files posix posix-extras log5scm syslog gochan irregex data-structures args medea)

(define data-dir (get-environment-variable "SHCPATH"))
(define space-file (format #f "~A/persistent_space.json" data-dir))

(initializers/register (lambda ()
                         (log-for (info) "  persistent space")
                         (space/restore-from **persistent-space** space-file)
                         (let ((ticker-thread (make-thread (lambda ()
                                                             (let ((ticker (gochan-tick 318000)))
                                                               (let loop ()
                                                                 (gochan-select ((ticker -> _)
                                                                                 (space/persist-to **persistent-space** space-file)
                                                                                 (loop))))))
                                                           "persistent space writer")))
                           (thread-start! ticker-thread))))


(define (power-on)
  (process "mplayer" (list "-quiet" "-nolirc" "-volume" "70" (string-join (list (get-environment-variable "SHCPATH") "power-on.mp3") "/"))))


;; The main function that starts the system

(define (startup)
  (log-for (info) "System starting up")
  (power-on)
  (get-weather-data)           ; to preload sunset & sunrise times
  (initializers/run)
  (polly/say "Hello. System is starting up." "startup" #t)
  (mqtt-queue/send-in 15 "shc/reset" ""))


(define (shutdown)
  (polly/say "System is shutting down. Goodbye." "shutdown" #t)
  (space/persist-to **persistent-space** space-file))


(define (read-cmd)
  (thread-wait-for-i/o! fileno/stdin #:input)
  (read-line))


(define (show-queue)
  (mqtt-queue/dump))


(define (show-node n)
  (node/dump n))

(define (show-motion-history)
  (let ((motion-data (space/read **state-space** (make-alist 'type 'motion-tracking 'dow (day-of-week)))))
    (if (null? motion-data)
        (format #t "No data~%")
        (format #t "~A~%" (string-join (map number->string (vector->list (alist-ref 'counts (car motion-data)))) " ")))))


(define (command-loop)
  (let loop ((cmd (read-cmd))
             (do-more #t))
    (cond ((equal? cmd "")
           )
          ((equal? cmd "?")
           (format #t "Command menu:~%")
           (format #t "? - show this menu~%")
           (format #t "# - show status of node #~%")
           (format #t "a - report whether the house is awake~%")
           (format #t "a t/f - mark the house as awake or not~%")
           (format #t "f - refresh node features~%")
           (format #t "h - motion history~%")
           (format #t "m - show MQTT queue~%")
           (format #t "n - show node~%")
           (format #t "c - show all nodes~%")
           (format #t "q - quit~%")
           (format #t "s topic message - send a message via MQTT~%")
           (format #t "t - give time and date~%")
           (format #t "w - give weather report~%"))
          ((irregex-match "\\'integer" cmd)
           (show-node (string->number cmd)))
          ((equal? (car (string-split cmd)) "a")
           (let ((args (cdr (string-split cmd))))
             (if (null? args)
                 (format #t "~%The house is ~A~%" (if (usually-awake-at? (seconds-today))
                                                      "awake"
                                                      "asleep"))
                 (set-awake (equal? (car args) "t")))))
          ((equal? cmd "f")
           (node/get-features))
          ((equal? cmd "h")
           (show-motion-history))
          ((equal? cmd "m")
           (show-queue))
          ((equal? cmd "c")
           (vector-for-each (lambda (n)
                              (format #t "~%")
                              (show-node n))
                            (node/ids)))
          ((equal? cmd "n")
           (format #t "~A~%" (json->string **node/config**)))
          ((equal? cmd "q")
           (set! do-more #f))
          ((equal? (car (string-split cmd)) "s")
           (let ((parts (string-split cmd)))
             (mqtt-queue/send-now (cadr parts) (caddr parts))))
          ((equal? cmd "t")
           (polly/timestamp #f))
          ((equal? cmd "w")
           (polly/weather #f)))
    (if do-more
        (loop (read-cmd) #t))))


(define (usage)
  (format (current-error-port)
          "Usage: ~A [options...]~%~A~%"
          (car (argv))
          (args:usage opts)))

(define opts
  (list (args:make-option (f fake-hue) #:none "fake HUE interface"
                          (begin
                            (format #t "faking hue~%")
                            (set! net/put (lambda (uri json) (format #t "Sending to Hue at ~A: ~A~%" uri json)))))
        (args:make-option (h help) #:none "Display this help"
                          (usage))))

(define (main)
  (startup)
  (command-loop)
  (shutdown))


(receive (options operands)
    (args:parse (command-line-arguments) opts)
  (main))
