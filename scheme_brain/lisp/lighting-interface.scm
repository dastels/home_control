;;; -*- scheme -*-
;;; Copyright 2017 Dave Astels. All rights reserved.
;;; Use of this source code is governed by a BSD-style
;;; license that can be found in the LICENSE file.

;;; This file provides the generic lighting interface

;;; PRIVATE HELPERS

(require-extension log5scm syslog medea)
(declare (uses logging mqtt-queue))
(declare (unit lighting-interface))

(define **light-state-function** #f)

(define (lighting/register-light-state-function f)
  (set! **light-state-function** f))


(define (lighting/node-lights-on? node)
  (and **light-state-function**    
       (**light-state-function** node)))


(define (lighting/node->topic node)
  (format #f "shc/lighting/~A" node))


(define (lighting/send-in topic-name payload secs)
  (mqtt-queue/unschedule topic-name)
  (if (zero? secs)
    (mqtt-queue/send-now topic-name payload)
    (mqtt-queue/send-in secs topic-name payload)))


(define (lighting/set-node-level-in node level secs)
  (let ((topic-name (lighting/node->topic node))
        (payload (json->string (vector level))))
    (lighting/send-in topic-name payload secs)))


(define (lighting/set-node-rgb-in node red green blue brightness secs)
  (let ((topic-name (lighting/node->topic node))
        (payload (json->string (vector red green blue brightness))))
    (lighting/send-in topic-name payload secs)))


(define (lighting/cleanup-state node)
  (log-for (info) "Cleaning up light state for node ~A" node)
  (space/take! **state-space** (make-alist 'type 'light-on 'node node))
  (space/take! **state-space** (make-alist'type 'light-at-last-turnon 'node node)))


;; listens for lights-off event (I.e. a lighting event setting level OR RGB to zero) and cleans up cached state.

(define (lighting/event-handler topic payload)
  (log-for (debug) "Checking whether to clean up ~A: ~S" topic payload)
  (let* ((node (control-topic->node topic))
         (value (read-json payload)))
    (log-for (debug) "Converted payload: ~S" value)
    (if (or (and (eqv? (vector-length value) 1)
                 (zero? (vector-ref value 0)))
            (and (> (vector-length value) 1)
                 (zero? (+ (vector-ref value 0)
                           (vector-ref value 1)
                           (vector-ref value 2)))))
        (lighting/cleanup-state node))))

(initializers/register (lambda ()
                         (log-for (info) "  lighting")
                         (mqtt-mux/register "shc/lighting/#" lighting/event-handler)))


;;; --------------------------------------------------------------------------------
;;; PUBLIC API

;; lights on at a specific level

(define (lighting/on-in node level secs)
  (lighting/set-node-level-in node level secs))


(define (lighting/on node level)
  (lighting/on-in node level 0))


;; lights on in a specific colour

(define (lighting/colour-in node red green blue brightness secs)
  (lighting/set-node-rgb-in node red green blue brightness secs))

(define (lighting/colour node red green blue brightness)
  (lighting/colour-in node red green blue brightness 0))


;; lights off

(define (lighting/off-in node secs)
  (log-for (info) "Scheduling node ~A lights off in ~A S" node secs)
  (lighting/set-node-level-in node 0 secs))


(define (lighting/off node)
  (lighting/off-in node 0))


;;; Lighting event management

(define (lighting/lights-off-scheduled? node)
  (mqtt-queue/scheduled? (format #f "shc/lighting/~A" node)))

(define (lighting/cancel-for node)
  (mqtt-queue/unschedule (lighting/node->topic node)))

