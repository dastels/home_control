;;; -*- scheme -*-
;;; Copyright 2017 Dave Astels. All rights reserved.
;;; Use of this source code is governed by a BSD-style
;;; license that can be found in the LICENSE file.
;;;
;;; This file configures logging

(declare (unit logging))
(require-extension log5scm syslog)

(define-category info)
(define-category debug)
(define-category mqtt-debug)
(define-category prox-debug)
(define-category warn)
;(start-sender debug-sender (port-sender (current-output-port)) (category debug))
;(start-sender mqtt-sender (port-sender (current-output-port)) (category mqtt-debug))
;(start-sender prox-sender (port-sender (current-output-port)) (category prox-debug))
(start-sender info-sender (port-sender (current-output-port)) (category info))
(start-sender warn-sender (port-sender (current-output-port)) (category warn))
