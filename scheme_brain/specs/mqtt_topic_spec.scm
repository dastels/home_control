;;; -*- scheme -*-

(use missbehave missbehave-matchers missbehave-stubs miscmacros files posix srfi-13 medea)

(load (normalize-pathname "./lisp/mqtt.scm"))

(describe "Extracting from a sensor data topic"

          (it "can extract node number"
              (expect (sensor-topic->node "shc/sensors/data/1/light") (to (be 1)))
              (expect (sensor-topic->node "shc/sensors/data/42/temperature") (to (be 42)))
              )

          (it "can extract sensor name"
              (expect (sensor-topic->name "shc/sensors/data/1/light") (to (be "light")))
              (expect (sensor-topic->name "shc/sensors/data/42/temperature") (to (be "temperature"))))

          )

(describe "Extracting from a sensor event topic"

          (it "can extract node number"
              (expect (sensor-topic->node "shc/sensors/event/1/light") (to (be 1)))
              (expect (sensor-topic->node "shc/sensors/event/42/temperature") (to (be 42))))

          (it "can extract sensor name"
              (expect (sensor-topic->name "shc/sensors/event/1/light") (to (be "light")))
              (expect (sensor-topic->name "shc/sensors/event/42/temperature") (to (be "temperature"))))

          )

(describe "Extracting from a lighting topic"

          (it "can extract node number"
              (expect (control-topic->node "shc/lighting/1") (to (be 1)))
              (expect (control-topic->node "shc/lighting/42") (to (be 42)))))
