;;; -*- mode: Scheme -*-

(use missbehave missbehave-matchers missbehave-stubs miscmacros files)

(load (normalize-pathname "./lisp/initializers.scm"))

(context "Initializer"
         (define a 0)
         (define b 0)
         (define s "")

		 (it "calls registered initializers"
			 (initializers/register (lambda () (set! a 2)))
			 (initializers/register (lambda () (set! b 3)))
			 (initializers/run)
			 (expect a (to (be 2)))
			 (expect b (to (be 3))))

         )
