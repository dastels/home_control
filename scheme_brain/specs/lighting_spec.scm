;;; -*- scheme -*-

(use missbehave missbehave-matchers missbehave-stubs miscmacros files posix srfi-13) 

(load (normalize-pathname "./lisp/initializers.scm"))
(load (normalize-pathname "./lisp/lighting-control.scm"))

(describe "Lighting control"

          (it "uses full strength lighting when the household is awake"
              (set! usually-awake-at? (lambda (x) #t))
              (expect (lighting-control/appropriate-light-level) (to (be 100))))

          (it "uses low lighting when the household is asleep"
              (set! usually-awake-at? (lambda (x) #f))
              (expect (lighting-control/appropriate-light-level) (to (be 10))))

          (it "uses full strength white lighting when the household is awake"
              (set! usually-awake-at? (lambda (x) #t))
              (expect (lighting-control/appropriate-colour) (to (be '(#xff #xff #xc0 100)))))

          (it "uses low red lighting when the household is asleep"
              (set! usually-awake-at? (lambda (x) #f))
              (expect (lighting-control/appropriate-colour) (to (be '(#x1f #x00 #x00 10)))))

          (it "knows it needs light if it's dark outside, regardless of room light level"
              (set! dark-out? (lambda () #t))
              (expect (lighting-control/light-needed? 1 1000) (to (be true)))
              (expect (lighting-control/light-needed? 1 0) (to (be true))))

          (it "knows it needs light if it's dark in the room and the lights are off"
              (set! dark-out? (lambda () #f))
              (set! lighting/node-lights-on? (lambda (n) #f))
              (set! lighting-control/light-needed-threshold (lambda (n) 600))
              (expect (lighting-control/light-needed? 1 300) (to (be true))))

          (it "knows it doesn't need light if it's light in the room and the light is off"
              (set! dark-out? (lambda () #f))
              (set! lighting/node-lights-on? (lambda (n) #f))
              (set! lighting-control/light-needed-threshold (lambda (n) 600))
              (expect (lighting-control/light-needed? 1 700) (to (be false))))



          )
