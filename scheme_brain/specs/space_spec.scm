;;; -*- scheme -*-

(use missbehave missbehave-matchers missbehave-stubs miscmacros files)

(load (normalize-pathname "./lisp/space.scm"))

(define (contain-string x)
  (matcher
   (check (subject)
          (string-contains (force subject) x))
   (message (form subject negate)
            (if negate
                (sprintf "Expected ~A to not contain ~A" (force subject) x)
                (sprintf "Expected ~A to contain ~A" (force subject) x)))))

(describe "Space"
          (define s (make-space "test"))
          (before each: (space/clear s))
          
          (it "initializes to an empty soup"
              (expect (space/soup s) (is null?)))
          
          (it "can be written to"
              (space/write! s '((a . 1)))
              (space/write! s '((b . 2)))
              (expect (space/soup s) (to (be '(((b . 2))
                                               ((a . 1)))))))
          
          (it "doesn't overwrite"
              (space/write! s '((a . 1)))
              (space/write! s '((a . 1)))
              (expect (space/soup s) (to (be '(((a . 1))
                                               ((a . 1)))))))
          
          (it "can take"
              (space/write! s '((a . 1) (b . 1)))
              (space/write! s '((a . 2) (b . 1)))
              (space/write! s '((a . 1) (b . 2)))
              (expect (space/soup s) (to (be '(((a . 1) (b . 2))
                                               ((a . 2) (b . 1))
                                               ((a . 1) (b . 1))))))
              (space/take! s '((a . 1)))
              (expect (space/soup s) (to (be '(((a . 2) (b . 1)))))))
          
          (it "can read a fully matching template"
              (space/write! s '((a . 1) (b . 1)))
              (space/write! s '((a . 1) (b . 2)))
              (space/write! s '((a . 2) (b . 3)))
              (expect (space/read s '((a . 1) (b . 2))) (to (be '(((a . 1) (b . 2)))))))
          
          (it "can read a partial matching template"
              (space/write! s '((a . 1) (b . 1)))
              (space/write! s '((a . 1) (b . 2)))
              (space/write! s '((a . 2) (b . 3)))
              (expect (space/read s '((a . 1))) (to (be '(((a . 1) (b . 2))
                                                          ((a . 1) (b . 1)))))))

          (it "can export"
              (space/write! s '((a . 1) (b . 1)))
              (space/write! s '((a . 1) (b . 2)))
              (space/write! s '((a . 2) (b . 3)))
              (let ((p (open-output-string)))
                (space/persist-to p s)
                (let ((output-string (get-output-string p)))
                  (expect output-string (to (contain-string "((a . 1) (b . 1))")))
                  (expect output-string (to (contain-string "((a . 1) (b . 2))")))
                  (expect output-string (to (contain-string "((a . 2) (b . 3))"))))))

          (it "can import"
              (space/restore-from (open-input-string "(((a . 1) (b . 1)) ((a . 1) (b . 2)) ((a . 2) (b . 3)))") s)
              (expect (space/read s '((a . 1))) (to (be '(((a . 1) (b . 1))
                                                          ((a . 1) (b . 2))))))))


