;;; -*- scheme -*-

(use missbehave missbehave-matchers missbehave-stubs miscmacros files posix srfi-13 medea)

(define captured-topic "")
(define captured-message "")

(load (normalize-pathname "./lisp/space.scm"))
(load (normalize-pathname "./lisp/util.scm"))
(load (normalize-pathname "./lisp/time.scm"))
(load (normalize-pathname "./lisp/mqtt.scm"))
(load (normalize-pathname "./lisp/node.scm"))

(define (reset-config)
  (set! **node/config** '((nodes . #(()
                                     ((id . 1) (name . "Node 1") (group . ((id . 1) (name . "Group 1"))) (features . 0) (extensions . #(((bit . 7) (direction . "output") (name . "fan")))))
                                     ((id . 2) (name . "Node 2") (features . 0) (light . #f))
                                     ((id . 3) (name . "Node 3") (features . 0) (light . #t) (lights-off-in . 1))
                                     ((id . 4) (name . "Node 4") (features . 0) (light . #t) (lights-off-in . 2))
                                     ((id . 5) (name . "Node 5") (features . 0))))
                          (groups . #(()
                                      ((id . 1) (name . "Group 1")))))))

(describe "A node"
		 (before each: (begin
                         (reset-config)
                         (set! **state-space** (make-space "state"))))

		 (it "can record features"
		     (node/record-features 1 #xAA)
		     (expect (list? (vector-ref (node-config/nodes) 1)) (to (be true)))
		     (expect (node-config/node-features (node-config/node 1)) (to (be #xAA))))

		 (it "can check feature bits"
		     (node/record-features 1 #xAA)
		     (expect (node/check-feature-bit 1 #x01) (to (be false)))
		     (expect (node/check-feature-bit 1 #x02) (to (be true)))
		     (expect (node/check-feature-bit 1 #x04) (to (be false)))
		     (expect (node/check-feature-bit 1 #x08) (to (be true)))
		     (expect (node/check-feature-bit 1 #x10) (to (be false)))
		     (expect (node/check-feature-bit 1 #x20) (to (be true)))
		     (expect (node/check-feature-bit 1 #x40) (to (be false)))
		     (expect (node/check-feature-bit 1 #x80)) (to (be true)))


		 (it "can check for motion"
		     (node/record-features 1 node/motion-mask)
		     (expect (node/has-motion? 1) (to (be true))))

		 (it "can check for light"
		     (node/record-features 1 node/light-mask)
		     (expect (node/has-light? 1) (to (be true))))

		 (it "can check for proximity"
		     (node/record-features 1 node/prox-mask)
		     (expect (node/has-proximity? 1) (to (be true))))

		 (it "can check for temperature"
		     (node/record-features 1 node/temp-rh-mask)
		     (expect (node/has-temperature? 1) (to (be true))))

		 (it "can check for neopixels"
		     (node/record-features 1 node/neopixel-mask)
		     (expect (node/has-neopixels? 1) (to (be true))))

		 (it "can check current motion state when there is motion noted"
		     (space/write! **state-space** '((node . 1) (type . "motion") (value . #t)))
		     (expect (node/motion-detected? 1) (to (be true))))

		 (it "can check current motion state when there isn't motion noted"
		     (space/write! **state-space** '((node . 1) (type . "motion") (value . #f)))
		     (expect (node/motion-detected? 1) (to (be false))))

         (it "knows not to not use light when it is not specified"
             (expect (node/use-light? 1) (to (be false))))

         (it "knows not to not use light when it is set to false"
             (expect (node/use-light? 2) (to (be false))))

         (it "knows to use light when it is set to true"
             (expect (node/use-light? 3) (to (be true))))

         (it "knows how long to wait before the light turns off"
             (expect (node/lights-off-time 3) (to (be 60000)))
             (expect (node/lights-off-time 4) (to (be 120000))))

         (it "can find extension data"
             (expect (node/extension-named 1 "fan") (to (be '((bit . 7) (direction . "output") (name . "fan"))))))

         (it "can tell if a node has an extension"
             (expect (node/has-extension-named? 1 "fan") (to (be true))))


         (it "can tell if a node doesn't have a specific extension"
             (expect (node/has-extension-named? 1 "alarm") (to (be false))))

         (it "knows the bit of an extension"
             (expect (node/extension-bit 1 "fan") (to (be 7))))

         (it "knows that there's no bit for a non-existent extension"
             (expect (node/extension-bit 1 "alarm") (to (be false))))

         (it "knows the direction of an extension"
             (expect (node/extension-direction 1 "fan") (to (be "output"))))

         (it "knows that there's no direction for a non-existent extension"
             (expect (node/extension-direction 1 "alarm") (to (be false))))

         (context "sending commands to the mesh"
                  
                  (before each: (begin
                                  (reset-config)
                                  (set! mqtt-queue/send-now (lambda (topic qos message)
                                                             (set! captured-topic topic)
                                                             (set! captured-message message)))
                                  
                                  (set! captured-topic "")
                                  (set! captured-message "")))
                  
                  (it "can configure the extensions"
                      (node/configure-extension-port 1 #xF0 #xC0)
                      (expect captured-topic (to (be "shc/sensors/command/1/ext-config")))
                      (expect captured-message (to (be "240 192"))))

                  (it "can read an extension bit"
                      (node/read-extension-bit 1 7)
                      (expect captured-topic (to (be "shc/sensors/command/1/ext-read")))
                      (expect captured-message (to (be "7"))))

                  (it "can write true to an extension bit"
                      (node/write-extension-bit 1 7 #t)
                      (expect captured-topic (to (be "shc/sensors/command/1/ext-write")))
                      (expect captured-message (to (be "7 1"))))

                  (it "can write false to an extension bit"
                      (node/write-extension-bit 1 7 #f)
                      (expect captured-topic (to (be "shc/sensors/command/1/ext-write")))
                      (expect captured-message (to (be "7 0"))))
                  
                  (it "knows when it has a fan"
                      (expect (node/has-fan? 1) (to (be true))))

                  (it "knows when it doesn't have a fan"
                      (reset-config)
                      (expect (node/has-fan? 2) (to (be false))))

                  (it "can tell the fan to turn on"
                      (node/fan-on-off 1 #t)
                      (expect captured-topic (to (be "shc/sensors/command/1/ext-write")))
                      (expect captured-message (to (be "7 0"))))

                  (it "can tell the fan to turn off"
                      (node/fan-on-off 1 #f)
                      (expect captured-topic (to (be "shc/sensors/command/1/ext-write")))
                      (expect captured-message (to (be "7 1"))))

                  (it "can set the color of its indicator LED"
                      (node/set-led 1 63 127 255)
                      (expect captured-topic (to (be "shc/sensors/command/1/led")))
                      (expect captured-message (to (be "63 127 255 0 0"))))

                  (it "can set the color of its indicator LED with blinking"
                      (node/set-led 1 63 127 255 800 200)
                      (expect captured-topic (to (be "shc/sensors/command/1/led")))
                      (expect captured-message (to (be "63 127 255 800 200"))))

                  (it "can set the color of its Neopixels"
                      (node/set-neopixels 1 63 127 255)
                      (expect captured-topic (to (be "shc/sensors/command/1/neopixel")))
                      (expect captured-message (to (be "63 127 255"))))

                  (it "can send a request for features to a node"
                      (node/request-features 1)
                      (expect captured-topic (to (be "shc/sensors/command/1/features")))
                      (expect captured-message (to (be ""))))
                  )

         (it "knows all nodes"
             (expect (node/ids) (to (be '#(1 2 3 4 5)))))

         (it "can update state"
             (space/write! **state-space** (make-alist 'node 1 'type "temperature" 'value 0))
             (node/update-state 1 "temperature" 2457)
             (let ((d (space/read **state-space** (make-alist 'node 1 'type "temperature"))))
               (expect (alist-ref 'value (car d)) (to (be 2457)))))

         (it "can get state"
             (space/write! **state-space** (make-alist 'node 1 'type "temperature" 'value 2457))
             (expect (alist-ref 'value (node/get-state 1 "temperature")) (to (be 2457))))
         
         (it "can map from node to group"
             (expect (node/node->group 1) (to (be "Group 1"))))

         (it "knows that groupless modes are in their own group"
             (expect (node/node->group 1) (to (be "Group 1"))))

         (context "processing sensor data"

                  (before each: (begin
                                  (reset-config)
                                  (set! mqtt-queue/send-now (lambda (topic message)
                                                              (set! captured-topic topic)
                                                              (set! captured-message message)))
                                  
                                  (set! captured-topic "")
                                  (set! captured-message "")))
                  
                  
                  (it "records data from a sensor"
                      (node/capture-state (make-alist 'topic "shc/sensors/data/1/temperature" 'payload "42"))
                      (expect (alist-ref 'value (car (space/read **state-space** (make-alist 'node 1 'type "temperature")))) (to (be 42))))

                  (it "forwards sensor data events"
                      (node/capture-state (make-alist 'topic "shc/sensors/data/1/temperature" 'payload "42"))
                      (expect captured-topic (to (be "shc/events/data/1/temperature")))
                      (expect captured-message (to (be "42"))))

                  (it "records motion from a sensor (boolean data handling)"
                      (node/capture-state (make-alist 'topic "shc/sensors/data/1/motion" 'payload "1"))
                      (expect (alist-ref 'value (car (space/read **state-space** (make-alist 'node 1 'type "motion")))) (to (be true))))
                  
                  (it "properly sets state from motion starting"
                      (node/capture-state (make-alist 'topic "shc/sensors/data/1/motionstart" 'payload "300"))
                      (expect (alist-ref 'value (car (space/read **state-space** (make-alist 'node 1 'type "motion")))) (to (be true)))
                      (expect (alist-ref 'value (car (space/read **state-space** (make-alist 'node 1 'type "light")))) (to (be 300))))

                  (it "properly sets state from motion ending"
                      (node/capture-state (make-alist 'topic "shc/sensors/data/1/motionend" 'payload ""))
                      (expect (alist-ref 'value (car (space/read **state-space** (make-alist 'node 1 'type "motion")))) (to (be false))))

                  (it "properly responds to sensor feature response"
                      (node/capture-state (make-alist 'topic "shc/sensors/data/1/features" 'payload "111"))
                      (expect (node-config/node-features (node-config/node 1)) (to (be 111))))

                  )

         (it "can load json config"
             (node/load-config-from-string "{\"nodes\": [{},
                                                         {\"id\": 1,
                                                          \"name\": \"Node A\",
                                                          \"features\": 0,
                                                          \"group\": {},
                                                          \"extensions\": [{\"bit\": 7, \"direction\": \"output\", \"name\": \"fan\"}]},
                                                         {\"id\": 2,
                                                          \"name\": \"Node 2\",
                                                          \"features\": 0,
                                                          \"light\": false}],
                                             \"groups\": [{},
                                                          {\"id\": 1, \"name\": \"Group 1\", \"nodes\": [1]}]}")
             (let ((node (node-config/node 1)))
               (expect (alist-ref 'id node) (to (be 1)))
               (expect (alist-ref 'name node) (to (be "Node A")))
               (expect (alist-ref 'features node) (to (be 0)))
               (expect (alist-ref 'extensions node) (to (be '#(((bit . 7) (direction . "output") (name . "fan")))))))))



