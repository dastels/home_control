;;; -*- scheme -*-

(use missbehave missbehave-matchers missbehave-stubs miscmacros files posix srfi-13 medea)

(define captured-topic "")
(define captured-message "")

(load (normalize-pathname "./lisp/initializers.scm"))
(load (normalize-pathname "./lisp/space.scm"))
(load (normalize-pathname "./lisp/util.scm"))
(load (normalize-pathname "./lisp/time.scm"))
(load (normalize-pathname "./lisp/mqtt.scm"))
(load (normalize-pathname "./lisp/mqtt-mux.scm"))
(load (normalize-pathname "./lisp/mqtt-queue.scm"))
(load (normalize-pathname "./lisp/node.scm"))
(load (normalize-pathname "./lisp/lighting-control.scm"))

;; (describe "Standalone node detecting motion"

;;           (before each: (begin
;;                           (set! mqtt-queue/send-now (lambda (topic qos message)
;;                                                       (set! captured-topic topic)
;;                                                       (set! captured-message message)))
                          
;;                           (set! captured-topic "")
;;                           (set! captured-message "")))
          
;;           (it "turns the light on when it sees motion"
;;               (lighting-control/motion-started-handler "shc/events/data/1/motionstart" "800")
;;               (expect captured-topic (to (be "shc/lighting/1")))
;;               (expect captured-message (to (be "{\"level\": 100}")))))
