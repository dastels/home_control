;;; -*- mode: Scheme -*-

(use missbehave missbehave-matchers missbehave-stubs miscmacros files)

(load (normalize-pathname "./lisp/mqtt-mux.scm"))


(context "MQTT mux"
         (define (f message) 42)
         (define (g message) 84)

         (define v '())
         (define (set-v message)
           (set! v (cdr message)))

         (before each: (mqtt-mux/reset))

		 (it "matches an exact pattern"
		     (expect (mqtt-mux/match-topic "shs/sensor/1" "shs/sensor/1") (to (be true))))
		 
		 (it "matches single wildcards"
		     (expect (mqtt-mux/match-topic "shs/sensor/1" "shs/+/1") (to (be true))))
		 
		 (it "matches rest-of-topic wildcard"
		     (expect (mqtt-mux/match-topic "shs/sensor/1" "shs/#") (to (be true))))
		 
		 (it "doesn't match a mismatch"
		     (expect (mqtt-mux/match-topic "shs/sensor/1" "shs/sensor/2") (to (be false)))
		     (expect (mqtt-mux/match-topic "shs/sensor/1" "shs/+") (to (be false)))
		     (expect (mqtt-mux/match-topic "shs/sensor/1" "shs/sensor/1/#") (to (be false))))
		 
		 (it "can register a handler for a specific pattern"
		     (mqtt-mux/register "topic/1/stuff" f)
		     (expect (length mqtt-mux/handlers) (to (be 1)))
		     (expect (caar mqtt-mux/handlers) (to (be "topic/1/stuff")))
		     (expect (cadar mqtt-mux/handlers) (to (be f))))

		 (it "can register multiple handlers for a pattern"
		     (mqtt-mux/register "topic/1/stuff" f)
		     (mqtt-mux/register "topic/1/stuff" g)
		     (expect (length mqtt-mux/handlers) (to (be 1)))
		     (expect (caar mqtt-mux/handlers) (to (be "topic/1/stuff")))
		     (expect (caddar mqtt-mux/handlers) (to (be f)))
		     (expect (cadar mqtt-mux/handlers) (to (be g))))

		 (it "can register handlers for multiple patterns"
		     (mqtt-mux/register "topic/1/stuff" f)
		     (mqtt-mux/register "topic/2/stuff" g)
		     (expect (length mqtt-mux/handlers) (to (be 2)))
		     (expect (caadr mqtt-mux/handlers) (to (be "topic/1/stuff")))
		     (expect (cadadr mqtt-mux/handlers) (to (be f)))
		     (expect (caar mqtt-mux/handlers) (to (be "topic/2/stuff")))
		     (expect (cadar mqtt-mux/handlers) (to (be g))))

		 (it "can find registered handlers in a simple situation"
		 	 (mqtt-mux/register "topic/1/stuff" f)
		 	 (expect (cdr (mqtt-mux/assoc-topic "topic/1/stuff" mqtt-mux/handlers)) (to (be (list f)))))

		 (it "can find registered handler in a multi-pattern situation"
		 	 (mqtt-mux/register "topic/1/stuff" f)
		     (mqtt-mux/register "topic/2/stuff" g)
		 	 (expect (cdr (mqtt-mux/assoc-topic "topic/1/stuff" mqtt-mux/handlers)) (to (be (list-including f))))
		 	 (expect (cdr (mqtt-mux/assoc-topic "topic/2/stuff" mqtt-mux/handlers)) (to (be (list-including g)))))

		 (it "can find registered handler in a wildcard situation"
		 	 (define (f client message) 42)
		     (define (g client message) 84)
		 	 (mqtt-mux/register "topic/+/stuff" f)
		     (mqtt-mux/register "topic/+/stuff" g)
		 	 (expect (cdr (mqtt-mux/assoc-topic "topic/1/stuff" mqtt-mux/handlers)) (to (be (list-including f))))
		 	 (expect (cdr (mqtt-mux/assoc-topic "topic/1/stuff" mqtt-mux/handlers)) (to (be (list-including g))))
		 	 (expect (cdr (mqtt-mux/assoc-topic "topic/2/stuff" mqtt-mux/handlers)) (to (be (list-including f))))
		 	 (expect (cdr (mqtt-mux/assoc-topic "topic/2/stuff" mqtt-mux/handlers)) (to (be (list-including g)))))

         (it "can dispatch"
             (set! v '())
		     (mqtt-mux/register "topic/+/stuff" set-v)
		     (mqtt_mux_dispatch "topic/1/stuff" "42")
		     (expect v (to (be "42"))))

		 (it "can deregister"
             (set! v '())
		     (mqtt-mux/register "topic/+/stuff" f)			 
		     (mqtt-mux/deregister "topic/+/stuff" f)			 
		     (mqtt-mux/dispatch (cons "topic/1/stuff" "84"))
		     (expect v (to (be '()))))


		 )
