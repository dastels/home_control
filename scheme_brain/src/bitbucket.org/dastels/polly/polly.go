// Copyright 2016 Dave Astels. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// This file provides a GoLisp interface to AWS Polly

package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/polly"
	"io"
	"os"
	"os/exec"
)

type SpeechData struct {
	Voice   string `json: "voice"`
	Text    string `json: "text"`
	CacheID string `json: "cacheid"`
	Wait    bool   `json: "wait"`
}

func saveStream(fname string, audioStream io.ReadCloser) (err error) {
	buf := make([]byte, 1024)
	var f *os.File
	f, err = os.Create(fname)
	defer f.Close()
	numberOfBytesRead := 0
	for {
		numberOfBytesRead, err = audioStream.Read(buf)
		if err == io.EOF {
			err = nil
			break
		} else if err != nil {
			return err
		}

		_, err = f.Write(buf[:numberOfBytesRead])
		if err != nil {
			return err
		}
	}
	return nil
}

func playSpeech(fname string, wait bool) {
	cmd := exec.Command("mplayer", "-quiet", "-nolirc", "-volume", "100", fname)
	if wait {
		cmd.Run()
	} else {
		cmd.Start()
	}
}

func makeCachePath(cacheId string) string {
	return os.ExpandEnv(fmt.Sprintf("$SHCPATH/polly_cache/%s.mp3", cacheId))
}

func speak(data SpeechData) (err error) {
	cachable := false
	fname := ""
	force := true
	wait := false
	cacheId := ""
	if data.CacheID != "" {
		cacheId = data.CacheID
		fname = makeCachePath(cacheId)
		cachable = true
		force = false
		if _, err := os.Stat(fname); err != nil {
			if os.IsNotExist(err) {
				force = true
			}
		}
	}
	wait = data.Wait

	if !cachable {
		fname = makeCachePath("disposable")
	}

	//var sess *session.Session
	var resp *polly.SynthesizeSpeechOutput
	if !cachable || force {
		sess, err := session.NewSession(&aws.Config{Region: aws.String("us-east-1")})
		if err != nil {
			return err
		}

		svc := polly.New(sess)

		params := &polly.SynthesizeSpeechInput{
			OutputFormat: aws.String(polly.OutputFormatMp3),
			Text:         aws.String(data.Text),
			VoiceId:      aws.String(data.Voice),
			TextType:     aws.String(polly.TextTypeText),
		}
		resp, err = svc.SynthesizeSpeech(params)
		if err != nil {
			return err
		}

		err = saveStream(fname, resp.AudioStream)
		if err != nil {
			return err
		}
	}

	playSpeech(fname, wait)

	return nil
}

func main() {
	var input string
	var data SpeechData

	scanner := bufio.NewScanner(os.Stdin)

	for true {
		for scanner.Scan() {
			input = scanner.Text()
			err := json.Unmarshal(([]byte)(input), &data)
			if err != nil {
				fmt.Fprintln(os.Stdout, "error:", err)
			} else {
				speak(data)
			}
		}
		if err := scanner.Err(); err != nil {
			fmt.Fprintln(os.Stdout, "reading standard input:", err)
		}

	}

}

// {"voice":"Amy", "text":"Good morning", "cacheid":"", "wait":true}
