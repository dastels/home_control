// Copyright 2016 Dave Astels. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// This file provides a GoLisp interface to AWS DynamoDB

package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"os"
)

type Data struct {
	Endpoint  string `json: "endpoint"`
	Command   string `json: "command"`
	Tablename string `json: "tablename"`
	Timestamp string `json: "timestamp"`
	Sortkey   string `json: "sortkey"`
	Node      string `json: "node"`
	Type      string `json: "type"`
	Value     string `json: "value"`
}

func dbclient(data Data) *dynamodb.DynamoDB {
	awsConfig := &aws.Config{
		Region:   aws.String("us-east-1"),
		Endpoint: aws.String(data.Endpoint),
	}
	return dynamodb.New(session.New(awsConfig))
}

func listTables(data Data) string {
	db := dbclient(data)
	params := &dynamodb.ListTablesInput{}
	resp, err := db.ListTables(params)
	if err != nil {
		return fmt.Sprintf("error: %s", err)
	}

	b, err := json.Marshal(resp.TableNames)
	if err != nil {
		return fmt.Sprintf("error: %s", err)
	}
	return string(b)
}

// (dynamo/create-table table-name)
func createTable(data Data) string {
	db := dbclient(data)

	params := &dynamodb.CreateTableInput{
		AttributeDefinitions: []*dynamodb.AttributeDefinition{
			{
				AttributeName: aws.String("timestamp"),
				AttributeType: aws.String("S"),
			},
			{
				AttributeName: aws.String("sort-key"),
				AttributeType: aws.String("S"),
			},
		},
		KeySchema: []*dynamodb.KeySchemaElement{
			{
				AttributeName: aws.String("timestamp"),
				KeyType:       aws.String("HASH"),
			},
			{
				AttributeName: aws.String("sort-key"),
				KeyType:       aws.String("RANGE"),
			},
		},
		ProvisionedThroughput: &dynamodb.ProvisionedThroughput{
			ReadCapacityUnits:  aws.Int64(1),
			WriteCapacityUnits: aws.Int64(1),
		},
		TableName: aws.String(data.Tablename),
	}
	_, err := db.CreateTable(params)

	if err != nil {
		return fmt.Sprintf("error: %s", err)
	}
	return "OK"
}

// (dynamo/add table-name {key: value ... })
func insertItem(data Data) string {
	db := dbclient(data)

	params := &dynamodb.PutItemInput{
		TableName: aws.String(data.Tablename),
		Item: map[string]*dynamodb.AttributeValue{
			"timestamp": {S: aws.String(data.Timestamp)},
			"sort-key":  {S: aws.String(data.Sortkey)},
			"node":      {N: aws.String(data.Node)},
			"type":      {S: aws.String(data.Type)},
			"value":     {N: aws.String(data.Value)},
		},
	}

	_, err := db.PutItem(params)

	if err != nil {
		return fmt.Sprintf("error: %s", err)
	}

	return "OK"
}

func main() {
	var input string
	var data Data

	scanner := bufio.NewScanner(os.Stdin)

	for true {
		for scanner.Scan() {
			input = scanner.Text()
			err := json.Unmarshal(([]byte)(input), &data)
			if err != nil {
				fmt.Fprintln(os.Stdout, "error:", err)
			} else {
				switch data.Command {
				case "listtables":
					fmt.Fprintln(os.Stdout, listTables(data))
				case "createtable":
					fmt.Fprintln(os.Stdout, createTable(data))
				case "insert":
					fmt.Fprintln(os.Stdout, insertItem(data))
				default:
					fmt.Fprintln(os.Stdout, "Bad Command")
				}
			}
		}
		if err := scanner.Err(); err != nil {
			fmt.Fprintln(os.Stdout, "reading standard input:", err)
		}

	}

}
