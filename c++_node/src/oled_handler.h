// OLED logging handler (mainly for debug mode)
//
// Copyright (c) 2020 Dave Astels

#ifndef __OLED_HANDLER_H__
#define __OLED_HANDLER_H__
#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SH110X.h>

#include "logging_handler.h"

class OledHandler: public LoggingHandler
{
 public:
  OledHandler();
  void emit(const char *level_name, const char *msg);
  const char* name(void) { return "oled"; }
  void clear(void);
private:
  Adafruit_SH110X *display;
};


#endif
