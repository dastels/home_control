// -*- mode: c++ -*-

// The MIT License (MIT)
//
// Copyright (c) 2020 Dave Astels
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANgTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#include <SPI.h>
#include <WiFiNINA.h>
#include <ArduinoJson.h>
#include <Wire.h>

#include "config.h"
#include "logging.h"
#include "serial_handler.h"
#include "neopixel_handler.h"
#include "oled_handler.h"
#include "motion.h"
#include "indicator.h"
#include "hue.h"
#include "group.h"

Logger *logger;

Motion pir(PIR_PIN);
Indicator pixel(NEOPIXEL_PIN);
// Initialize the SSL client library
// with the IP address and port of the server
// that you want to connect to (port 443 is default for HTTPS):
WiFiClient client;
WiFiSSLClient ssl_client;
Hue hue(&client, &ssl_client);

char ssid[] = WIFI_SSID;        // your network SSID (name)
char pass[] = WIFI_PASS;    // your network password (use for WPA, or use as key for WEP)
int keyIndex = 0;            // your network key Index number (needed only for WEP)

int status = WL_IDLE_STATUS;

Group **groups = nullptr;
Group *group = nullptr;

unsigned long lights_off_time = 0;

void log_ip(const char *prefix, IPAddress ip)
{
  logger->debug("%s %d.%d.%d.%d", prefix, ip[0], ip[1], ip[2], ip[3]);
}



void printWifiStatus() {
  logger->debug("SSID: %s", WiFi.SSID());
  log_ip("IP Address:", WiFi.localIP());
  log_ip("Subnet mask:", (IPAddress)WiFi.subnetMask());
  log_ip("Gateway IP :", (IPAddress)WiFi.gatewayIP());
  byte mac[6];
  WiFi.macAddress(mac);
  logger->debug("MAC address: %02x:%02x:%02x:%02x:%02x",   mac[5], mac[4], mac[3], mac[2], mac[1], mac[0]);
  long rssi = WiFi.RSSI();
  logger->debug("signal strength (RSSI): %ld dBm", rssi);
}

void printEncryptionType(int thisType)
{
  switch (thisType) {
    case ENC_TYPE_WEP:
      logger->debug("WEP");
      break;
    case ENC_TYPE_TKIP:
      logger->debug("WPA");
      break;
    case ENC_TYPE_CCMP:
      logger->debug("WPA2");
      break;
    case ENC_TYPE_NONE:
      logger->debug("None");
      break;
    case ENC_TYPE_AUTO:
      logger->debug("Auto");
      break;
    case ENC_TYPE_UNKNOWN:
    default:
      logger->debug("Unknown");
      break;
  }
}

Group *group_numbered(uint8_t group_number)
{
  for (int i = 0; groups[i] != 0; i++) {
    if (groups[i]->number() == group_number) {
      return groups[i];
    }
  }
  return nullptr;
}


// Check for a 128x64 OLED featherwing

bool oled_present()
{
  Wire.beginTransmission(0x3C);
  return Wire.endTransmission() == 0;
}


void setup_logging()
{
  logger = Logger::get_logger();
  logger->set_level(level_for(LOG_LEVEL));
  if (oled_present()) {
    logger->add_handler(new OledHandler());
  }
  if (level_for(LOG_LEVEL) <= LogLevel::INFO) {
    logger->add_handler(new SerialHandler());
  }
  logger->add_handler(new NeopixelHandler(&pixel));
  logger->report();
}


void setup()
{
  Wire.begin();
  pixel.show(0x00FF00);
  setup_logging();
  logger->info("Starting up");

  // check for the WiFi module:
  WiFi.setPins(SPIWIFI_SS, SPIWIFI_ACK, ESP32_RESETN, ESP32_GPIO0, &SPIWIFI);
  if (WiFi.status() == WL_NO_MODULE) {
    logger->critical("Communication with WiFi module failed!");
    while (true);
  }

  String fv = WiFi.firmwareVersion();
  if (fv < "1.0.0") {
    logger->debug("Please upgrade the firmware");
  }

  // attempt to connect to Wifi network:
  logger->debug("Attempting to connect to SSID: %s", ssid);

  // Connect to WPA/WPA2 network. Change this line if using open or WEP network:
  do {
    status = WiFi.begin(ssid, pass);
    delay(100); // wait until connected
  } while (status != WL_CONNECTED);
  logger->debug("Connected to wifi");

  printWifiStatus();
  for (int tries = 0; tries <= 10; tries++) {
    if (tries == 10) {
        logger->critical("Failed to get HUE IP");
        while (true);
    }
    if (hue.fetch_hue_ip()) {
      break;
    }
  }

  groups = hue.groups();
  if (groups == nullptr) {
    logger->critical("Problem gettin groups");
    while (true);
  }
  for (int i = 0; i < 16; i++) {
    if (groups[i] == NULL) {
      break;
    }
    logger->debug("Finding lights for group %d %s", groups[i]->number(), groups[i]->name());
    groups[i]->lights(hue.lights_for_group(groups[i]->number()));
  }

  group = group_numbered(GROUP);
  if (!group) {
    logger->critical("No group found numbered %d", GROUP);
    while (true);
  }
  pixel.show(0x000000);
}

unsigned long printed_at = 0;

bool time_for_lights_off_p(void)
{
  if (lights_off_time > 0) {
    if (millis() >= lights_off_time) {
      printed_at = 0;
      return true;
    } else {
      unsigned long seconds_remaining = (lights_off_time - millis()) / 1000;
      if (seconds_remaining % 10 == 0) {
        unsigned long chunked_seconds_remaning = seconds_remaining / 10;
        if (chunked_seconds_remaning != printed_at) {
          printed_at = chunked_seconds_remaning;
          logger->info("Lights off in %d seconds", seconds_remaining);
        }
      }
    }
  }
  return false;
}


void set_lights_off_time()
{
  lights_off_time = millis() + OFF_DELAY;
  logger->info("Setting lights off timeout to %d seconds", OFF_DELAY / 1000);
}


void reset_lights_off_time()
{
  lights_off_time = 0;
  logger->info("Resetting lights off timeout");
}


void loop()
{
  pir.update();

  if (pir.started_p()) {
    logger->debug("Motion started");
    if (!group->turn_on(100)) {
      logger->critical("Couldn't turn group %d on", GROUP);
    } else {
      logger->clear();
      reset_lights_off_time();
    }
  } else if (pir.ended_p()) {
    logger->debug("Motion ended");
    set_lights_off_time();
  } else if (time_for_lights_off_p()) {
    logger->debug("Turning off group %d", GROUP);
    if (!group->turn_off()) {
      logger->critical("Couldn't turn group %d off", GROUP);
    } else {
      logger->clear();
    }
    reset_lights_off_time();
  }

}
