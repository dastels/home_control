// Adafruit IO logging handler (mainly for debug mode)
//
// Copyright (c) 2020 Dave Astels

#ifndef __AIO_HANDLER_H__
#define __AIO_HANDLER_H__

#include "logging_handler.h"

class AioHandler: public LoggingHandler
{
 public:
  AioHandler();
  void emit(const char *level_name, const char *msg);
  const char* name(void) { return "aio"; }
};


#endif
