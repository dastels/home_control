// -*- mode: c++ -*-

// The MIT License (MIT)
//
// Copyright (c) 2020 Dave Astels
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#include <Arduino.h>

#include "group.h"
#include "hue.h"
#include "logging.h"

extern Logger *logger;

Group::Group(Hue *hue, int group_number, const char *group_name)
  : _hue(hue)
  , _lights(nullptr)
  , _number(group_number)
  , _name(group_name)
{
}

bool Group::turn_on(uint8_t brightness)
{
  uint8_t counter = 0;
  bool all_on = false;
  bool valid;

  while (!all_on) {
    _hue->update_group(_number, true, (uint8_t)(brightness * 2.54));
    delay(100);

    all_on = true;
    for (int i = 0; i < 8 && _lights[i] != 0; i++) {
      bool state = _hue->light_on_p(_lights[i], valid);
      if (!valid) {
        all_on = false;
        break;
      }
      all_on = all_on && state;
    }

    if (!all_on) {
      counter++;
      if (counter >= 25) {
        return false;
      } else if (counter > 5) {
        logger->warning("Trouble turning on group %d", _number);
      }
    }
  }

  return true;
}

bool Group::turn_off(void)
{
  uint8_t counter = 0;
  bool all_off = false;
  bool valid;

  while (!all_off) {
    _hue->update_group(_number, false, 0);
    delay(100);

    all_off = true;
    for (int i = 0; i < 8 && _lights[i] != 0; i++) {
      bool state = _hue->light_on_p(_lights[i], valid);
      if (!valid) {
        all_off = false;
        break;
      }
      all_off = all_off && !state;
    }

    if (!all_off) {
      counter++;
      if (counter >= 25) {
        return false;
      } else if (counter > 5) {
        logger->warning("Trouble turning off group %d", _number);
      }
    }
  }

  return true;
}


void Group::print(void)
{
  Serial.print(_number);
  Serial.print(" ");
  Serial.print(_name);
  Serial.print(" [");
  for (int i = 0; i < 8 && _lights[i] != 0; i++) {
    if (i > 0) {
      Serial.print(", ");
    }
    Serial.print(_lights[i]);
  }
  Serial.println("]");
}
