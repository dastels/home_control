;;; Copyright 2016 Dave Astels. All rights reserved.
;;; Use of this source code is governed by a BSD-style
;;; license that can be found in the LICENSE file.
;;;
;;; This file manages initialization
;;; ----------------------------------------------------------------------------


(define **initializers** nil)


(define (initializers/register f)
  (set! **initializers** (cons f **initializers**)))

(define (initializers/run data-dir)
  (log-it "Initializing:")
  (for-each (lambda (f)
              (f data-dir))
            **initializers**))



