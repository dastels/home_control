;;; Hue lighting device


;;; --------------------------------------------------------------------------------
;;;  Config functions

(define hue/config {})

(define (hue/group->lights group-name)
  (let ((group (find (lambda (g)
                      (equal? (name: g) group-name))
                    (groups: hue/config))))
    (when group
      (get-slot-or-nil group lights:))))


(define (hue/node->lights node)
  (let ((group-name (node/node->group node)))
    (unless (nil? group-name)
      (hue/group->lights group-name))))


;;; --------------------------------------------------------------------------------
;;; Utility functions

(define (hue/unitize rgb)
  (map (lambda (p) (/ (float p) 255.0))
       rgb))


(define (hue/gamma-correct rgb)
  (map (lambda (colour-part)
         (if (> colour-part 0.04045)
           (expt (/ (+ colour-part 0.055) 1.055) 2.4)
           (/ colour-part 12.92)))
       (hue/unitize rgb)))


(define (hue/rgb->xy r g b)
  (let* ((corrected-rgb (hue/gamma-correct (list r g b)))
         (r (first corrected-rgb))
         (g (second corrected-rgb))
         (b (third corrected-rgb))
         (x (+ (* r 0.664511) (* g 0.154324) (* b 0.162028)))
         (y (+ (* r 0.283881) (* g 0.668433) (* b 0.047685)))
         (z (+ (* r 0.000088) (* g 0.072310) (* b 0.986039))))
    (list (/ x (+ x y z))
          (/ y (+ x y z)))))

(define hue/hue-bridge-ip (internalipaddress: (car (json->lisp (cadr (net/get "https://www.meethue.com/api/nupnp"))))))
;(define hue/hue-bridge-ip "10.215.222.117")


(define (hue/make-lights-url . args)
  (let ((id (if (nil? args)
              ""
              (format #f "/~A" (car args))))
        (command (if (nil? (cdr args))
                   ""
                   (format #f "/~A" (cadr args)))))
    (format #f "http://~A/api/~A/lights~A~A"
            hue/hue-bridge-ip
            (user: hue/config)
            id
            command)))


(define (hue/make-groups-url)
  (format #f "http://~A/api/~A/groups" hue/hue-bridge-ip (user: hue/config)))


(define (hue/colour-light? light-id)
  (let ((found (memp (lambda (l)
                       (eqv? (number: l) light-id))
                     (lights: hue/config))))
	(and (not (false? found))
		 (not (false? (re-string-match-go ".*color.*" (type: (car found))))))))


(define (hue/colour-group? group-name)
  (every hue/colour-light? (hue/group->lights group-name)))


;;; --------------------------------------------------------------------------------
;;; Public functions


(define (hue/light-id->name light-id)
  (let ((found (memp (lambda (l)
                       (eqv? (number: l) light-id))
                     (lights: hue/config))))
    (if (false? found)
      ""
      (name: (car found)))) )

;;; Single light control

(define (hue/percent->brightness p)
  (integer (/ (* p 254) 100)))


(define (hue/on-off light-id on-off)
  (log-it "HUE: Turning ~A light ~A" (if on-off "on" "off") (hue/light-id->name light-id))
  (net/request 'put (hue/make-lights-url light-id 'state) {} (lisp->json {on: on-off})))

(define (hue/off light-id)
  (log-it "HUE: Turning off light ~A" (hue/light-id->name light-id))
  (net/request 'put (hue/make-lights-url light-id 'state) {} (lisp->json {on: #f})))

(define (hue/on light-id percent-level)
  (log-it "HUE: Turning on light ~A" (hue/light-id->name light-id))
  (net/request 'put (hue/make-lights-url light-id 'state) {} (lisp->json {on: #t bri: (hue/percent->brightness percent-level)})))

(define (hue/colour light-id r g b percent-brightness)
  (let ((brightness (hue/percent->brightness percent-brightness)))
	(log-it "HUE: Setting colour on light ~A to (~A, ~A, ~A) at brightness ~A" (hue/light-id->name light-id) r g b brightness)
	(net/request 'put (hue/make-lights-url light-id 'state) {} (lisp->json {on: #t bri: brightness xy: (hue/rgb->xy r g b)}))))

(define (hue/on? light-id)
  (let ((response (json->lisp (cadr (net/request 'get (hue/make-lights-url light-id) {} (lisp->json {}))))))
    (on: (state: response))))

;;; --------------------------------------------------------------------------------
;;; Group control

(define (hue/group-off group-name)
  (for-each (lambda (light-id)
              (hue/off light-id))
            (hue/group->lights group-name)))

(define (hue/group-on group-name brightness)
  (for-each (lambda (light-id)
              (hue/on light-id brightness))
            (hue/group->lights group-name)))

(define (hue/group-colour group-name r g b p)
  (for-each (lambda (light-id)
              (hue/colour light-id r g b p))
            (hue/group->lights group-name)))

(define (hue/handler client message)
  ;; (log-it "In HUE handler~%")
  (on-error 
   (let* ((node (node-number-from (message->command-info message)))
          (group-name (node/node->group node))
          (value (json->lisp (payload: message))))
     ;; (log-it "Setting ~A (~A) to ~A~%" node group-name value)
     (cond ((level:? value)
            (if (eqv? (level: value) 0)
                (hue/group-off group-name)
                (hue/group-on group-name (level: value))))
           ((red:? value)
            (let ((r (red: value))
                  (g (green: value))
                  (b (blue: value))
				  (p (brightness: value)))
              (if (or (zero? p)
					  (zero? (+ r g b)))
                  (hue/group-off group-name)
                  (hue/group-colour group-name r g b p))))))
   (lambda (err-string)
     (log-it "Error ~S while processing ~S" err-string message))))


(define (hue/node-lights-on? node)
  (let ((lights (hue/node->lights node)))
    (if (nil? lights)
      #f
      (every hue/on? lights))))


;;; --------------------------------------------------------------------------------
;;; Intialization


(define (hue/scan-lights)
  (let* ((response (json->lisp (cadr (net/request 'get (hue/make-lights-url) {} "{}")))))
    (map (lambda (key)
           (let ((light (get-slot response key))
                 (id (string->number (string-trim-right (symbol->string key) "0123456789"))))
             {number: id
              modelid: (modelid: light)
              type: (type: light)
              name: (name: light)}))
         (frame-keys response))))


(define (hue/scan-groups)
  (let* ((response (json->lisp (cadr (net/request 'get (hue/make-groups-url) {} "{}")))))
    (map (lambda (g)
           {name: (name: g)
            lights: (map string->number (lights: g))})
         (frame-values response))))


(define (hue/load-config data-dir)
  (let ((in (open-input-file (format #f "~A/hue-config.json" data-dir))))
    (if (nil? in)
      (error "Invalid HUE configuration file.")
      (let ((data (json->lisp (read-string in))))
        (close-port in)
        (if (nil? (frame-keys data))
          (log-it "Error parsing in hue-config.json: ~A" data)
          (set! hue/config data))))))


(initializers/register (lambda (data-dir)
                         (log-it "  hue")
                         (hue/load-config data-dir)
                         (lights:! hue/config (hue/scan-lights))
                         (groups:! hue/config (hue/scan-groups))
                         (lighting/register-light-state-function hue/node-lights-on?)
                         (mqtt-mux/register "shc/lighting/#" hue/handler)))

