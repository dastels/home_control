;;; 

(define (string->naked-sym s)
  (intern (format #f "~a:" s)))


(define (node-value-query-handler client message)
  (let ((command (payload: message))
		(node-id (node-number-from (message->sensor-info message))))
	(when (equal? command "get-values")
		  (let* ((entries (read:> state-space {node: node-id}))
				 (json (lisp->json (fold-left (lambda (f e)
												(set-slot! f (string->naked-sym (type: e)) (value: e))
												f)
											  {}
											  entries))))
			(mqtt/publish (format #f "shc/node/~A/values" node-id) 0 json)))))


(initializers/register (lambda (data-dir)
                         (log-it "  ui support")
                         (mqtt-mux/register "shc/node/+" node-value-query-handler)))
