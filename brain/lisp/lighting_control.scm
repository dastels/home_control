;;; Copyright 2016 Dave Astels. All rights reserved.
;;; Use of this source code is governed by a BSD-style
;;; license that can be found in the LICENSE file.

;;; This file handles motion based lighting control

(define lighting-control/lights-off-delay 300000)


(define (lighting-control/need-light? light-level)
  (< light-level lighting-control/need-light-threshold))


(define (lighting-control/appropriate-light-level)
  (if (usually-awake-at? (seconds))
    100
    10))


(define (lighting-control/appropriate-colour)
  (if (usually-awake-at? (seconds))
	  '(#xff #xff #xc0 100)
	  '(#x1f #x00 #x00 10)))

(define (lighting-control/light-at-last-turnon node)
  (let ((light-level (read:> state-space {type: "light-at-last-turnon" node: node}))))
  (if (nil? light-level)
    0
    (value: (car light-level))))


(define (lighting-control/light-needed-threshold node)
  (let ((threshold (read:> persistent-space {type: "light-needed-threshold" node: node})))
    (if (nil? threshold)
      400
      (value: (car threshold)))))



(define (lighting-control/light-needed? node light-level)
  (or (dark-out?)
	  (let ((threshold (lighting-control/light-needed-threshold node)))
		(if (lighting/node-lights-on? node)
		  (< (lighting-control/light-at-last-turnon node) threshold)  
		  (< light-level threshold)))))


(define (lighting-control/group-containing node)
  (let ((found (memp (lambda (g) (eqv? node (node: g)))
					 (groups: node/config))))
	(and found
		 (car found))))


(define (lighting-control/check-group-nodes-motion group)
  (let ((combiner (eval (intern (combine-using: group)))))
    (apply combiner (map (lambda (n)
                           (node/motion-detected? n))
                         (nodes: group)))))


(define (lighting-control/motion-started-handler client message)
  (let* ((node (node-number-from (message->sensor-info message)))
         (current-light (string->number (payload: message)))
         (need-more-light? (or (not (node/use-light? node))
                               (lighting-control/light-needed? node current-light))))
    (log-it "Motion started on sensor ~A~A" node (if need-more-light? ": light is required" ""))
    (node/set-led node 0 0 32)
    (when need-more-light?
      (lighting/cancel-for node) ; cancel the scheduled turn-off command
      (unless (nil? (hue/node->lights node))
        (take:> state-space {type: 'light-at-last-turnon node: node})
        (write:> state-space {type: 'light-at-last-turnon node: node value: current-light}))
	  (if (hue/colour-group? (node/node->group node))
		  (apply lighting/colour (cons node (lighting-control/appropriate-colour)))
		  (lighting/on node (lighting-control/appropriate-light-level))))))


(define (lighting-control/sound-handler client message)
  (let ((node (node-number-from (message->sensor-info message))))
    (when (mqtt-queue/scheduled? (lighting/node->topic node)) ; lights waiting to turn off
      (lighting/cancel-for node)
      (lighting/off-in node (node/lights-off-time node)))))


(define (lighting-control/motion-ended-handler client message)
  (let ((node (node-number-from (message->sensor-info message))))
    (node/set-led node 0 0 0)
    (log-it "Motion ended on sensor ~A" node)
    (lighting/off-in node (node/lights-off-time node))))


(initializers/register (lambda (data-dir)
                         (log-it "  light control")
                         (mqtt-mux/register "shc/events/data/+/motionstart" lighting-control/motion-started-handler)
                         (mqtt-mux/register "shc/events/data/+/motionend" lighting-control/motion-ended-handler)
                         (mqtt-mux/register "shc/events/data/+/sound" lighting-control/sound-handler)))

