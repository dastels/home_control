;;; Copyright 2016 Dave Astels. All rights reserved.
;;; Use of this source code is governed by a BSD-style
;;; license that can be found in the LICENSE file.

;;; This file handles nodes

;;; node: node id
;;; type: "config"
;;; features: feature bits
;;; light-levels: [
;;;   {from: start-time to: end-time level: light-level}
;;;   ...
;;; ]

;;; --------------------------------------------------------------------------------
;;; Node feature management

(define node/config {nodes: #() groups: #()})


;;; Feature bit masks

(define node/motion-mask #x01)
(define node/light-mask #x02)
(define node/prox-mask #x04)
(define node/temp-rh-mask #x08)
(define node/neopixel-mask #x20)


;;; Capture and record the feature bits sent by a node
(define (node/record-features node features)
  (let ((node-data (vector-ref (nodes: node/config) node)))
    (unless (nil? node-data)
      (features:! node-data features))))


(define (node/features-for node)
  (let ((node-data (vector-ref (nodes: node/config) node)))
    (if (and node-data
			 (features:? node-data))
		(features: node-data)
		0)))


;;; Check whether or not a node has a given feature
(define (node/check-feature-bit node mask)
  (not (zero? (binary-and (node/features-for node) mask))))


;;; Ask a node to send its feature bits
(define (node/request-features node)
  (mqtt-queue/send-now (format #f "shc/sensors/command/~A/features" node) 0 ""))


(define (node/has-motion? node)
  (node/check-feature-bit node node/motion-mask))


(define (node/has-light? node)
  (node/check-feature-bit node node/light-mask))


(define (node/has-proximity? node)
  (node/check-feature-bit node node/prox-mask))


(define (node/has-temperature? node)
  (node/check-feature-bit node node/temp-rh-mask))


(define (node/has-neopixels? node)
  (node/check-feature-bit node node/neopixel-mask))


;;; a hack while experimenting. Will eventually be mask #x80 if it works out
(define (node/has-sound? node)
  (eqv? node 9))


(define (node/motion-detected? node)
  (let ((data (read:> state-space {node: node type: "motion"})))
    (if (nil? data)
      #f
      (value: (car data)))))


(define (node/use-light? node)
  (let ((node-data (vector-ref (nodes: node/config) node)))
    (and (notnil? node-data)
         (light:? node-data)
         (light: node-data))))


(define (node/lights-off-time node)
  (let ((node-data (vector-ref (nodes: node/config) node)))
    (if (and (notnil? node-data)
             (lights-off-in:? node-data))
      (minutes->millis (lights-off-in: node-data))
      0)))


;;; --------------------------------------------------------------------------------
;;; Extension port

(define (node/extension-named node extension-name)
  (let ((node-record (vector-ref (nodes: node/config) node)))
    (if (extensions:? node-record)
      (let ((ext-data (memp (lambda (ext)
                              (equal? (name: ext) extension-name))
                            (extensions: node-record))))
        (unless (false? ext-data)
          (car ext-data))))))


(define (node/has-extension-named? node ext-name)
  (notnil? (node/extension-named node ext-name)))


(define (node/extension-bit node ext-name)
  (let ((ext-data (node/extension-named node ext-name)))
    (unless (nil? ext-data)
      (bit: ext-data))))


(define (node/extension-direction node ext-name)
  (let ((ext-data (node/extension-named node ext-name)))
    (unless (nil? ext-data)
      (direction: ext-data))))


(define (node/configure-extension-port node directions pullups)
  (when (and (byte? directions)
             (byte? pullups))
    (mqtt-queue/send-now (format #f "shc/sensors/command/~A/ext-config" node) 0 (format #f "~A ~A" directions pullups))))


(define (node/read-extension-bit node bit)
  (mqtt-queue/send-now (format #f "shc/sensors/command/~A/ext-read" node) 0 (number->string bit)))


(define (node/write-extension-bit node bit value)
    (when (bit-number? bit)
      (mqtt-queue/send-now (format #f "shc/sensors/command/~A/ext-write" node) 0 (format #f "~A ~A" bit (if value "1" "0")))))


;;; --------------------------------------------------------------------------------
;;; Fan control

(define node/fan-bit 7)

(define (node/has-fan? node)
  (node/has-extension-named? node "fan"))


(define (node/fan-on-off node flag)
  (when (node/has-fan? node)
    (let ((fan-bit (node/extension-bit node "fan")))
      (node/write-extension-bit node fan-bit (not flag))))) ; negated because relay control is active low


;;; --------------------------------------------------------------------------------
;;; Lighting control

;;; Set the colour of a nodes neopixel array
(define (node/set-neopixels node red green blue)
  (if (node/has-neopixels? node)
    (mqtt-queue/send-now (format #f "shc/sensors/command/~A/neopixel" node) 0 (format #f "~A ~A ~A" red green blue))))


;;; Set the colour of a nodes LED, and optionally blink it
(define (node/set-led node red green blue . blinking)
  (let ((off (if (nil? blinking) 0 (car blinking)))
        (on (if (nil? blinking) 0 (cadr blinking))))
    (mqtt-queue/send-now (format #f "shc/sensors/command/~A/led" node) 0 (format #f "~A ~A ~A ~A ~A" red green blue off on))))


;;; --------------------------------------------------------------------------------
;;; Managing all nodes

;;; Return a list of all known node ids
(define (node/ids)
  (map (lambda (node)
         (id: node))
       (filter notnil? (nodes: node/config))))


;;; --------------------------------------------------------------------------------
;;; Handle incoming sensor date from nodes

(define (node/save-to-dynamo node type value)
  (let ((timestamp (format #f "~A ~A" (date->string (date-today)) (time->24hr-string (time-now))))
        (sort-key (format #f "~A ~A" node type)))
    (dynamo/add {timestamp: timestamp sort-key: sort-key node: node type: type value: value}))
  )


(define (node/update-state node type value)
  (take:> state-space {node: node type: type})
  (write:> state-space {timestamp: (format #f "~A ~A" (date->string (date-today)) (time->24hr-string-with-seconds (time-now)))
                        node: node
                        type: type
                        value: value})
  (unless (equal? type "proximity")
    (node/save-to-dynamo node type value)))


(define (node/get-state node type)
  (car (read:> state-space {node: node type: type})))


(define (node/capture-state client message)
  (let* ((info (message->sensor-info message))
		 (node (node-number-from info))
		 (type (sensor-name-from info))
         (payload (payload: message))
         (event-topic (format #f "shc/events/data/~A/~A" node type)))
    (if (equal? type "register")
      (node/request-features node)
      (begin
	(cond ((equal? type "motion") 
	       (node/update-state node "motion" (eq? payload "1")))
	      ((equal? type "motionstart")
	       (node/update-state node "motion" #t)
	       (node/update-state node "light" (string->number payload)))
	      ((equal? type "motionend")
	       (node/update-state node "motion" #f))
	      ((equal? type "features")
	       (node/record-features node (integer (string->number payload))))
	      (else
	       (unless (equal? type "sound")
		       (node/update-state node type (string->number payload)))))
	(mqtt-queue/send-now event-topic 0 payload)))))


(define (node/node->group node)
  (let ((node-record (vector-ref (nodes: node/config) node)))
    (if (group:? node-record)
      (name: (group: node-record))
      (name: node-record))))


(define (node/load-config data-dir)
    (let ((in (open-input-file (format #f "~A/node-config.json" data-dir))))
    (if (nil? in)
      (error "Invalid Node configuration file.")
      (let ((data (json->lisp (read-string in))))
        (close-port in)
        (if (nil? (frame-keys data))
          (log-it "Error parsing in node-config.json: ~A" data)
          (let* ((highest-node-id (fold-left (lambda (h n)
                                               (if (> (id: n) h)
                                                 (id: n)
                                                 h))
                                             0
                                            (nodes: data)))
                 (node-vector (make-vector (1+ highest-node-id) nil)))
            (for-each (lambda (n)
                        (vector-set! node-vector (id: n) n))
                      (nodes: data))
            (for-each (lambda (group)
                        (for-each (lambda (node-id)
                                    (group:! (vector-ref node-vector node-id) group))
                                  (nodes: group)))
                      (groups: data))
            (nodes:! node/config node-vector)
            (groups:! node/config (groups: data))))))))


(define (node/try-to-fetch-features node-data)
  (unless (nil? node-data)
	(do ((count 5 (-1+ count))
		 (node (id: node-data)))	; give up after several tries (node must not be live or in range)
		((or (zero? count)
			 (not (zero? (node/features-for node))))
		 (when (zero? count)
		   (log-it "Node ~A is unresponsive." node)))
	  (node/request-features node)
	  (sleep 250))
	(sleep (+ 100 (random 500)))))

(define (node/get-features)
  (vector-for-each node/try-to-fetch-features
                   (nodes: node/config)))


(initializers/register (lambda (data-dir)
                         (mqtt-mux/register "shc/sensors/data/#" node/capture-state)
                         (log-it "  node interaction")
                         (node/load-config data-dir)
                         (node/get-features)))
