;;; Copyright 2016 Dave Astels. All rights reserved.
;;; Use of this source code is governed by a BSD-style
;;; license that can be found in the LICENSE file.

;;; This file provides the generic lighting interface

;;; PRIVATE HELPERS


(define **light-state-function** nil)

(define (lighting/register-light-state-function f)
  (set! **light-state-function** f))


(define (lighting/node-lights-on? node)
  (if (nil? **light-state-function**)
    #f
    (**light-state-function** node)))


(define (lighting/node->topic node)
  (format #f "shc/lighting/~A" node))


(define (lighting/send-in topic-name payload millis)
  (mqtt-queue/unschedule topic-name)
  (if (zero? millis)
    (mqtt-queue/send-now topic-name 0 payload)
    (mqtt-queue/send-in millis topic-name 0 payload)))



(define (lighting/set-node-level-in node level millis)
  (let ((topic-name (lighting/node->topic node))
        (payload (lisp->json {level: level})))
    (lighting/send-in topic-name payload millis)))


(define (lighting/set-node-rgb-in node red green blue brightness millis)
  (let ((topic-name (lighting/node->topic node))
        (payload (lisp->json {red: red green: green blue: blue brightness: brightness})))
    (lighting/send-in topic-name payload millis)))



(define (lighting/cleanup-state node)
  (log-it "Cleaning up light state for node ~A" node)
  (take:> state-space {type: 'light-on node: node})
  (take:> state-space {type: 'light-at-last-turnon node: node}))


(define (lighting/event-handler client message)
  (let* ((node (node-number-from (message->command-info message)))
         (value (json->lisp (payload: message))))
    (cond ((and (level:? value)
                (zero? (level: value)))
            (lighting/cleanup-state node))
          ((and (red:? value)
                (zero? (+ (red: value)
                          (green: value)
                          (blue: value))))
           (lighting/cleanup-state node)))))

(initializers/register (lambda (data-dir)
                         (log-it "  lighting")
                         (mqtt-mux/register "shc/lighting/#" lighting/event-handler)))


;;; --------------------------------------------------------------------------------
;;; PUBLIC API

;; lights on at a specific level

(define (lighting/on-in node level millis)
  (lighting/set-node-level-in node level millis))


(define (lighting/on node level)
  (lighting/on-in node level 0))


;; lights on in a specific colour

(define (lighting/colour-in node red green blue brightness millis)
  (lighting/set-node-rgb-in node red green blue brightness millis))

(define (lighting/colour node red green blue brightness)
  (lighting/colour-in node red green blue brightness 0))


;; lights off

(define (lighting/off-in node millis)
  (log-it "Scheduling node ~A lights off in ~A mS" node millis)
  (lighting/set-node-level-in node 0 millis))


(define (lighting/off node)
  (lighting/off-in node 0))


;;; Lighting event management

(define (lighting/cancel-for node)
  (mqtt-queue/unschedule (lighting/node->topic node)))

