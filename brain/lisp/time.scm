;;; Copyright 2016 Dave Astels. All rights reserved.
;;; Use of this source code is governed by a BSD-style
;;; license that can be found in the LICENSE file.

;;; This file contains time related utility functions and constants

(define seconds-per-day 86400)
(define seconds-per-hour 3600)
(define seconds-per-minute 60)
(define millis-per-day (* seconds-per-day 1000))
(define millis-per-hour (* seconds-per-hour 1000))
(define millis-per-minute (* seconds-per-minute 1000))

(define (hours->millis h)
  (* h millis-per-hour))


(define (minutes->millis m)
  (* m 60000))


(define (seconds->millis s)
  (* s 1000))


(define (hours->seconds h)
  (* h seconds-per-hour))


(define (hms->seconds time)
  (let ((h (car time))
        (m (cadr time))
        (s (caddr time)))
    (+ (* h seconds-per-hour) (* m seconds-per-minute) s)))


(define (seconds->hms seconds)
  (let* ((h (integer (/ seconds seconds-per-hour)))
         (m (integer (/ (- seconds (* h seconds-per-hour)) 60)))
         (s (- seconds (+ (* h seconds-per-hour) (* m seconds-per-minute)))))
    (list h m s)))


(define (start-of-day day)
  (let ((data (read:> persistent-space {type: 'start-of-day day: day})))
    (if (nil? data)
      25200
      (time: (car data)))))


(define (end-of-day day)
  (let ((data (read:> persistent-space {type: 'end-of-day day: day})))
    (if (nil? data)
      84600
      (time: (car data)))))


(define (tomorrow)
  (date-in-days 1))


(define (after? t1 t2)
  (> t1 t2))


(define (before? t1 t2)
  (< t1 t2))


(define (date->string d)
  (string-join (map str d) "/"))


(define (time->string t)
  (let* ((hour (first t))
         (pm? (> hour 12))
         (h-m (list (if pm? (- hour 12) hour)
                    (second t))))
    (format #f "~A ~A"
            (string-join (map (lambda (n) (string-pad-left (number->string n) 2 #\0)) h-m) ":")
            (if pm? "pm" "am"))))


(define (time->24hr-string t)
  (string-join (map (lambda (n)
                      (string-pad-left (number->string n) 2 #\0))
                    (list-head t 2))
               ":"))


(define (time->24hr-string-with-seconds t)
  (string-join (map (lambda (n)
                      (string-pad-left (number->string n) 2 #\0))
                    t)
               ":"))


(define (dark-out?)
  (let ((now (seconds)))
    (or (> now (time: (car (read:> state-space {type: 'sunset}))))
        (< now (time: (car (read:> state-space {type: 'sunrise})))))))


;;; This does not work when end of day is >= midnight

(define (usually-awake-at? t)
  (let ((day (day-of-week)))
    (and (after? t (start-of-day day))
         (before? t (end-of-day day)))))


