;;; Copyright 2016 Dave Astels. All rights reserved.
;;; Use of this source code is governed by a BSD-style
;;; license that can be found in the LICENSE file.

;;; This file handles motion logic


(define motion-polly/recent-motions '())


(define (motion-polly/add-motion new-motion-event)
  (set! motion-polly/recent-motions (list new-motion-event (car motion-polly/recent-motions))))


;; was the current event the first one "today"
;; we take that to mean the first since the usual wake-up time

;; argument is  the current and previous (can be an empty frame) motion event that have happened so far today

(define (first-motion-today? current-event previous-event)
  (let* ((current-event-time (adatime->seconds (created-at: current-event)))
         (previous-event-time (adatime->seconds (created-at: previous-event))))
    (and (> current-event-time (start-of-day (day-of-week)))
         (or (nequal? (adatime->date (created-at: previous-event)) (date-today))
             (and (< previous-event-time (start-of-day (day-of-week)))
                  (> (- current-event-time previous-event-time) (hours->seconds 4)))))))

(define (polly/motionstart-handler client message)
  (let* ((now (time-now))
         (now-seconds (seconds))
         (today (date-today))
		 (node (string->number (fourth (string-split (topic: message) "/"))))
         (current-event {created-at: (date-time->adatime today now)})
         (previous-event (if (nil? motion-polly/recent-motions)
                           {created-at: "2000-01-01T00:00:00.000Z"}
                           (car motion-polly/recent-motions))))
    (motion-polly/add-motion current-event)
    (cond ((first-motion-today? current-event previous-event) ;if this is the first motion detected today give a more involved greeting
           (cond ((> now-seconds (hours->seconds 17)) ; after 5pm
                  (polly/say "Good evening!" {tag: "good_evening" wait: #t}))
                 ((> now-seconds (hours->seconds 12)) ; after 12 noon
                  (polly/say "Good afternoon!" {tag: "good_afternoon" wait: #t}))
                 (else                  ;before noon
                  (polly/say "Good morning!" {tag: "good_morning" wait: #t})))
           (polly/timestamp {wait: #t})
           (polly/weather))
          ((> (- now-seconds (adatime->seconds (created-at: previous-event))) (hours->seconds 4)) ; say hi if it's been a while
           (polly/say "Hello!" {tag: "hello"}))
          )))
                           


(define (polly/motionend-handler client message)
  (polly/say (format #f "Motion ended at node ~A" (string->number (fourth (string-split (topic: message) "/"))))))

(initializers/register (lambda (data-dir)
                         (log-it "  motion based voice output")
                         (mqtt-mux/register "shc/events/data/+/motionstart" polly/motionstart-handler)
                         ;;(mqtt-mux/register "shc/events/data/+/motionend" polly/motionend-handler)
                         ))
