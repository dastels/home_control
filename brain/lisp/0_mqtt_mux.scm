;;; Copyright 2016 Dave Astels. All rights reserved.
;;; Use of this source code is governed by a BSD-style
;;; license that can be found in the LICENSE file.

;;; This file provides an MQTT subscription multiplexer
;;; MUST BE LOADED FIRST

(define mqtt-mux/handlers '())

(define (mqtt-mux/reset)
  (set! mqtt-mux/handlers '()))


(define (mqtt-mux/match-topic topic pattern)
  (let loop ((topic-parts (string-split topic "/"))
             (pattern-parts (string-split pattern "/")))
    (cond ((or (nil? topic-parts) (nil? pattern-parts))
           (and (nil? topic-parts) (nil? pattern-parts)))
          ((equal? (car pattern-parts) "#")
           #t)
          ((equal? (car pattern-parts) "+")
           (loop (cdr topic-parts) (cdr pattern-parts)))
          ((nequal? (car topic-parts) (car pattern-parts))spec
           #f)
          (else
           (loop (cdr topic-parts) (cdr pattern-parts))))))


(define (mqtt-mux/assoc-topic topic l)
  (cond ((nil? l)
         #f)
        ((mqtt-mux/match-topic topic (caar l))
         (car l))
        (else
         (mqtt-mux/assoc-topic topic (cdr l)))))


(define (mqtt-mux/dispatch client message)
;  (log-it "mqtt-dispatch ~A with ~A~%" (topic: message) (payload: message))
  (let ((handlers (mqtt-mux/assoc-topic (topic: message) mqtt-mux/handlers)))
    (unless (false? handlers)
      (for-each (lambda (handler)
                  ;; (format #t "Handling ~A with ~A~%" (topic: message)handler)
                  (handler client message))
                (cdr handlers)))))


(define (mqtt-mux/register topic-pattern handler)
  (let ((existing-handlers (assoc topic-pattern mqtt-mux/handlers)))
    (cond ((false? existing-handlers)
           (set! mqtt-mux/handlers (acons topic-pattern (list handler) mqtt-mux/handlers)))
          (else
           (set-cdr! existing-handlers (cons handler (cdr existing-handlers)))))
    (mqtt/subscribe topic-pattern 0 mqtt-mux/dispatch)))


(define (mqtt-mux/deregister topic-pattern handler)
  (let ((existing-handlers (assoc topic-pattern mqtt-mux/handlers)))
    (unless (false? existing-handlers)
      (let ((new-handlers (remove (lambda (x) (equal? x handler)) (cdr existing-handlers))))
        (if (nil? new-handlers)
          (set! mqtt-mux/handlers (dissoc topic-pattern mqtt-mux/handlers))
          (set-cdr! existing-handlers new-handlers))))))

