;;; Copyright 2016 Dave Astels. All rights reserved.
;;; Use of this source code is governed by a BSD-style
;;; license that can be found in the LICENSE file.

;;; This file handles fan control

(define (node->fan-topic node)
  (format #f "shc/fan/~A" node))

(define (fan-off-in node minutes)
  (mqtt-queue/send-in (minutes->millis minutes) (node->fan-topic node) 0 (lisp->json #f)))

(define (fan-control/fan-handler client message)
  (let ((node (node-number-from (message->command-info message)))
        (value (json->lisp (payload: message))))
    (node/fan-on-off node value)))

(define (fan-control/motion-started-handler client message)
  (let ((node (node-number-from (message->sensor-info message))))
    (when (node/has-fan? node)
      (log-it "Motion started on sensor ~A, turning on fan" node)
      (node/fan-on-off node #t))))

(define (fan-control/motion-ended-handler client message)
  (let ((node (node-number-from (message->sensor-info message))))
    (when (node/has-fan? node)
      (fan-off-in node 5))))

(define node-3-humidity '())

(define (fan-control/humidity-handler client message)
  (let ((node (node-number-from (message->sensor-info message)))
        (humidity (integer (string->number (payload: message)))))
    (when (and (node/has-fan? node)
               (> humidity 4000))    ; arbitrarily pic 40% RH as the threshold
      (log-it "Node ~A humidity is high, fan should be/remain on" node)
      (node/fan-on-off node #t)
      (mqtt-queue/unschedule (node->fan-topic node))
      (fan-off-in node 5))))


(initializers/register (lambda (data-dir)
                         (log-it "  fan control")
                         (mqtt-mux/register "shc/fan/+" fan-control/fan-handler)
                         (mqtt-mux/register "shc/events/data/+/humidity" fan-control/humidity-handler)
                         (mqtt-mux/register "shc/events/data/+/motionstart" fan-control/motion-started-handler)
                         (mqtt-mux/register "shc/events/data/+/motionend" fan-control/motion-ended-handler)))
