;;; Copyright 2016 Dave Astels. All rights reserved.
;;; Use of this source code is governed by a BSD-style
;;; license that can be found in the LICENSE file.

;;; This file handles using Polly for various tasks

(define polly/voice polly/russell)

;;; Say something, optionally caching it

(define (polly/say text . maybe-opts)
  (let* ((opts (if (or (nil? maybe-opts)
                       (nil? (car maybe-opts)))
                   {}
                   (car maybe-opts)))
         (tag (get-slot-or-nil opts tag:))
         (wait (get-slot-or-nil opts wait:)))
    (polly/speak polly/voice
               text
               (if (nil? tag) "" tag)
               (if (nil? wait) #f wait))))


(define (polly/weather . opts)
  (polly/say (describe-weather (current-weather-conditions)) (car opts)))


(define (polly/today . opts)
  (polly/say (date->string (date-today)) (car opts)))


(define (polly/now . opts)
  (polly/say (time->string (time-now)) (car opts)))


(define (polly/timestamp . opts)
  (polly/say (format #f "It is ~A on ~A." (time->string (time-now)) (date->string (date-today))) (car opts)))


(define (polly/temperature-report . opts)
  (let* ((readings (read:> state-space {type: "temperature"}))
		 (text (string-join (map (lambda (reading)
								   (let ((temp (/ (value: reading) 100.0))
										 (room (node/node->group (node: reading))))
									 (format #f "~A is at ~A" room temp)))
								 readings)
							". ")))
	(polly/say text (car opts))))

(define (polly/movement-report . opts)
  (let* ((rooms (map (lambda (f)
					   (node/node->group (node: f)))
					 (read:> state-space {type: "motion" value: #t}))))
	(polly/say (string-append "Movement is currently detected in: "
							  (string-join rooms ", "))
			   (car opts))))
