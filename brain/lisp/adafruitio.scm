;;; Copyright 2016 Dave Astels. All rights reserved.
;;; Use of this source code is governed by a BSD-style
;;; license that can be found in the LICENSE file.

;;; This file provides some helpful functions for supporting the AdaFruit IO service

;; Convert a date (year month day) to an adafruit io search timestamp
(define (date-time->adatime date time)
  (format #f "~A-~A-~AT~A:~A:~A.000Z" (first date) (second date) (third date) (first time) (second time) (third time)))

;; Convert an adafruit io timestamp to the number of seconds since midnith this morning
(define (adatime->seconds adatime)
  (let* ((time-string (cadr (string-split adatime "T")))
         (time-parts (map string->number (string-split (car (string-split time-string ".")) ":"))))
    (+ (* (first time-parts) 3600)
       (* (second time-parts) 60)
       (third time-parts))))

;; Convert an adafruit io timestamp to a date (year month day)
(define (adatime->date adatime)
  (let* ((date-string (car (string-split adatime "T"))))
    (map string->number (string-split date-string "-"))))
