;;; Copyright 2017 Dave Astels. All rights reserved.
;;; Use of this source code is governed by a BSD-style
;;; license that can be found in the LICENSE file.

;;; This file handles proximity detector processing


;;; Send a command to the host to set the proximity measurment interval on the specified node
;;; 0 menas stop measuring proximity
(define (proximity/interval node interval)
  (mqtt-queue/send-now (format #f "shc/sensors/command/~A/proximity" node)
                       0
                       (format #f "~A" interval)))


;;; If motion is detected on a node have it start watching proximity and cancel any schedule command to turn of the proximity lighting
(define (proximity/on-handler client message)
  (let ((node (node-number-from (message->sensor-info message))))
    (when (node/has-proximity? node)
      (mqtt-queue/unschedule (node->lights-off-topic node))
      (proximity/interval node 300))))


;;; If motion is no longer detected on a node have it stop watching proximity
(define (proximity/off-handler client message)
  (let ((node (node-number-from (message->sensor-info message))))
    (when (node/has-proximity? node)
	  (proximity/interval node 0))))


(define (proximity/light-off node)
  (when (node/has-neopixels? node)
    (node/set-neopixels node 0 0 0)
	(proximity/interval node 0)
    (take:> state-space {type: "prox-light-on" node: node})))


;;; Turn off the spot lighting because the timeout expired
(define (proximity/lights-off-handler client message)
  (let ((node (string->number (third (string-split (topic: message) "/")))))
    (proximity/light-off node)))


(define (node->lights-off-topic node)
  (format #f "shc/events/~A/proximity-lights-off" node))


;;; check if something is in proximity
(define (proximity/close-enough? proximity-value)
  (<= proximity-value 350))


(define (is-moving-at? node)
  (let ((entries (read:> state-space {type: "motion" node: node})))
    (and (notnil? entries)
         (value: (car entries)))))


;;; Handle a proximity measurement
;;;  - turn on the spot lighting if in proximity
;;;  - schedule turning spot lighting off and stopping proximity monitoring
(define (proximity/handler client message)
  (let* ((node (node-number-from (message->sensor-info message)))
         (distance (string->number (payload: message)))
         (close (proximity/close-enough? distance))
         (lights-off-topic (node->lights-off-topic node))
         (light-on-template {type: "prox-light-on" node: node})
         (light-on? (notnil? (read:> state-space light-on-template))))
    ;; (when (< distance 8000) (log-it "Distance from node ~A is ~Amm" node distance))
    (cond ((and (is-moving-at? node)
                close)
           (unless light-on?
             (log-it "Turning on node ~A neopixels" node)
             (node/set-neopixels node 255 255 255)
             (write:> state-space light-on-template))
		   (proximity/interval node 2000)
           (mqtt-queue/unschedule lights-off-topic))
          ((and (not close)
                light_on?
                (not (mqtt-queue/scheduled? lights-off-topic)))
           (log-it "Scheduling neopixel turn-off for node ~A" node)
           (mqtt-queue/send-in 180000 lights-off-topic 0 "0")))))



;;; --------------------------------------------------------------------------------
;;; Initialization

(initializers/register (lambda (data-dir)
                         (log-it "  proximity handling")
                         (mqtt-mux/register "shc/events/data/+/motionstart" proximity/on-handler)
                         (mqtt-mux/register "shc/events/data/+/motionend" proximity/off-handler)
                         (mqtt-mux/register "shc/events/+/proximity-lights-off" proximity/lights-off-handler)
                         (mqtt-mux/register "shc/events/data/+/proximity" proximity/handler)))

