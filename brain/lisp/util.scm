;;; Copyright 2016 Dave Astels. All rights reserved.
;;; Use of this source code is governed by a BSD-style
;;; license that can be found in the LICENSE file.

;;; This file provides some utilities

;;; These definitions depend on this file being loaded toward the end of the startup sequence, since
;;; it depends on blackbopard.scm and space.scm

;;; --------------------------------------------------------------------------------
;;; Spaces

(define state-space (new:> Space "state"))

(define persistent-space (new:> Space "persistent"))

(define (initialize-persiatent-space data-dir)
  (let ((space-file (format #f "~A/persistent_space.json" data-dir)))
    (log-it "Loading persistent space")
    (restore-from:> persistent-space space-file)
    (log-it "Starting space persistance")
    (ticker 10700 (lambda () (persist-to:> persistent-space space-file)))))


(initializers/register (lambda (data-dir)
                         (initialize-persiatent-space data-dir)))

;;; --------------------------------------------------------------------------------
;;; General utilities


(define (byte? n)
  (and (>= n 0)
       (< n 256)))


(define (bit-number? n)
  (and (>= n 0)
       (< n 8)))
