;;; Copyright 2016 Dave Astels. All rights reserved.
;;; Use of this source code is governed by a BSD-style
;;; license that can be found in the LICENSE file.
;;;
;;; This file starts up brain
;;; ----------------------------------------------------------------------------


(define (power-on)
  (exec "mplayer" "--volume=100" (string-join (list (get-env "GOPATH") "power-on.mp3") "/")))


;; The main function that starts the system

(define (main program-args)
  (let ((data-dir (car program-args)))
	(cond ((string? data-dir)
           (log-it "System starting up")
           (power-on)
           (get-weather-data)           ; to preload sunset & sunrise times
           (initializers/run data-dir)
           (sleep 5000)
           (polly/say "Hello. System is starting up." {tag: "startup" wait: #t}))
          (else
           (log-it "String arg expected for data-directory, but was given ~S~%Exiting." data-dir)))))
