;;; Copyright 2016 Dave Astels. All rights reserved.
;;; Use of this source code is governed by a BSD-style
;;; license that can be found in the LICENSE file.

;;; This file handles fetching and voicing a weather conditions

(define darksky-key (get-env "DARKSKY_KEY"))

(define darksky-url (format #f "https://api.darksky.net/forecast/~A/42.9837,-81.2497?units=ca&exclude=minutely,hourly&language=en" darksky-key))


(define (get-weather-data)
  (on-error
   (let* ((weather-data (json->lisp (second (net/get darksky-url))))
		  (todays-data (car (data: (daily: weather-data)))))
	 (take:> state-space {type: 'sunrise})
	 (write:> state-space {type: 'sunrise time: (- (sunriseTime: todays-data) (time: todays-data))})
	 (take:> state-space {type: 'sunset})
	 (write:> state-space {type: 'sunset time: (- (sunsetTime: todays-data) (time: todays-data))})
	 weather-data)
   (lambda (err)
	 (when (nil? (read:> state-space {type: 'sunrise}))
	   (write:> state-space {type: 'sunrise time: (hours->seconds 7)})
	   (write:> state-space {type: 'sunset time: (hours->seconds 20)})))))


(define (current-weather-conditions)
  (currently: (get-weather-data)))


(define (bearing->direction bearing)
  (cond ((or (>= bearing 337) (<= bearing 23))
         'north)
        ((<= bearing 68)
         'northeast)
        ((<= bearing 113)
         'east)
        ((<= bearing 158)
         'southeast)
        ((<= bearing 203)
         'south)
        ((<= bearing 248)
         'southwest)
        ((<= bearing 293)
         'west)
        ((<= bearing 338)
         'northwest)))


(define (naturalize-temperature v)
  (let* ((int-v (integer v))
         (leftover (- v int-v)))
    (cond ((<= leftover 0.15)
           (number->string int-v))
          ((<= leftover 0.35)
           (if (zero? int-v)
             "one quarter degree"
             (format #f "~A and a quarter" int-v)))
          ((<= leftover 0.65)
           (if (zero? int-v)
             "one half degree"
             (format #f "~A and a half" int-v)))
          ((<= leftover 0.85)
           (if (zero? int-v)
             "three quarters of a degree"
             (format #f "~A and three quarters" int-v)))
          (else
           (number->string (1+ int-v))))))


(define (naturalize-speed v)
  (let* ((int-v (integer v))
         (leftover (- v int-v)))
    (cond ((<= leftover 0.15)
           (number->string int-v))
          ((<= leftover 0.35)
           (if (zero? int-v)
             "one quarter"
             (format #f "~A and a quarter" int-v)))
          ((<= leftover 0.65)
           (if (zero? int-v)
             "one half"
             (format #f "~A and a half" int-v)))
          ((<= leftover 0.85)
           (if (zero? int-v)
             "three quarters"
             (format #f "~A and three quarters" int-v)))
          (else
           (number->string (1+ int-v))))))


(define (pluralize-degrees temp)
  (if (eqv? (abs temp) 1.0)
    "degree"
    "degrees"))


(define (describe-weather data)
  ;; extract data
  (let ((summary (get-slot-or-nil data summary:))
        (temperature (get-slot-or-nil data temperature:))
        (apparent-temperature (get-slot-or-nil data apparentTemperature:))
        (humidity (get-slot-or-nil data humidity:))
        (precip-probability (get-slot-or-nil data precipProbability:))
        (precip-type (get-slot-or-nil data precipType:))
        (wind-speed (get-slot-or-nil data windSpeed:))
        (wind-bearing (get-slot-or-nil data windBearing:)))
    ;; build phrases from the extracted data
    (let ((summary-phrase (if (nil? summary)
                            ""
                            (format #f "It is currently ~A." summary)))
          (temperature-phrase (if (nil? temperature)
                                ""
                                (format #f "The temperature is ~A ~A celcius" (naturalize-temperature temperature) (pluralize-degrees temperature))))
          (humidity-phrase (if (nil? humidity)
                             ""
                             (format #f "with a relative humidity of ~A percent" (integer (* 100 humidity)))))
          (apparent-phrase (if (or (nil? apparent-temperature)
                                   (< (abs (- apparent-temperature temperature)) 0.15))
                             "."
                             (format #f "~A it feels like ~A~A."
                                     (if (nil? humidity)
                                       (if (nil? temperature)
                                         ""
                                         " but ")
                                       " and ")
                                     (naturalize-temperature apparent-temperature)
                                     (if (nil? temperature)
                                       (format #f  " ~A celcius" (pluralize-degrees apparent-temperature))
                                       ""))))
          (precip-phrase (if (or (nil? precip-probability)
                                 (eqv? precip-probability 0))
                           ""
                           (format #f "There is a ~A percent chance of ~A." (integer (* 100 precip-probability)) precip-type)))
          (wind-phrase (if (or (nil? wind-speed)
                               (eqv? wind-speed 0))
                         ""
                         (format #f "The wind is out of the ~A at ~A ~A per hour."
                                 (bearing->direction wind-bearing)
                                 (naturalize-speed wind-speed)
                                 (if (> wind-speed 1.15) "kilometers" "kilometer")))))
      (string-join (list summary-phrase temperature-phrase humidity-phrase apparent-phrase precip-phrase wind-phrase) " "))))
