;;; Copyright 2016 Dave Astels. All rights reserved.
;;; Use of this source code is governed by a BSD-style
;;; license that can be found in the LICENSE file.

;;; This file provides logging functionallity

(define **LOGGING** #f)

(define (logging/start)
  (set! **LOGGING** #t))

(define (logging/stop)
  (set! **LOGGING** #f))

(define (log-it format-string . objects)

  (define (pad-2-digit n)
    (string-pad-left (number->string n) 2 #\0))

  (when **LOGGING**
		(let* ((today (date-today))
			   (date-string (format #f "~A/~A/~A" (third today) (second today) (first today)))
			   (now (time-now))
			   (time-string (format #f "~A:~A:~A" (pad-2-digit (first now)) (pad-2-digit (second now)) (pad-2-digit (third now))))
			   (message (apply format (if (nil? objects)
										  (list #f format-string)
										  (cons* #f format-string objects)))))
		  (format #t "~A ~A - ~A~%" date-string time-string message))))
