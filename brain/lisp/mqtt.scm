;;; MQTT support

;; Strips apart a sensor data message topic shc/sensors/data/<node>/<sensor> to extract node & sensor info
(define (message->sensor-info message)
  (let ((topic-parts (string-split (topic: message) "/")))
	(cons (string->number (fourth topic-parts)) (fifth topic-parts))))

;; Strips apart a command (lighting, fan, etc) message topic shc/<command>/<node> to extract node info
(define (message->command-info message)
  (let ((topic-parts (string-split (topic: message) "/")))
	(cons (string->number (third topic-parts)) "")))

(define node-number-from car)

(define sensor-name-from cdr)
