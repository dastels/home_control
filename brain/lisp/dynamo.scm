;;; Copyright 2016 Dave Astels. All rights reserved.
;;; Use of this source code is governed by a BSD-style
;;; license that can be found in the LICENSE file.

;;; This file handles the interface to AWS DynamoDB

(define dynamo/size 60)
(define dynamo/queue (make-vector dynamo/size nil))
(define dynamo/front 0)
(define dynamo/rear 0)


(define (dynamo/equal?)
  (eqv? dynamo/front dynamo/rear))


(define (dynamo/advance-front)
  (set! dynamo/front (remainder (1+ dynamo/front) dynamo/size)))


(define (dynamo/advance-rear)
  (set! dynamo/rear (remainder (1+ dynamo/rear) dynamo/size)))


(typedef dynamo/add frame)
(define (dynamo/add record)
  ;; (log-it "Enqueue: ~A" record)
  (dynamo/advance-rear)
  (when (dynamo/equal?)
    ;; (log-it "Dynamo write queue is full, discarding oldest data. Consider increasing queue size.")
    (dynamo/advance-front))
  (vector-set! dynamo/queue dynamo/rear record)
  ;; (log-it "Queue now: ~A ~A ~A" dynamo/front dynamo/rear dynamo/queue)
  record)


(typedef dynamo/remove -> frame)
(define (dynamo/remove)
  (unless (dynamo/equal?)
    (dynamo/advance-front)
    (let ((item (vector-ref dynamo/queue dynamo/front)))
      ;; (log-it "Dequeue: ~A" item)
      (vector-set! dynamo/queue dynamo/front nil)
      ;; (log-it "Queue now: ~A ~A ~A" dynamo/front dynamo/rear dynamo/queue)
      item)))


(define (dynamo/process)
  (let ((item (dynamo/remove)))
    (when item
      (dynamo/send "AstelsSmartHome" item))))


(define (dynamo/create-table-if-required)
  (let ((tables (dynamo/list-tables)))
    (when (false? (member "AstelsSmartHome" tables))
      (dynamo/create-table "AstelsSmartHome"))))

(define **DYNAMO** #f)
(define dynamo/ticker nil)

(define (dynamo/start)
  (unless **DYNAMO**
	(set! **DYNAMO** #t)
	(set! dynamo/ticker (ticker 1000 dynamo/process))))

(define (dynamo/stop)
  (when **DYNAMO**
	(set! **DYNAMO** #f)
	(stop-ticker dynamo/ticker)
	(set! dynamo/ticker nil)))

(initializers/register (lambda (data-dir)
                         (log-it "  dynamodb interface")
                         (when **DYNAMO**
						   (dynamo/create-table-if-required)
						   (dynamo/start))))
