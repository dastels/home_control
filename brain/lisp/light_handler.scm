;;; Copyright 2016 Dave Astels. All rights reserved.
;;; Use of this source code is governed by a BSD-style
;;; license that can be found in the LICENSE file.

;;; This file handles light detector processing

(define (light/handler client message)
  (let ((node (node-number-from (message->sensor-info message)))
        (light-level (string->number (payload: message))))
    (unless (lighting-control/need-light? light-level)
      (lights-off node))))


;; this was a bad idea.  It could be light enough only becasue the light is on.
;; (initializers/register (lambda (data-dir)
;;                          (mqtt-mux/register "shc/sensors/data/+/light" light/handler)))

                
