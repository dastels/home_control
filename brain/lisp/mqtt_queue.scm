;;; Copyright 2016 Dave Astels. All rights reserved.
;;; Use of this source code is governed by a BSD-style
;;; license that can be found in the LICENSE file.

;;; This file provides immediate an scheduled MQTT queuing.
;;; Queue entries have the form:
;;; {
;;;   time: <the time in milliseconds then the queued message should be sent, from midnight>
;;;   topic: <the topic to punblish to>
;;;   qos: <the qos to use>
;;;   payload: <the message payload>
;;; }

;;; The queue of message to vbe sent later today
(define mqtt-queue/todays-scheduled-messages '())

;;; An a-list of messages for the future, indexed by date
(define mqtt-queue/future-scheduled-messages '())


;;; insert a message into the schedule
(define (mqtt-queue/insert-in-place m l)
  (cond ((nil? l)
         (cons m '()))
        ((<= (time: m) (time: (car l)))
         (cons m l))
        (else
         (cons (car l) (mqtt-queue/insert-in-place m (cdr l))))))


(define (mqtt-queue/insert message)
  (set! mqtt-queue/todays-scheduled-messages
        (mqtt-queue/insert-in-place message mqtt-queue/todays-scheduled-messages)))


;;; process the queue, publishing any messages whose time has come
(define (mqtt-queue/process-queue)
  (cond ((<= (millis) mqtt-queue/mqtt-scheduler-interval)
         (let ((next-day-pair (assoc (date-today) mqtt-queue/future-scheduled-messages)))
           (when next-day-pair 
             (set! mqtt-queue/todays-scheduled-messages
                   (cdr next-day-pair))
             (set! mqtt-queue/future-scheduled-messages
                   (dissoc (date-today) mqtt-queue/future-scheduled-messages)))))
        ((and (not (nil? mqtt-queue/todays-scheduled-messages))
              (<= (time: (car mqtt-queue/todays-scheduled-messages)) (millis)))
         (let ((m (car mqtt-queue/todays-scheduled-messages)))
           (set! mqtt-queue/todays-scheduled-messages
                 (cdr mqtt-queue/todays-scheduled-messages))
           (mqtt/publish (topic: m) (qos: m) (payload: m))
           (mqtt-queue/process-queue)))))

;;; --------------------------------------------------------------------------------
;;; Public functions

;;; publish a message immediately
(define (mqtt-queue/send-now topic qos payload)
  (mqtt/publish topic qos payload))


;;; Send a message at a specific time on a specific date
(define (mqtt-queue/send-at-on hms ymd topic qos payload)
  (if (equal? ymd (date-today))
    (mqtt-queue/send-at hms topic qos payload)
    (let* ((queue-pair-or-f (assoc ymd mqtt-queue/future-scheduled-messages))
           (queue (if queue-pair-or-f (cdr queue-pair-or-f) '()))
           (new-queue (mqtt-queue/insert-in-place {time: (* (hms->seconds hms) 1000) topic: topic qos: qos payload: payload} queue)))
      (set! mqtt-queue/future-scheduled-messages
            (acons ymd new-queue (dissoc ymd mqtt-queue/future-scheduled-messages))))))


;;; Send a message at a particular time today
(define (mqtt-queue/send-at-millis time-in-millis topic qos payload)
  (cond ((< time-in-millis millis-per-day)
         (mqtt-queue/unschedule topic)
         (mqtt-queue/insert {time: time-in-millis topic: topic qos: qos payload: payload}))
        (else
         (mqtt-queue/send-at-on (seconds->hms (integer (/ (- time-in-millis millis-per-day) 1000)))
                                (tomorrow)
                                topic
                                qos
                                payload))))


;;; enqueue a message in delay milliseconds
(define (mqtt-queue/send-in delay topic qos payload)
  (if (zero? delay)
      (mqtt-queue/send-now topic qos payload)
      (mqtt-queue/send-at-millis (+ (millis) delay) topic qos payload)))


;;; Send at a specific time
(define (mqtt-queue/send-at hms topic qos payload)
  (mqtt-queue/send-at-millis (* (hms->seconds time) 1000) topic qos payload))


;;; remove all scheduled messages for a topic
(define (mqtt-queue/unschedule topic)
  (set! mqtt-queue/todays-scheduled-messages
        (remove (lambda (m)
                  (equal? (topic: m) topic))
                mqtt-queue/todays-scheduled-messages))
  (for-each (lambda (day-schedule-pair)
              (set-cdr! day-schedule-pair (remove (lambda (m)
                                                    (equal? (topic: m) topic))
                                                  (cdr day-schedule-pair))))
            mqtt-queue/future-scheduled-messages))


(define (mqtt-queue/scheduled? topic)
  (not (and (false? (memp (lambda (m)
                            (equal? (topic: m) topic))
                          mqtt-queue/todays-scheduled-messages))
            (false? (memp (lambda (m)
                            (equal? (topic: m) topic))
                          (map cdr mqtt-queue/future-scheduled-messages))))))


;;; --------------------------------------------------------------------------------
;;; Initialization

(define mqtt-queue/mqtt-scheduler-interval 250)
(define mqtt-queue/scheduled-message-ticker nil)

;;; start the queue processor
(define (mqtt-queue/start)
  (set! mqtt-queue/scheduled-message-ticker
        (ticker mqtt-queue/mqtt-scheduler-interval
                mqtt-queue/process-queue)))


;;; Stop the queue processor
(define (mqtt-queue/stop)
  (stop-ticker mqtt-queue/scheduled-message-ticker))


(mqtt-queue/start)
