;;; Copyright 2016 Dave Astels. All rights reserved.
;;; Use of this source code is governed by a BSD-style
;;; license that can be found in the LICENSE file.

;;; This file impliments a tuplespace.
;;; It's not overly efficient, so most appropriate for small spaces.

(define Space {

  acquire-lock:
  (lambda ()
    (do ()
        ((atomic-compare-and-swap! lock 0 1))
      (log-it "Waiting for ~A lock" name)
      (sleep 10)))

  release-lock:
  (lambda ()
    (atomic-store! lock 0))
  
  match:
  (lambda (entry template)
    (reduce-left and #t 
                 (map (lambda (key)
                        (and (has-slot? entry key)
                             (equal? (get-slot entry key)
                                     (get-slot template key))))
                      (frame-keys template))))
  
  
  fire-callbacks:
  (lambda (entry)
    (for-each (lambda (triggered-callback)
                (when (match entry (car triggered-callback))
                  ((cdr triggered-callback) entry)))
              callbacks))
  
  ;; Public functions
  
  new:
  (lambda (n)
    {name: n
     soup: '()
     callbacks: '()
     lock: (atomic)
     proto*: Space})
  
  persist-to:
  (lambda (fname)
    (let ((out (open-output-file fname))
          (text (lisp->json soup)))
      (write-string text out)
      (close-port out)))

  restore-from:
  (lambda (fname)
    (acquire-lock)
    (let* ((in (open-input-file fname))
           (json (read-string in)))
      (close-port in)
      (soup:! self (json->lisp json)))
    (release-lock))

  dump:
  (lambda ()
    (string-join (map (lambda (f)
                        (format #f "~A" f))
                      soup)
                 "\n"))
  
  ;; Add frame to the soup. Multiple copies are allowed
  ;; Returns nil
  
  write:
  (lambda (entry)
    (acquire-lock)
    (soup:! self (cons entry soup))
    (release-lock)
    (fire-callbacks entry)
    nil)
  
  
  ;; Find entries that match template
  ;; Return a list of the matching entries

  read:
  (lambda (template)
    (filter (lambda (entry)
              (match entry template)) 
            soup))


  ;; Find entries that match template and remove them from the soup
  ;; Returns nil
  
  take:
  (lambda (template)
    (acquire-lock)
    (soup:! self (remove (lambda (entry)
                           (match entry template))
                         soup))
    (release-lock)
    nil)


  ;; Adds a callback and trigger template. When an entry is added that matches the template, eval the callback
  ;; Returns nil
  
  notify:
  (lambda (template callback)
    (acquire-lock)
    (callbacks:! self (cons (cons template callback)
                            callbacks))
    (release-lock)
    nil)

  unnotify:
  (lambda (template)
    (acquire-lock)
    (callbacks:! self (remove (lambda (c)
                                (equal? (car c) template))
                              callbacks))
    (release-lock))
  
  })
