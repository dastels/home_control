;;; -*- mode: Scheme -*-

(let ((path (format #f "~A/lisp" (get-env "GOPATH"))))
  (load-from path "0_initializers.scm")
  (load-from path "0_logging.scm"))

(context "initializer" ()

		 (it "calls registered initializers"
			 (define a 0)
			 (define b 0)
			 (initializers/register (lambda (d) (set! a 2)))
			 (initializers/register (lambda (d) (set! b 3)))
			 (initializers/run "")
			 (assert-eq a 2)
			 (assert-eq b 3))

		 (it "passes the string arg through"
			 (define s "")
			 (initializers/register (lambda (d) (set! s d)))
			 (initializers/run "hello")
			 (assert-eq s "hello")))
