;;; -*- mode: Scheme -*-

(let ((path (format #f "~A/lisp" (get-env "GOPATH"))))
  (load-from path "space.scm"))

(context "space" 

		 ((define s (new:> Space "test")))

		 (it "initializes to an empty soup"
			 (assert-nil (soup: s)))

		 (it "can write"
			 (write:> s {a: 1})
			 (write:> s {b: 2})
			 (assert-eq (soup: s) (list {b: 2} {a: 1})))

		 (it "doesn't overwrite"
			 (write:> s {a: 1})
			 (write:> s {a: 1})
			 (assert-eq (soup: s) (list {a: 1} {a: 1})))

		 (it "notifies on full match"
			 (define w #f)
			 (notify:> s {a: 1 b: 2} (lambda (e) (set! w (equal? e {a: 1 b: 2}))))
			 (write:> s {a: 1 b: 2})
			 (assert-true w))

		 (it "notifies on partial match"
			 (define w #f)
			 (notify:> s {a: 1} (lambda (e) (set! w (equal? e {a: 1 b: 2}))))
			 (write:> s {a: 1 b: 2})
			 (assert-true w))

		 (it "can unregister notifications"
			 (define w #f)
			 (notify:> s {a: 1 b: 2} (lambda (e) (set! w (equal? e {a: 1 b: 2}))))
			 (unnotify:> s {a: 1 b: 2})
			 (write:> s {a: 1 b: 2})
			 (assert-false w))

		 (it "can take"
			 (write:> s {a: 1 b: 1})
			 (write:> s {a: 2 b: 1})
			 (write:> s {a: 1 b: 2})
			 (assert-eq (soup: s) (list {a: 1 b: 2}  {a: 2 b: 1} {a: 1 b: 1}))
			 (take:> s {a: 1})
			 (assert-eq (soup: s) (list {a: 2 b: 1})))
		 
		 (it "can read a fully matching template"
			 (write:> s {a: 1 b: 1})
			 (write:> s {a: 1 b: 2})
			 (write:> s {a: 2 b: 3})
			 (assert-eq (read:> s {a: 1 b: 2}) (list {a: 1 b: 2})))

		 (it "can read a partial matching template"
			 (write:> s {a: 1 b: 1})
			 (write:> s {a: 1 b: 2})
			 (write:> s {a: 2 b: 3})
			 (assert-eq (read:> s {a: 1}) (list {a: 1 b: 2} {a: 1 b: 1})))

		 )

