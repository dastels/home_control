;;; -*- mode: Scheme -*-

(let ((path (format #f "~A/lisp" (get-env "GOPATH"))))
  (load-from path "0_mqtt_mux.scm"))

(context "MQTT mux" ((mqtt-mux/reset))

		 (it "matches an exact pattern"
			 (assert-true (mqtt-mux/match-topic "shs/sensor/1" "shs/sensor/1")))
		 
		 (it "matches single wildcards"
			 (assert-true (mqtt-mux/match-topic "shs/sensor/1" "shs/+/1")))
		 
		 (it "matches rest-of-topic wildcard"
			 (assert-true (mqtt-mux/match-topic "shs/sensor/1" "shs/#")))
		 
		 (it "doesn't match a mismatch"
			 (assert-false (mqtt-mux/match-topic "shs/sensor/1" "shs/sensor/2"))
			 (assert-false (mqtt-mux/match-topic "shs/sensor/1" "shs/+"))
			 (assert-false (mqtt-mux/match-topic "shs/sensor/1" "shs/sensor/1/#")))
		 
		 (it "can register a handler for a specific pattern"
			 (define (f client message) 42)
			 (mqtt-mux/register "topic/1/stuff" f)
			 (assert-eq (length mqtt-mux/handlers) 1)
			 (assert-eq (caar mqtt-mux/handlers) "topic/1/stuff")
			 (assert-eq (cadar mqtt-mux/handlers) f))

		 (it "can register multiple handlers for a pattern"
			 (define (f client message) 42)
			 (define (g client message) 84)
			 (mqtt-mux/register "topic/1/stuff" f)
			 (mqtt-mux/register "topic/1/stuff" g)
			 (assert-eq (length mqtt-mux/handlers) 1)
			 (assert-eq (caar mqtt-mux/handlers) "topic/1/stuff")
			 (assert-eq (caddar mqtt-mux/handlers) f)
			 (assert-eq (cadar mqtt-mux/handlers) g))

		 (it "can register handlers for multiple patterns"
			 (define (f client message) 42)
			 (define (g client message) 84)
			 (mqtt-mux/register "topic/1/stuff" f)
			 (mqtt-mux/register "topic/2/stuff" g)
			 (assert-eq (length mqtt-mux/handlers) 2)
			 (assert-eq (caadr mqtt-mux/handlers) "topic/1/stuff")
			 (assert-eq (cadadr mqtt-mux/handlers) f)
			 (assert-eq (caar mqtt-mux/handlers) "topic/2/stuff")
			 (assert-eq (cadar mqtt-mux/handlers) g))

		 (it "can find registered handlers in a simple situation"
		 	 (define (f client message) 42)
		 	 (mqtt-mux/register "topic/1/stuff" f)
		 	 (assert-eq (cdr (mqtt-mux/assoc-topic "topic/1/stuff" mqtt-mux/handlers)) (list f)))

		 (it "can find registered handler in a multi-pattern situation"
		 	 (define (f client message) 42)
			 (define (g client message) 84)
		 	 (mqtt-mux/register "topic/1/stuff" f)
			 (mqtt-mux/register "topic/2/stuff" g)
		 	 (assert-memq (cdr (mqtt-mux/assoc-topic "topic/1/stuff" mqtt-mux/handlers)) f)
		 	 (assert-memq (cdr (mqtt-mux/assoc-topic "topic/2/stuff" mqtt-mux/handlers)) g))

		 (it "can find registered handler in a wildcard situation"
		 	 (define (f client message) 42)
			 (define (g client message) 84)
		 	 (mqtt-mux/register "topic/+/stuff" f)
			 (mqtt-mux/register "topic/+/stuff" g)
		 	 (assert-memq (cdr (mqtt-mux/assoc-topic "topic/1/stuff" mqtt-mux/handlers)) f)
		 	 (assert-memq (cdr (mqtt-mux/assoc-topic "topic/1/stuff" mqtt-mux/handlers)) g)
		 	 (assert-memq (cdr (mqtt-mux/assoc-topic "topic/2/stuff" mqtt-mux/handlers)) f)
		 	 (assert-memq (cdr (mqtt-mux/assoc-topic "topic/2/stuff" mqtt-mux/handlers)) g))

		 (it "can dispatch"
			 (define v nil)
			 (define (f client message)
			   (set! v (payload: message)))
			 (mqtt-mux/register "topic/+/stuff" f)			 
			 (mqtt-mux/dispatch nil {topic: "topic/1/stuff" payload: "42"})
			 (assert-eq v "42"))

		 (it "can deregister"
			 (define v nil)
			 (define (f client message)
			   (set! v (payload: message)))
			 (mqtt-mux/register "topic/+/stuff" f)			 
			 (mqtt-mux/deregister "topic/+/stuff" f)			 
			 (mqtt-mux/dispatch nil {topic: "topic/1/stuff" payload: "42"})
			 (assert-eq v nil))


		 )
