;;; -*- mode: Scheme -*-

(let ((path (format #f "~A/lisp" (get-env "GOPATH"))))
  (load-from path "node.scm")
  (load-from path "space.scm")
  (load-from path "util.scm"))))

(context "A node"
		 ((nodes:! node/config (make-vector 5 {})) )

		 (it "can record features"
			 (node/record-features 1 #xAA)
			 (assert-true (frame? (vector-ref (nodes: node/config) 1)))
			 (assert-eq (features: (vector-ref (nodes: node/config) 1)) #xAA))

		 (it "can check feature bits"
			 (node/record-features 1 #xAA)
			 (assert-false (node/check-feature-bit 1 #x01))
			 (assert-true (node/check-feature-bit 1 #x02))
			 (assert-false (node/check-feature-bit 1 #x04))
			 (assert-true (node/check-feature-bit 1 #x08))
			 (assert-false (node/check-feature-bit 1 #x10))
			 (assert-true (node/check-feature-bit 1 #x20))
			 (assert-false (node/check-feature-bit 1 #x40))
			 (assert-true (node/check-feature-bit 1 #x80)))


		 (it "can check for motion"
			 (node/record-features 1 node/motion-mask)
			 (assert-true (node/has-motion? 1)))

		 (it "can check for light"
			 (node/record-features 1 node/light-mask)
			 (assert-true (node/has-light? 1)))

		 (it "can check for proximity"
			 (node/record-features 1 node/prox-mask)
			 (assert-true (node/has-proximity? 1)))

		 (it "can check for temperature"
			 (node/record-features 1 node/temp-rh-mask)
			 (assert-true (node/has-temperature? 1)))

		 (it "can check for neopixels"
			 (node/record-features 1 node/neopixel-mask)
			 (assert-true (node/has-neopixels? 1)))

		 (it "can check current motion state when there is motion noted"
			 (write:> state-space {node: 1 type: "motion" value: #t})
			 (assert-true (node/motion-detected? 1)))

		 (it "can check current motion state when there isn't motion noted"
			 (write:> state-space {node: 1 type: "motion" value: #f})
			 (assert-false (node/motion-detected? 1)))

		 )
