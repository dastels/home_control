// Copyright 2016 Dave Astels.  All rights reserved.

package main

import (
	. "bitbucket.org/dastels/brain"
	. "bitbucket.org/dastels/golisp"
	"flag"
	"fmt"
	"os"
	"path/filepath"
	"runtime/pprof"
	"strings"
)

var (
	runTests     bool   = false
	verboseTests bool   = false
	runRepl      bool   = false
	definition   string = ""
	codeToEval   string = ""
	cpuprofile   string = ""
)

func test() {
	verboseFlag := ""
	testFunction := ""
	if verboseTests {
		verboseFlag = " #t"
	}
	testName := flag.Arg(0)

	if strings.HasSuffix(testName, ".scm") {
		testFunction = "run-test"
	} else {
		testFunction = "run-all-tests"
	}

	testCommand := fmt.Sprintf("(%s \"%s\"%s)", testFunction, testName, verboseFlag)
	_, err := ProcessFile(os.ExpandEnv("$GOLISPHOME/tools/testing.scm"))
	if err != nil {
		fmt.Printf("Error: %s\n", err)
	}
	_, err = ParseAndEval(testCommand)
	if err != nil {
		fmt.Printf("Error: %s\n", err)
	}
}

func loadFile(path string) error {
	absPath, err := filepath.Abs(path)
	if err != nil {
		fmt.Printf(" - Error: %s\n", err)
		return err
	}
	if !(filepath.Base(absPath) == ".") && !(filepath.Base(absPath) == "..") {
		extension := filepath.Ext(path)
		if extension == ".scm" {
			fmt.Printf("  Loading %s", path)
			_, err = ProcessFile(absPath)
			if err != nil {
				fmt.Printf(" - Error: %s\n", err)
				return err
			} else {
				fmt.Printf("\n")
			}
		}
	}
	return nil
}

func walkFunc(path string, info os.FileInfo, err error) error {
	return loadFile(path)
}

func main() {
	var walkErr error
	SetupMqttClient()
	SetupAws()

	flag.BoolVar(&runRepl, "r", false, "Whether to run the repl after loading golisp code.  Defaults to false.")
	flag.BoolVar(&runTests, "t", false, "Whether to run tests and exit.  Defaults to false.")
	flag.BoolVar(&verboseTests, "v", false, "Whether tests should be verbose.  Defaults to false.")
	flag.StringVar(&definition, "d", "", "symbol=value, where value is a lisp value. Adds this binding to the global environment before loading any code.")
	flag.StringVar(&codeToEval, "e", "", "Code that should be evaluated after all files are loaded. This will be done instead of using a main, if any. It will also be done before entering the REPL.")
	flag.StringVar(&cpuprofile, "cpuprofile", "", "Enable profiling and specify the file to write profile data to")
	flag.Parse()

	if cpuprofile != "" {
		f, err := os.Create(cpuprofile)
		if err != nil {
			fmt.Println("Error creating profilling file: %s\n", err)
			return
		}
		pprof.StartCPUProfile(f)
		defer pprof.StopCPUProfile()
	}

	if definition != "" {
		symbolAndValue := strings.Split(definition, "=")
		sym := Global.Intern(symbolAndValue[0])
		val, err := ParseAndEval(symbolAndValue[1])
		if err != nil {
			fmt.Println("Error with cmd line definition value")
			return
		}
		Global.BindTo(sym, val)
	}

	walkErr = filepath.Walk(os.ExpandEnv("$GOLISPHOME/lisp"), walkFunc)
	if walkErr != nil {
		fmt.Printf("Error loading code\n")
		return
	}

	if runTests {
		test()
	} else {
		var programArgs []string
		for i := 0; i < flag.NArg(); i = i + 1 {
			fmt.Printf("Arg %d: %s\n", i, flag.Arg(i))
			if flag.Arg(i) == "-" {
				programArgs = flag.Args()[i+1:]
				println("===> found program args")
				break
			} else {
				name, err := filepath.Abs(flag.Arg(i))
				if err != nil {
					fmt.Printf(" - Error: %s\n", err)
					return
				}
				f, err := os.Open(name)
				if err != nil {
					fmt.Println(err)
					return
				}
				defer f.Close()
				fi, err := f.Stat()
				if err != nil {
					fmt.Println(err)
					return
				}
				switch mode := fi.Mode(); {
				case mode.IsDir():
					fmt.Printf("Loading dir %s\n", flag.Arg(i))
					walkErr = filepath.Walk(flag.Arg(i), walkFunc)
					if walkErr != nil {
						fmt.Printf("Error loading code\n")
						return
					}
				case mode.IsRegular():
					loadErr := loadFile(flag.Arg(i))
					if loadErr != nil {
						return
					}
				}
			}
		}

		if codeToEval != "" {
			result, err := ParseAndEval(codeToEval)
			if err != nil {
				fmt.Printf("Error: %s\n", err)
			} else {
				fmt.Printf("==> %s\n", String(result))
			}
		}

		// If a function named "main" has been loaded, call it with any program args supplied after "--"
		mainValue := Global.ValueOf(Intern("main"))

		// if the repl was asked for OR there is no main function, drop into the repl
		if FunctionP(mainValue) {
			println("Calling main")
			args := make([]*Data, 0, len(programArgs))
			for _, arg := range programArgs {
				args = append(args, StringWithValue(arg))
			}
			argList := Cons(InternalMakeList(Intern("quote"), ArrayToList(args)), nil)
			result, err := FunctionValue(mainValue).Apply(argList, Global)

			// handle any result/error from the main function
			if err != nil {
				fmt.Printf("Error: %s\n", err)
			} else {
				fmt.Printf("==> %s\n", String(result))
			}
		}
		if runRepl {
			Repl()
		}
	}
}
