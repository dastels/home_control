// Copyright 2016 Dave Astels. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// This file provides a GoLisp interface to AWS Polly

package brain

import (
	. "bitbucket.org/dastels/golisp"
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/polly"
	"io"
	"os"
	"os/exec"
)

func init() {
	InitPollyConstants()
	InitPollyPrimitives()
}

func InitPollyConstants() {
	Global.BindTo(Intern("polly/female"), StringWithValue(polly.GenderFemale))
	Global.BindTo(Intern("polly/male"), StringWithValue(polly.GenderMale))

	Global.BindTo(Intern("polly/cy-GB"), StringWithValue(polly.LanguageCodeCyGb))
	Global.BindTo(Intern("polly/da-DK"), StringWithValue(polly.LanguageCodeDaDk))
	Global.BindTo(Intern("polly/de-DE"), StringWithValue(polly.LanguageCodeDeDe))
	Global.BindTo(Intern("polly/en-AU"), StringWithValue(polly.LanguageCodeEnAu))
	Global.BindTo(Intern("polly/en-GB"), StringWithValue(polly.LanguageCodeEnGb))
	Global.BindTo(Intern("polly/en-GB-WLS"), StringWithValue(polly.LanguageCodeEnGbWls))
	Global.BindTo(Intern("polly/en-IN"), StringWithValue(polly.LanguageCodeEnIn))
	Global.BindTo(Intern("polly/en-US"), StringWithValue(polly.LanguageCodeEnUs))
	Global.BindTo(Intern("polly/es-ES"), StringWithValue(polly.LanguageCodeEsEs))
	Global.BindTo(Intern("polly/es-US"), StringWithValue(polly.LanguageCodeEsUs))
	Global.BindTo(Intern("polly/fr-CA"), StringWithValue(polly.LanguageCodeFrCa))
	Global.BindTo(Intern("polly/fr-FR"), StringWithValue(polly.LanguageCodeFrFr))
	Global.BindTo(Intern("polly/is-IS"), StringWithValue(polly.LanguageCodeIsIs))
	Global.BindTo(Intern("polly/it-IT"), StringWithValue(polly.LanguageCodeItIt))
	Global.BindTo(Intern("polly/js-JP"), StringWithValue(polly.LanguageCodeJaJp))
	Global.BindTo(Intern("polly/nb-NO"), StringWithValue(polly.LanguageCodeNbNo))
	Global.BindTo(Intern("polly/nl-NL"), StringWithValue(polly.LanguageCodeNlNl))
	Global.BindTo(Intern("polly/pl-PL"), StringWithValue(polly.LanguageCodePlPl))
	Global.BindTo(Intern("polly/pt-BR"), StringWithValue(polly.LanguageCodePtBr))
	Global.BindTo(Intern("polly/pt-PT"), StringWithValue(polly.LanguageCodePtPt))
	Global.BindTo(Intern("polly/ro-RO"), StringWithValue(polly.LanguageCodeRoRo))
	Global.BindTo(Intern("polly/ru-RU"), StringWithValue(polly.LanguageCodeRuRu))
	Global.BindTo(Intern("polly/sv-SE"), StringWithValue(polly.LanguageCodeSvSe))
	Global.BindTo(Intern("polly/tr-TR"), StringWithValue(polly.LanguageCodeTrTr))

	Global.BindTo(Intern("polly/mp3"), StringWithValue(polly.OutputFormatMp3))
	Global.BindTo(Intern("polly/ogg-vorbis"), StringWithValue(polly.OutputFormatOggVorbis))
	Global.BindTo(Intern("polly/pcm"), StringWithValue(polly.OutputFormatPcm))

	Global.BindTo(Intern("polly/ssml"), StringWithValue(polly.TextTypeSsml))
	Global.BindTo(Intern("polly/text"), StringWithValue(polly.TextTypeText))

	Global.BindTo(Intern("polly/geraint"), StringWithValue(polly.VoiceIdGeraint))
	Global.BindTo(Intern("polly/gwyneth"), StringWithValue(polly.VoiceIdGwyneth))
	Global.BindTo(Intern("polly/mads"), StringWithValue(polly.VoiceIdMads))
	Global.BindTo(Intern("polly/naja"), StringWithValue(polly.VoiceIdNaja))
	Global.BindTo(Intern("polly/hans"), StringWithValue(polly.VoiceIdHans))
	Global.BindTo(Intern("polly/marlene"), StringWithValue(polly.VoiceIdMarlene))
	Global.BindTo(Intern("polly/nicole"), StringWithValue(polly.VoiceIdNicole))
	Global.BindTo(Intern("polly/russell"), StringWithValue(polly.VoiceIdRussell))
	Global.BindTo(Intern("polly/amy"), StringWithValue(polly.VoiceIdAmy))
	Global.BindTo(Intern("polly/brian"), StringWithValue(polly.VoiceIdBrian))
	Global.BindTo(Intern("polly/emma"), StringWithValue(polly.VoiceIdEmma))
	Global.BindTo(Intern("polly/raveena"), StringWithValue(polly.VoiceIdRaveena))
	Global.BindTo(Intern("polly/ivy"), StringWithValue(polly.VoiceIdIvy))
	Global.BindTo(Intern("polly/joanna"), StringWithValue(polly.VoiceIdJoanna))
	Global.BindTo(Intern("polly/joey"), StringWithValue(polly.VoiceIdJoey))
	Global.BindTo(Intern("polly/justin"), StringWithValue(polly.VoiceIdJustin))
	Global.BindTo(Intern("polly/kendra"), StringWithValue(polly.VoiceIdKendra))
	Global.BindTo(Intern("polly/limberly"), StringWithValue(polly.VoiceIdKimberly))
	Global.BindTo(Intern("polly/salli"), StringWithValue(polly.VoiceIdSalli))
	Global.BindTo(Intern("polly/conchita"), StringWithValue(polly.VoiceIdConchita))
	Global.BindTo(Intern("polly/enrique"), StringWithValue(polly.VoiceIdEnrique))
	Global.BindTo(Intern("polly/miguel"), StringWithValue(polly.VoiceIdMiguel))
	Global.BindTo(Intern("polly/penelope"), StringWithValue(polly.VoiceIdPenelope))
	Global.BindTo(Intern("polly/chantal"), StringWithValue(polly.VoiceIdChantal))
	Global.BindTo(Intern("polly/celine"), StringWithValue(polly.VoiceIdCeline))
	Global.BindTo(Intern("polly/mathieu"), StringWithValue(polly.VoiceIdMathieu))
	Global.BindTo(Intern("polly/dora"), StringWithValue(polly.VoiceIdDora))
	Global.BindTo(Intern("polly/karl"), StringWithValue(polly.VoiceIdKarl))
	Global.BindTo(Intern("polly/carla"), StringWithValue(polly.VoiceIdCarla))
	Global.BindTo(Intern("polly/giorgio"), StringWithValue(polly.VoiceIdGiorgio))
	Global.BindTo(Intern("polly/mizuki"), StringWithValue(polly.VoiceIdMizuki))
	Global.BindTo(Intern("polly/liv"), StringWithValue(polly.VoiceIdLiv))
	Global.BindTo(Intern("polly/lotte"), StringWithValue(polly.VoiceIdLotte))
	Global.BindTo(Intern("polly/ruben"), StringWithValue(polly.VoiceIdRuben))
	Global.BindTo(Intern("polly/ewa"), StringWithValue(polly.VoiceIdEwa))
	Global.BindTo(Intern("polly/jacek"), StringWithValue(polly.VoiceIdJacek))
	Global.BindTo(Intern("polly/jan"), StringWithValue(polly.VoiceIdJan))
	Global.BindTo(Intern("polly/maja"), StringWithValue(polly.VoiceIdMaja))
	Global.BindTo(Intern("polly/ricardo"), StringWithValue(polly.VoiceIdRicardo))
	Global.BindTo(Intern("polly/vitotia"), StringWithValue(polly.VoiceIdVitoria))
	Global.BindTo(Intern("polly/cristiano"), StringWithValue(polly.VoiceIdCristiano))
	Global.BindTo(Intern("polly/ines"), StringWithValue(polly.VoiceIdInes))
	Global.BindTo(Intern("polly/carmen"), StringWithValue(polly.VoiceIdCarmen))
	Global.BindTo(Intern("polly/maxim"), StringWithValue(polly.VoiceIdMaxim))
	Global.BindTo(Intern("polly/tatyana"), StringWithValue(polly.VoiceIdTatyana))
	Global.BindTo(Intern("polly/astrid"), StringWithValue(polly.VoiceIdAstrid))
	Global.BindTo(Intern("polly/feliz"), StringWithValue(polly.VoiceIdFiliz))

}

func SetupAws() {
	// mySession, err := session.NewSession()
	// if err != nil {
	// 	panic(err)
	// }

	// pollyService := polly.New(mySession)
}

func InitPollyPrimitives() {
	MakeTypedPrimitiveFunction("polly/speak", "(2,4)", sayImpl, []uint32{StringType, StringType, StringType, BooleanType})
}

func saveStream(fname string, audioStream io.ReadCloser) (err error) {
	if err != nil {
		return
	}
	buf := make([]byte, 1024)
	var f *os.File
	f, err = os.Create(fname)
	defer f.Close()
	numberOfBytesRead := 0
	for {
		numberOfBytesRead, err = audioStream.Read(buf)
		if err == io.EOF {
			err = nil
			break
		} else if err != nil {
			return
		}

		_, err = f.Write(buf[:numberOfBytesRead])
		if err != nil {
			return
		}
	}
	return
}

// (polly/say voice-id text [cacheid [force]])
func sayImpl(args *Data, env *SymbolTableFrame) (result *Data, err error) {
	cachable := false
	fname := ""
	force := true
	wait := false
	cacheId := ""
	if Length(args) > 2 {
		if StringValue(Third(args)) != "" {
			cacheId = StringValue(Third(args))
			fname = os.ExpandEnv(fmt.Sprintf("$GOPATH/polly_cache/%s.mp3", cacheId))
			cachable = true
			force = false
			if _, err := os.Stat(fname); err != nil {
				if os.IsNotExist(err) {
					force = true
				}
			}
		}
		if Length(args) > 3 {
			wait = NotNilP(Fourth(args)) && BooleanValue(Fourth(args))
		}
	}
	if !cachable {
		fname = os.ExpandEnv(fmt.Sprintf("$GOPATH/polly_cache/disposable.mp3"))
	}

	var sess *session.Session
	var resp *polly.SynthesizeSpeechOutput
	if !cachable || force {
		sess, err = session.NewSession(&aws.Config{Region: aws.String("us-east-1")})
		if err != nil {
			return
		}

		svc := polly.New(sess)

		params := &polly.SynthesizeSpeechInput{
			OutputFormat: aws.String(polly.OutputFormatMp3),
			Text:         aws.String(StringValue(Second(args))),
			VoiceId:      aws.String(StringValue(First(args))),
			TextType:     aws.String(polly.TextTypeText),
		}
		resp, err = svc.SynthesizeSpeech(params)
		if err != nil {
			return
		}

		err = saveStream(fname, resp.AudioStream)
		if err != nil {
			return
		}
	}

	cmd := exec.Command("mplayer", "--volume=100", fname)
	if wait {
		cmd.Run()
	} else {
		cmd.Start()
	}
	return
}
