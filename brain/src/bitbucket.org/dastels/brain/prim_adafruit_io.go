// Copyright 2016 Dave Astels. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// This package impliments primitives to interface with AdaFruit IO.

package brain

import (
	. "bitbucket.org/dastels/golisp"
	"github.com/adafruit/io-client-go"
	"os"
)

var client *adafruitio.Client

func init() {
	InitAdafruitIoPrimitives()
}

func InitAdafruitIoPrimitives() {
	MakeTypedPrimitiveFunction("adafruitio/send", "2", sendImpl, []uint32{StringType, AnyType})
	MakeTypedPrimitiveFunction("adafruitio/search", "3", searchImpl, []uint32{StringType, StringType, StringType})
	MakeTypedPrimitiveFunction("adafruitio/last", "1", lastImpl, []uint32{StringType})
	MakeTypedPrimitiveFunction("adafruitio/prev", "1", previousImpl, []uint32{StringType})
	MakeTypedPrimitiveFunction("adafruitio/next", "1", nextImpl, []uint32{StringType})
}

func getFeed(feedname string, env *SymbolTableFrame) (feed *adafruitio.Feed, err error) {
	feed, _, err = client.Feed.Get(feedname)
	if err != nil {
		err = ProcessError(err.Error(), env)
	}
	return
}

func setupClient(feedname string, env *SymbolTableFrame) (err error) {
	if client == nil {
		client = adafruitio.NewClient(os.Getenv("ADAFRUIT_IO_KEY"))
	}
	feed, err := getFeed(feedname, env)
	if err != nil {
		err = ProcessError(err.Error(), env)
		return
	}
	client.SetFeed(feed)
	return
}

func convertDataToFrame(d *adafruitio.Data) *Data {
	dataFrame := make(FrameMap)
	dataFrame["id:"] = IntegerWithValue(int64(d.ID))
	dataFrame["value:"] = StringWithValue(d.Value)
	dataFrame["position:"] = StringWithValue(d.Position)
	dataFrame["feed-id:"] = IntegerWithValue(int64(d.FeedID))
	dataFrame["group-id:"] = IntegerWithValue(int64(d.GroupID))
	dataFrame["expiration:"] = StringWithValue(d.Expiration)
	dataFrame["latitude:"] = FloatWithValue(d.Latitude)
	dataFrame["longitude:"] = FloatWithValue(d.Longitude)
	dataFrame["elevation:"] = FloatWithValue(d.Elevation)
	dataFrame["completed-at:"] = StringWithValue(d.CompletedAt)
	dataFrame["created-at:"] = StringWithValue(d.CreatedAt)
	dataFrame["updated-at:"] = StringWithValue(d.UpdatedAt)
	dataFrame["created-epoch:"] = FloatWithValue(d.CreatedEpoch)
	return FrameWithValue(&dataFrame)
}

// (adafruitio/send feedname-or-key value-string)

func sendImpl(args *Data, env *SymbolTableFrame) (result *Data, err error) {
	setupClient(StringValue(First(args)), env)
	dataObject := &adafruitio.Data{Value: String(Second(args))}

	d, _, err := client.Data.Send(dataObject)
	if err != nil {
		err = ProcessError(err.Error(), env)
		return
	}
	result = convertDataToFrame(d)
	return
}

// (adafruitio/search feedname-or-key start-time end-time) -> list of data frames

func searchImpl(args *Data, env *SymbolTableFrame) (result *Data, err error) {
	setupClient(StringValue(First(args)), env)
	start := StringValue(Second(args))
	end := StringValue(Third(args))
	filter := adafruitio.DataFilter{StartTime: start, EndTime: end}
	data, _, err := client.Data.Search(&filter)
	if err != nil {
		err = ProcessError(err.Error(), env)
		return
	}
	dataFrames := make([]*Data, 0, len(data))
	for _, d := range data {
		dataFrames = append(dataFrames, convertDataToFrame(d))
	}
	result = ArrayToList(dataFrames)
	return
}

// (adafruitio/last feedname-or-key -> frame)

func lastImpl(args *Data, env *SymbolTableFrame) (result *Data, err error) {
	setupClient(StringValue(First(args)), env)

	d, _, err := client.Data.Last()
	if err != nil {
		err = ProcessError(err.Error(), env)
		return
	}
	result = convertDataToFrame(d)
	return
}

// (adafruitio/previous feedname-or-key -> frame)

func previousImpl(args *Data, env *SymbolTableFrame) (result *Data, err error) {
	setupClient(StringValue(First(args)), env)

	d, _, err := client.Data.Prev()
	if err != nil {
		err = ProcessError(err.Error(), env)
		return
	}
	result = convertDataToFrame(d)
	return
}

// (adafruitio/next feedname-or-key -> frame)

func nextImpl(args *Data, env *SymbolTableFrame) (result *Data, err error) {
	setupClient(StringValue(First(args)), env)

	d, _, err := client.Data.Next()
	if err != nil {
		err = ProcessError(err.Error(), env)
		return
	}
	result = convertDataToFrame(d)
	return
}
