// Copyright 2016 Dave Astels. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// This file provides a GoLisp interface to AWS DynamoDB

package brain

import (
	. "bitbucket.org/dastels/golisp"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

func init() {
	InitDynamoPrimitives()
}

func InitDynamoPrimitives() {
	MakePrimitiveFunction("dynamo/list-tables", "0", listTablesImpl)
	MakeTypedPrimitiveFunction("dynamo/create-table", "1", createTableImpl, []uint32{StringType})
	MakeTypedPrimitiveFunction("dynamo/send", "2", sendItemImpl, []uint32{StringType, FrameType})
}

func dbclient() *dynamodb.DynamoDB {
	awsConfig := &aws.Config{
		Region:   aws.String("us-east-1"),
		Endpoint: aws.String("http://10.215.222.140:8000"),
	}
	return dynamodb.New(session.New(awsConfig))
}

// (dynamo/list-tables)
func listTablesImpl(args *Data, env *SymbolTableFrame) (result *Data, err error) {
	db := dbclient()
	params := &dynamodb.ListTablesInput{}
	resp, err := db.ListTables(params)
	if err != nil {
		return nil, ProcessError(err.Error(), env)
	}

	tablenames := make([]*Data, 0, len(resp.TableNames))
	for _, tablename := range resp.TableNames {
		tablenames = append(tablenames, StringWithValue(*tablename))
	}
	result = ArrayToList(tablenames)
	return
}

// (dynamo/create-table table-name)
func createTableImpl(args *Data, env *SymbolTableFrame) (result *Data, err error) {
	tablename := StringValue(First(args))
	db := dbclient()

	params := &dynamodb.CreateTableInput{
		AttributeDefinitions: []*dynamodb.AttributeDefinition{
			{
				AttributeName: aws.String("timestamp"),
				AttributeType: aws.String("S"),
			},
			{
				AttributeName: aws.String("sort-key"),
				AttributeType: aws.String("S"),
			},
		},
		KeySchema: []*dynamodb.KeySchemaElement{
			{
				AttributeName: aws.String("timestamp"),
				KeyType:       aws.String("HASH"),
			},
			{
				AttributeName: aws.String("sort-key"),
				KeyType:       aws.String("RANGE"),
			},
		},
		ProvisionedThroughput: &dynamodb.ProvisionedThroughput{
			ReadCapacityUnits:  aws.Int64(1),
			WriteCapacityUnits: aws.Int64(1),
		},
		TableName: aws.String(tablename),
	}
	_, err = db.CreateTable(params)

	if err != nil {
		return nil, ProcessError(err.Error(), env)
	}
	return
}

// (dynamo/add table-name {key: value ... })
func sendItemImpl(args *Data, env *SymbolTableFrame) (result *Data, err error) {
	table := StringValue(First(args))
	item := FrameValue(Second(args))

	db := dbclient()
	value := item.Get("value:")
	if BooleanP(value) {
		if BooleanValue(value) {
			value = IntegerWithValue(1)
		} else {
			value = IntegerWithValue(0)
		}
	}

	params := &dynamodb.PutItemInput{
		TableName: aws.String(table),
		Item: map[string]*dynamodb.AttributeValue{
			"timestamp": {S: aws.String(String(item.Get("timestamp:")))},
			"sort-key":  {S: aws.String(String(item.Get("sort-key:")))},
			"node":      {N: aws.String(String(item.Get("node:")))},
			"type":      {S: aws.String(String(item.Get("type:")))},
			"value":     {N: aws.String(String(value))},
		},
	}

	_, err = db.PutItem(params)

	if err != nil {
		return nil, ProcessError(err.Error(), env)
	}

	return
}
