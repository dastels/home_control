// Copyright 2016 Dave Astels. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// This package impliments primitives to interface with MQTT.

package brain

import (
	. "bitbucket.org/dastels/golisp"
	"fmt"
	MQTT "github.com/eclipse/paho.mqtt.golang"
	"os"
	"strconv"
	"time"
	"unsafe"
)

var MqttClient MQTT.Client

func init() {
	InitMqttPrimitives()
}

func InitMqttPrimitives() {
	MakeTypedPrimitiveFunction("mqtt/publish", "3", publishImpl, []uint32{StringType, IntegerType, StringType})
	MakeTypedPrimitiveFunction("mqtt/subscribe", "3", subscribeImpl, []uint32{StringType, IntegerType, FunctionType})
	MakeTypedPrimitiveFunction("mqtt/unsubscribe", ">=1", unsubscribeImpl, []uint32{StringType})
}

func SetupMqttClient() {
	hostname, _ := os.Hostname()
	server := "tcp://127.0.0.1:1883"
	clientId := hostname + strconv.Itoa(time.Now().Second())
	options := MQTT.NewClientOptions().AddBroker(server).SetClientID(clientId).SetCleanSession(true)
	MqttClient = MQTT.NewClient(options)
	if token := MqttClient.Connect(); token.Wait() && token.Error() != nil {
		panic(token.Error())
	}
}

// Publish to a client
//  (mqtt:publish topic-name qos payload)
//  client is a previosuly connected client object
//  topic-name is a string naming a specific topic
//  qos is an integer (0, 1, or 2) defineing the quality of service. Usually 0.
//  payload is the string to publish to the topic

func publishImpl(args *Data, env *SymbolTableFrame) (result *Data, err error) {
	topic := First(args)
	qos := Second(args)
	payload := Third(args)
	if token := MqttClient.Publish(StringValue(topic), byte(IntegerValue(qos)), false, StringValue(payload)); token.Wait() && token.Error() != nil {
		err = ProcessError(token.Error().Error(), env)
	}
	return
}

func convertMessage(message MQTT.Message) *Data {
	messageFrameMap := make(FrameMap)
	messageFrameMap["duplicate:"] = BooleanWithValue(message.Duplicate())
	messageFrameMap["qos:"] = IntegerWithValue(int64(message.Qos()))
	messageFrameMap["retained:"] = BooleanWithValue(message.Retained())
	messageFrameMap["topic:"] = StringWithValue(message.Topic())
	messageFrameMap["id:"] = IntegerWithValue(int64(message.MessageID()))
	messageFrameMap["payload:"] = StringWithValue(string(message.Payload()[:]))
	return FrameWithValue(&messageFrameMap)
}

func makeHandler(topic string, f *Data, env *SymbolTableFrame) MQTT.MessageHandler {
	handler := func(client MQTT.Client, message MQTT.Message) {
		c := ObjectWithTypeAndValue("MQTTClient", unsafe.Pointer(&client))
		m := convertMessage(message)
		_, err := FunctionValue(f).ApplyWithoutEval(InternalMakeList(c, m), env)
		if err != nil {
			fmt.Printf("Error in %s subscription handler: %s\n", topic, err.Error())
		}
	}

	return handler
}

func subscribeImpl(args *Data, env *SymbolTableFrame) (result *Data, err error) {
	topic := First(args)
	qos := Second(args)
	handler := Third(args)
	if token := MqttClient.Subscribe(StringValue(topic), byte(IntegerValue(qos)), makeHandler(StringValue(topic), handler, env)); token.Wait() && token.Error() != nil {
		err = ProcessError(token.Error().Error(), env)
	}
	return
}

// Unsubscribe from one or more topics
// (mqtt:unsubscribe topic...)
//  client is a previously connected client object
//  topic is the fully instantialed name of a topic. Several can be specified

func unsubscribeImpl(args *Data, env *SymbolTableFrame) (result *Data, err error) {
	topicNames := make([]string, 0, Length(args)-1)
	for cell := args; NotNilP(cell); cell = Cdr(cell) {
		topicNames = append(topicNames, StringValue(Car(cell)))
	}

	if token := MqttClient.Unsubscribe(topicNames...); token.Wait() && token.Error() != nil {
		err = ProcessError(token.Error().Error(), env)
	}
	return
}
