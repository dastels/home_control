/* -*- mode: C++ -*-
 * Command queue
 * Buffers commands until the mesh can consume them.
 * If the queue fills, old commands (oldest first) start begin discarded in order to retain most recent.
 *
 * Dave Astels
 */

#ifndef __COMMAND_QUEUE__
#define __COMMAND_QUEUE__

#define COMMAND_SIZE 16

bool is_command_available();
void add_command(uint8_t *data);
void get_and_remove_command(uint8_t *data);
int command_queue_fullness();

#endif
