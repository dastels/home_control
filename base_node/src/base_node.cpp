
/* -*- mode: C++;-*-
 * Base node 
 * Dave Astels
 */

//#define DEBUG 1

#include <Arduino.h>
#include "base_node.h"
#include <RF24.h>
#include <RF24Network.h>
#include <RF24Mesh.h>
#include <Wire.h>
#include "sensor_data_queue.h"
#include "command_queue.h"
#include "i2c.h"
#include "indicator.h"
#include "tft.h"

RF24 radio(49, 53);
RF24Network network(radio);
RF24Mesh mesh(radio, network);

uint8_t command_buffer[COMMAND_SIZE];
sensor_data_t sensor_data;
uint32_t heartbeat_time = 0;
uint32_t heartbeat_pulse = 0;

uint32_t display_time = 0;
int last_cmd_fullness = 0;
int last_sens_fullness = 0;
int cmd_highwater = 0;
int sen_highwater = 0;

uint16_t node_addresses[16];


uint8_t packet_type_for(uint16_t cmd_tag)
{
    switch (cmd_tag) {
    case LED_COMMAND:
      return (uint8_t)'I';
    case NEOPIXEL_COMMAND:
      return (uint8_t)'L';
    case PROXIMITY_COMMAND:
      return (uint8_t)'P';
    case FEATURE_COMMAND:
      return (uint8_t)'F';
    case EXT_CONFIG_COMMAND:
      return (uint8_t)'C';
    case EXT_WRITE_COMMAND:
      return (uint8_t)'W';
    case EXT_READ_COMMAND:
      return (uint8_t)'R';
    default:
      return 0;
    }
}


void i2c_request_handler()
{
  get_and_remove_sensor_data(&sensor_data);
  Wire.write((uint8_t*)(&sensor_data), sizeof(sensor_data_t));
}


// ISR code.. so have to buffer this as well for sending to mesh in main loop
void i2c_receive_handler(int byte_count)
{
  int index = 0;
  while (Wire.available()) {
    command_buffer[index++] = Wire.read();
  }
  add_command(command_buffer);
}


void send_to_node(uint8_t *buffer)
{
  i2c_command_preamble_t *preamble = (i2c_command_preamble_t*)buffer;
  uint8_t packet_type = packet_type_for(preamble->cmd_tag);
  if (packet_type == 0) {
    return;
  }

  uint16_t address = node_addresses[preamble->node];
  if (address == 0) {
    LOG(F("Couldn't find address for node"));
  } else {
    RF24NetworkHeader header(address, OCT);
    header.type = packet_type;
    int x = network.write(header, buffer, COMMAND_SIZE);
#ifdef DEBUG
    LOG(x == 1 ? F("Send OK") : F("Send failed"));
#endif
  }
}


void handle_heartbeat(uint32_t now)
{
  if (now >= heartbeat_time) {
	heartbeat_time = now + HEARTBEAT;
	heartbeat_pulse = now + HEARTBEAT_PULSE;
	red(true);
  } else if (now >= heartbeat_pulse) {
	heartbeat_pulse = 0xFFFFFFFF;
	red(false);
  }
}


void update_display(uint32_t now)
{
  if (now >= display_time) {
	display_time = now + DISPLAY_INTERVAL;
	int f = command_queue_fullness();
	if (f > cmd_highwater) {
	  cmd_highwater = f;
	}
	if (f != last_cmd_fullness) {
	  last_cmd_fullness = f;
	  rect(0, f, cmd_highwater, BLUE);
	}
	f = sensor_queue_fullness();
	if (f > sen_highwater) {
	  sen_highwater = f;
	}
	if (f != last_sens_fullness) {
	  last_sens_fullness = f;
	  rect(1, f, sen_highwater, MAGENTA);
	}
  }
}


void mesh_heartbeat()
{
  mesh.update();
  mesh.DHCP();
}


void handle_command_to_nodes()
{
  if (is_command_available()) {
    noInterrupts();
    get_and_remove_command(command_buffer);
    interrupts();
    send_to_node(command_buffer);
  }
}


void handle_data_from_nodes()
{
  if (network.available()) {
    RF24NetworkHeader header;
    network.peek(header);
    
    switch(header.type) {
    case 'S':
    case 'R':
      sensor_data_t data;
      network.read(header, (uint8_t*)&data, sizeof(sensor_data_t));
      node_addresses[data.node] = header.from_node;
      noInterrupts();
      add_sensor_data(&data);
      interrupts();
      break;
    default:
      network.read(header, 0, 0);
      break;
    }
  }
}


void setup()
{
  randomSeed(analogRead(0));
  initialize_indicators();
  
#ifdef DEBUG
  Serial.begin(9600);
#endif
  LOG(F("Starting up"));

  init_tft();
  
  Wire.begin(8);
  Wire.onRequest(i2c_request_handler);
  Wire.onReceive(i2c_receive_handler);
  
  mesh.setNodeID(0);
#ifdef DEBUG
  Serial.println(mesh.getNodeID());
  Serial.flush();
#endif
  mesh.begin(MESH_DEFAULT_CHANNEL, RF24_250KBPS);
  heartbeat_time = millis() + HEARTBEAT;
}


void loop()
{
  uint32_t now = millis();

  handle_heartbeat(now);
  update_display(now);
  mesh_heartbeat();
  handle_command_to_nodes();
  handle_data_from_nodes();
  delay(10);
}
  
