/* -*- mode: C++ -*-
 * Sensor data queue
 * Buffers sensor data readings until host system can consume them.
 * If the queue fills, old readings (oldest first) start begin discarded in order to retain most recent.
 *
 * Dave Astels
 */

#ifndef __SENSOR_DATA_QUEUE__
#define __SENSOR_DATA_QUEUE__

#include "base_node.h"
#include "rf24_defs.h"

void add_sensor_data(sensor_data_t *data);
void get_and_remove_sensor_data(sensor_data_t *data);
int sensor_queue_fullness();

#endif
