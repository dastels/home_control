/* -*- mode: C++ -*-
 * Base node 
 * Dave Astels
 */

#ifndef __INDICATOR__
#define __INDICATOR__

void initialize_indicators(void);
void red(bool);

#endif
