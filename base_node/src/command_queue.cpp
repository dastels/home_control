/* -*- mode: C++;-*-
 *
 * Command queue
 * Buffers commands until mesh can consume them.
 * If the queue fills, old commands (oldest first) start begin discarded in order to retain most recent.
 *
 * Dave Astels
 */

#include <Arduino.h>
#include "command_queue.h"
#include "indicator.h"

#define COMMAND_QUEUE_SIZE 32

uint8_t command_queue[COMMAND_QUEUE_SIZE * COMMAND_SIZE];
int cfront = 0;
int crear = 0;


bool is_command_available()
{
  return cfront != crear;
}


void add_command(uint8_t *data)
{
  crear = (crear + 1) % COMMAND_QUEUE_SIZE;
  if (cfront == crear) {          /* is full? */
    cfront = (cfront + 1) % COMMAND_QUEUE_SIZE; /* advance cfront and loose the oldest reading */
  }
  memcpy(&command_queue[crear * COMMAND_SIZE], data, COMMAND_SIZE);
}


void get_and_remove_command(uint8_t *data)
{
  if (cfront == crear) {          /* is empty? */
    memset(data, 0, COMMAND_SIZE);
  } else {
    cfront = (cfront + 1) % COMMAND_QUEUE_SIZE;
    memcpy(data, &command_queue[cfront * COMMAND_SIZE], COMMAND_SIZE);
  }
}


int command_queue_fullness()
{
  return (((COMMAND_QUEUE_SIZE - cfront + crear) % COMMAND_QUEUE_SIZE) * 100) / COMMAND_QUEUE_SIZE;
}
