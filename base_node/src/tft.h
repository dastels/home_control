/* -*- mode: C++ -*-
 * Base node 
 * Dave Astels
 */

#ifndef __TFT__
#define __TFT__

// Assign human-readable names to some common 16-bit color values:
#define	BLACK   0x0000
#define	BLUE    0x001F
#define	RED     0xF800
#define	GREEN   0x07E0
#define CYAN    0x07FF
#define MAGENTA 0xF81F
#define YELLOW  0xFFE0
#define WHITE   0xFFFF

void init_tft();
void rect(int which, int percent_filled, int high_water, uint16_t color);

#endif
