/* -*- mode: C++ -*-
 * Base node 
 * Dave Astels
 */

#ifndef __BASE_NODE__
#define __BASE_NODE__


#ifdef DEBUG
#define LOG(x) { Serial.println(F("")); Serial.println(x); Serial.flush(); }
#else
#define LOG(x)
#endif

#define HEARTBEAT 1000
#define HEARTBEAT_PULSE 50
#define DISPLAY_INTERVAL 900

#endif
