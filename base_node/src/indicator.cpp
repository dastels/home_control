/* -*- mode: C++;-*-
 * Base node 
 * Dave Astels
 */

#include <Arduino.h>
#include "indicator.h"

#define RED_PIN 23

void red(bool on_off)
{
  digitalWrite(RED_PIN, on_off ? HIGH : LOW);
}


void initialize_indicators(void)
{
  pinMode(RED_PIN, OUTPUT);
  red(true);
}

