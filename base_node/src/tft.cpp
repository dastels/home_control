/* -*- mode: C++;-*-
 * Base node TFT display functions
 * Dave Astels
 */

#include <Elegoo_GFX.h>    // Core graphics library
#include <Elegoo_TFTLCD.h> // Hardware-specific library
#include "tft.h"

// The control pins for the LCD can be assigned to any digital or
// analog pins...but we'll use the analog pins as this allows us to
// double up the pins with the touch screen (see the TFT paint example).
#define LCD_CS A3 // Chip Select goes to Analog 3
#define LCD_CD A2 // Command/Data goes to Analog 2
#define LCD_WR A1 // LCD Write goes to Analog 1
#define LCD_RD A0 // LCD Read goes to Analog 0

#define LCD_RESET A4 // Can alternately just connect to Arduino's reset pin

// When using the BREAKOUT BOARD only, use these 8 data lines to the LCD:
// For the Arduino Uno, Duemilanove, Diecimila, etc.:
//   D0 connects to digital pin 8  (Notice these are
//   D1 connects to digital pin 9   NOT in order!)
//   D2 connects to digital pin 2
//   D3 connects to digital pin 3
//   D4 connects to digital pin 4
//   D5 connects to digital pin 5
//   D6 connects to digital pin 6
//   D7 connects to digital pin 7
// For the Arduino Mega, use digital pins 22 through 29
// (on the 2-row header at the end of the board).


Elegoo_TFTLCD tft(LCD_CS, LCD_CD, LCD_WR, LCD_RD, LCD_RESET);

void init_tft()
{
  tft.reset();
  tft.begin(0x9341);
  tft.fillScreen(BLACK);

  tft.setRotation(1);
  tft.setTextSize(2);
  tft.setTextColor(WHITE);
  tft.setCursor(0, 218);
  tft.println("cmd");
  tft.setCursor(0, 188);
  tft.println("sen");

  tft.drawRect(50,  240-32, 270, 32, WHITE);
  tft.drawRect(50,  240-64, 270, 32,  WHITE);
}


void rect(int which, int percent_filled, int high_water, uint16_t color) {
  int left = 51;
  int top = (240-32) - (which * 31);
  int filled = (percent_filled * 268) / 100;
  int high = (high_water * 268) / 100;
  tft.fillRect(left, top, filled, 30, color);
  tft.fillRect(left + filled, top, 268 - filled, 30, BLACK);
  tft.drawLine(left + high, top, left + high, top + 29, RED);
}


