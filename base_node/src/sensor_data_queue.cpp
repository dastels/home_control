/* -*- mode: C++;-*-
 *
 * Sensor data queue
 * Buffers sensor data readings until host system can consume them.
 * If the queue fills, old readings (oldest first) start begin discarded in order to retain most recent.
 *
 * Dave Astels
 */

#include <Arduino.h>
#include "base_node.h"
#include "sensor_data_queue.h"
#include "indicator.h"
#define SENSOR_QUEUE_SIZE 128

sensor_data_t sensor_queue[SENSOR_QUEUE_SIZE];
int sfront = 0;
int srear = 0;

void add_sensor_data(sensor_data_t *data)
{
  srear = (srear + 1) % SENSOR_QUEUE_SIZE;
  if (sfront == srear) {          /* is full? */
    sfront = (sfront + 1) % SENSOR_QUEUE_SIZE; /* advance front and loose the oldest reading */
  }
  memcpy(&sensor_queue[srear], data, sizeof(sensor_data_t));
}


void get_and_remove_sensor_data(sensor_data_t *data)
{
  if (sfront == srear) {          /* is empty? */
    memset(data, 0, sizeof(sensor_data_t));
  } else {
    sfront = (sfront + 1) % SENSOR_QUEUE_SIZE;
    memcpy(data, &sensor_queue[sfront], sizeof(sensor_data_t));
  }
}


int sensor_queue_fullness()
{
  return (((SENSOR_QUEUE_SIZE - sfront + srear) % SENSOR_QUEUE_SIZE) * 100) / SENSOR_QUEUE_SIZE;
}
