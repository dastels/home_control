package main

import (
	"flag"
	// "github.com/gorilla/handlers"
	// "github.com/gorilla/mux"
	"fmt"
	"log"
	"net/http"
	"strconv"
)

var StaticFolder string

func main() {
	InitMqtt()

	var port int
	flag.IntVar(&port, "port", 8400, "port to serve on")
	flag.StringVar(&StaticFolder, "folder", "", "folder containing the static files")
	flag.Parse()

	router := NewRouter(StaticFolder)

	fmt.Printf("Serving on %d\n", port)
	http.Handle("/", router)
	err := http.ListenAndServe(":"+strconv.Itoa(port), nil)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}
