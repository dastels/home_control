Sensor mesh simulator.

This provides a web based interface that allows you to set values of the various sensors on the mesh nodes, as well as trigger update, motion start, and motion end events. Sensor values are persistant and events publish appropriately to the system's sensor queues.

go get github.com/mattn/go-sqlite3

go get github.com/gorilla/mux

go get github.com/kr/fs

go get github.com/eclipse/paho.mqtt.golang

go get golang.org/x/net/websocket


