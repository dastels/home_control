package main

import (
	"fmt"
	"github.com/gorilla/mux"
	"github.com/kr/fs"
	"net/http"
	"os"
	"strings"
)

func NewRouter(static_folder string) *mux.Router {
	router := mux.NewRouter().StrictSlash(true)
	for _, route := range routes {
		var handler http.Handler

		handler = route.HandlerFunc
		handler = Logger(handler, route.Name)

		router.
			Methods(route.Method).
			Path(route.Pattern).
			Name(route.Name).
			Handler(handler)
	}

	walker := fs.Walk(static_folder)
	for walker.Step() {
		if err := walker.Err(); err != nil {
			fmt.Fprintf(os.Stderr, "Error: %s\n", err)
			continue
		}
		www := walker.Path()
		if info, err := os.Stat(www); err == nil && !info.IsDir() {
			pathname := strings.Replace(www, static_folder, "", -1)
			fmt.Printf("Setting handler for %s\n", pathname)
			router.HandleFunc(pathname, func(w http.ResponseWriter, r *http.Request) {
				fmt.Printf("Serving %s\n", www)
				http.ServeFile(w, r, www)
			})
		}
	}

	return router
}
