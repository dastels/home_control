package main

import (
	"fmt"
	MQTT "github.com/eclipse/paho.mqtt.golang"
	"github.com/gorilla/mux"
	"html/template"
	"log"
	"net/http"
	"os"
	"path"
	"strconv"
)

var resultChannel = make(chan string, 1)

func InitDb(w http.ResponseWriter, r *http.Request) {
	InitializeDatabase()
	fmt.Fprintf(w, "Database initilized.")
}

func Index(w http.ResponseWriter, r *http.Request) {
	t := template.Must(template.ParseFiles(path.Join(StaticFolder, "index.template")))
	data := LoadData()
	err := t.Execute(w, data)
	if err != nil {
		log.Fatalf("template execution: %s", err)
	}
}

func BytesToString(c []byte) string {
	n := -1
	for i, b := range c {
		if b == 0 {
			break
		}
		n = i
	}
	return string(c[:n+1])
}

func sensorValueHandler(c MQTT.Client, m MQTT.Message) {
	resultChannel <- BytesToString(m.Payload()[:])
}

func getNodeSensorValuesJson(nodeid int) string {
	topic := fmt.Sprintf("shc/node/%d/values", nodeid)

	token := client.Subscribe(topic, 0, sensorValueHandler)

	if token.Wait() && token.Error() != nil {
		fmt.Println(token.Error())
		os.Exit(1)
	}

	client.Publish(fmt.Sprintf("shc/node/%d", nodeid), 0, false, "get-values")
	json := <-resultChannel

	if token := client.Unsubscribe(topic); token.Wait() && token.Error() != nil {
		fmt.Println(token.Error())
		os.Exit(1)
	}

	return json
}

func GetNodeValues(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	nodeId, err := strconv.Atoi(vars["nodeid"])
	if err != nil {
		log.Fatalf("improper node id: %s", vars["nodeid"])
	}

	fmt.Fprintf(w, getNodeSensorValuesJson(nodeId))
}
