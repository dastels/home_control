package main

import (
	"fmt"
	MQTT "github.com/eclipse/paho.mqtt.golang"
	"log"
	"os"
	"strconv"
	"time"
)

var client MQTT.Client

func InitMqtt() {
	MQTT.DEBUG = log.New(os.Stdout, "", 0)
	MQTT.ERROR = log.New(os.Stdout, "", 0)
	hostname, _ := os.Hostname()
	server := "tcp://127.0.0.1:1883"
	clientId := hostname + strconv.Itoa(time.Now().Second())

	options := MQTT.NewClientOptions().AddBroker(server).SetClientID(clientId).SetCleanSession(true)
	client = MQTT.NewClient(options)
	if token := client.Connect(); token.Wait() && token.Error() != nil {
		panic(token.Error())
	}
	fmt.Printf("Connected to %s\n", server)
}

func CleanupMqtt() {
	client.Disconnect(250)
	time.Sleep(1 * time.Second)
}
