package main

import (
	"database/sql"
	sqlite3 "github.com/mattn/go-sqlite3"
	"log"
	"os"
	"path"
)

var nodeNames = [10]string{"Bedroom", "En Suite", "Hall", "Entry", "Bathroom", "Office", "Diningroom", "Kitchen", "Pantry", "Livingroom"}

type NodeData struct {
	Id   int
	Name string
}

type Data struct {
	Nodes []NodeData
}

var db *sqlite3.SQLiteDriver

func InitializeDatabase() {
	dbpath := path.Join(StaticFolder, "smarthome.db")
	os.Remove(dbpath)
	db, err := sql.Open("sqlite3", dbpath)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	sqlStmt := `
	create table nodes (node integer not null, name text not null);
	delete from nodes;
	`
	_, err = db.Exec(sqlStmt)
	if err != nil {
		log.Printf("%q: %s\n", err, sqlStmt)
		return
	}
	tx, err := db.Begin()
	if err != nil {
		log.Fatal(err)
	}

	stmt, err := tx.Prepare("insert into nodes(node, name) values(?, ?)")
	if err != nil {
		log.Fatal(err)
	}
	defer stmt.Close()

	for node := 0; node < 10; node++ {
		_, err = stmt.Exec(node+1, nodeNames[node])
		if err != nil {
			log.Fatal(err)
		}
	}

	tx.Commit()
}

func LoadData() Data {
	db, err := sql.Open("sqlite3", path.Join(StaticFolder, "smarthome.db"))
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()
	rows, err := db.Query("select node, name from nodes")
	if err != nil {
		log.Fatal(err)
	}
	d := Data{Nodes: make([]NodeData, 0, 10)}
	defer rows.Close()
	for rows.Next() {
		var node int
		var name string
		err = rows.Scan(&node, &name)
		if err != nil {
			log.Fatal(err)
		} else {
			d.Nodes = append(d.Nodes, NodeData{Id: node, Name: name})
		}
	}
	err = rows.Err()
	if err != nil {
		log.Fatal(err)
	}
	return d
}
