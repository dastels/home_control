// -*- mode: c++ -*-

// The MIT License (MIT)
//
// Copyright (c) 2020 Dave Astels
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#ifndef __CONFIG_H__
#define __CONFIG_H__

#include "logging.h"

#define TRACE 1

// AirLift definitions
#define SPIWIFI       (SPI)  // The SPI port
#define SPIWIFI_SS    (13)   // Chip select pin
#define ESP32_RESETN  (12)   // Reset pin
#define SPIWIFI_ACK   (11)   // a.k.a BUSY or READY pin
#define ESP32_GPIO0   (-1)
//#define HUE_IP "10.215.222.147"

// Hardware
#define NEOPIXEL_PIN (A5)
#define PIR_PIN (10)

// Credentials
#define WIFI_SSID "Shibari"
#define WIFI_PASS "$k7cGE$7=jY67kg#"
#define HUE_USER "nFPC9UctVmE6SvozsdB5j7Pyo5BPIunF5qSlZ8Gg"
#define AIO_USER "dastels"
#define AIO_KEY "796252b4b3484ed3b00665fab73de47c7fb8ea9e"
#define DARKSKY_KEY "12345678900aasdfqwerzxvb"

// logging

#ifndef LOG_LEVEL
#define LOG_LEVEL ("ERROR")
#endif

#ifndef GROUP
#define GROUP (3)
#endif

#ifndef OFF_DELAY
#define OFF_DELAY (180000)
#endif

#endif
