// -*- mode: c++ -*-

// The MIT License (MIT)
//
// Copyright (c) 2020 Dave Astels
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.


#ifndef __HUE_H__
#define __HUE_H__

#include <stdint.h>
#include <WiFiNINA.h>

#include "group.h"

class Hue
{
private:
  WiFiClient *client;
  WiFiSSLClient *ssl_client;
  IPAddress *hue_ip;
  char *hue_ip_string;
public:
  Hue(WiFiClient *a_client, WiFiSSLClient *an_ssl_client);
  bool fetch_hue_ip(void);
  bool connect(void);
  Group **group_names(void);
  uint8_t *lights_for_group(uint8_t group_number);
  bool light_on_p(uint8_t light_number, bool &valid);
  void update_group(uint8_t group_number, bool on_off, uint8_t brightness);
  void update_light(uint8_t light_number, bool on_off, uint8_t brightness);
  void update_all_lights(uint8_t *light_numbers, bool on_off, uint8_t brightness);
};

#endif
