// Serial logging handler (mainly for debug mode)
//
// Copyright (c) 2020 Dave Astels

#include <Arduino.h>
#include "logging.h"
#include "neopixel_handler.h"

NeopixelHandler::NeopixelHandler(Indicator *pixel)
  : LoggingHandler()
  , indicator(pixel)
{
}


void NeopixelHandler::emit(const char *level_name, const char *msg)
{
  uint32_t colour = 0x000000;
  switch  (level_for(level_name)) {
  case LogLevel::ERROR:
    colour = 0xFFA500;
    break;
  case LogLevel::CRITICAL:
    colour = 0xFF0000;
    break;
  default:
    return;
  }

  while (true) {
    indicator->show(colour);
    delay(500);
    indicator->show(0x000000);
    delay(500);
  }
}
