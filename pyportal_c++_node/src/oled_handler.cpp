// OLED logging handler (mainly for debug mode)
//
// Copyright (c) 2020 Dave Astels

#include <Arduino.h>
#include "oled_handler.h"


OledHandler::OledHandler()
  : LoggingHandler()
  , display(new Adafruit_SH110X(64, 128, &Wire))
{
  if ((_initialized = display && display->begin(0x3C, true))) {
    display->clearDisplay();
    display->display();
    display->setRotation(3);
    display->setTextSize(1);
    display->setTextColor(SH110X_WHITE);
  }
}


void OledHandler::emit(const char *level_name, const char *msg)
{
  if (_initialized) {
    display->clearDisplay();
    display->setCursor(0,0);
    display->print(format(level_name, msg));
    display->display();
  }
}
