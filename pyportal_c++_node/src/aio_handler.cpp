// Adafruit IO logging handler (mainly for debug mode)
//
// Copyright (c) 2020 Dave Astels

#include <Arduino.h>
#include "aio_handler.h"


AioHandler::AioHandler(): LoggingHandler()
{
}


void AioHandler::emit(const char *level_name, const char *msg)
{
}
