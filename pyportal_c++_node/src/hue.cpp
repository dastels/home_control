// -*- mode: c++ -*-

// The MIT License (MIT)
//
// Copyright (c) 2020 Dave Astels
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#include <string.h>
#include <ArduinoJson.h>

#include "config.h"
#include "hue.h"
#include "logging.h"

extern Logger *logger;
void log_ip(const char *prefix, IPAddress ip);


DynamicJsonDocument jsonBuffer(8500);

Hue::Hue(WiFiClient *a_client, WiFiSSLClient *an_ssl_client)
  : client(a_client)
  , ssl_client(an_ssl_client)
  , hue_ip(nullptr)
  , hue_ip_string(nullptr)
{
}


bool Hue::fetch_hue_ip()
{
  if (hue_ip == nullptr) {
    logger->debug("Getting HUE IP");
    if (!ssl_client->connectSSL("discovery.meethue.com", 443)) {
      logger->debug("COULD NOT CONNECT");
      return false;
    }
    ssl_client->println("GET / HTTP/1.1");
    ssl_client->println("Host: discovery.meethue.com");
    ssl_client->println("Connection: close");
    if (!ssl_client->println()) {
      ssl_client->stop();
      logger->debug("CONNECTION ERROR");
      return false;
    }

    char status[32] = {0};
    ssl_client->readBytesUntil('\r', status, sizeof(status));
    if (strcmp(status, "HTTP/1.1 200 OK") != 0) {
      ssl_client->stop();
      logger->debug("%d", status);
      return false;
    }

    char endOfHeaders[] = "\r\n\r\n";
    if (!ssl_client->find(endOfHeaders)) {
      ssl_client->stop();
      logger->debug("HEADER ERROR");
      return false;
    }

    auto error = deserializeJson(jsonBuffer, *ssl_client);
    ssl_client->stop();

    if (error) {
      logger->debug("JSON PARSE ERROR");
      return false;
    }
    hue_ip_string = strdup((const char *)jsonBuffer[0]["internalipaddress"]);
    hue_ip = new IPAddress();
    if (!hue_ip->fromString(hue_ip_string)) {
      logger->debug("Error converting IP string");
      return false;
    }
    log_ip("HUE IP:", *hue_ip);
  }
  return true;
}


bool Hue::connect(void)
{
  //log_ip("Connecting to", *hue_ip);

  for (int tries = 0; ; tries++) {
    if (tries == 20) {
      logger->critical("Could not connect");
      return false;
    }
    if (client->connect(*hue_ip, 80)) {
      break;
    }
    logger->debug("Connect failed (%d), retrying %d", client->status(), tries);
  }
  return true;
}


Group **Hue::group_names()
{
  logger->debug("Finding group names");

  if (!connect()) {
    return NULL;
  }

  client->print("GET /api/");
  client->print(HUE_USER);
  client->print("/groups");
  client->println(" HTTP/1.1");

  client->print("Host: ");
  client->println(*hue_ip_string);
  client->println("Connection: close");
  if (!client->println()) {
    client->stop();
    logger->debug("CONNECTION ERROR");
    return NULL;
  }

  char status[32] = {0};
  client->readBytesUntil('\r', status, sizeof(status));
  if (strcmp(status, "HTTP/1.1 200 OK") != 0) {
    client->stop();
    logger->debug("Connection status: %d", status);
    return NULL;
  }

  char endOfHeaders[] = "\r\n\r\n";
  if (!client->find(endOfHeaders)) {
    client->stop();
    logger->debug("HEADER ERROR");
    return NULL;
  }

  auto error = deserializeJson(jsonBuffer, *client);
  client->stop();

  if (error) {
    logger->debug("JSON PARSE ERROR");
    return NULL;
  }
  JsonObject root = jsonBuffer.as<JsonObject>();
  int group_count = 0;
  Group **group_names = new Group*[16];
  for (int i = 0; i < 16; i++) {
    group_names[i] = NULL;
  }
  for (JsonPair kv : root) {
    group_names[group_count++] = new Group(this, (uint8_t)atoi(kv.key().c_str()), strdup(kv.value()["name"].as<char*>()));
    logger->debug("Group %s: %s", kv.key().c_str(), kv.value()["name"].as<char*>());
  }

  return group_names;
}


uint8_t *Hue::lights_for_group(uint8_t group_number)
{
  if (!connect()) {
    return NULL;
  }

  client->print("GET /api/");
  client->print(HUE_USER);
  client->print("/groups/");
  client->print(group_number);
  client->println(" HTTP/1.1");

  client->print("Host: ");
  client->println(hue_ip_string);
  client->println("Connection: close");
  if (!client->println()) {
    client->stop();
    logger->debug("CONNECTION ERROR");
    return NULL;
  }

  char status[32] = {0};
  client->readBytesUntil('\r', status, sizeof(status));
  if (strcmp(status, "HTTP/1.1 200 OK") != 0) {
    client->stop();
    logger->debug("Connection status: %d", status);
    return NULL;
  }

  char endOfHeaders[] = "\r\n\r\n";
  if (!client->find(endOfHeaders)) {
    client->stop();
    logger->debug("HEADER ERROR");
    return NULL;
  }

  auto error = deserializeJson(jsonBuffer, *client);
  client->stop();

  if (error) {
    logger->debug("JSON PARSE ERROR");
    return NULL;
  }

  JsonArray lights = jsonBuffer["lights"];

  uint8_t *light_numbers = new uint8_t[8];
  for (int i = 0; i < 8; i++) {
    light_numbers[i] = 0;
  }
  int light_number = 0;
  for(JsonVariant v : lights) {
    logger->debug("Light: %d", v.as<int>());
    light_numbers[light_number++] = v.as<int>();
  }
  return light_numbers;
}


bool Hue::light_on_p(uint8_t light_number, bool &valid)
{
  logger->debug("Getting light on state for %d", light_number);
  if (!connect()) {
    return false;
  }

  client->print("GET /api/");
  client->print(HUE_USER);
  client->print("/lights/");
  client->print(light_number);
  client->println(" HTTP/1.1");

  client->print("Host: ");
  client->println(hue_ip_string);
  client->println("Connection: close");
  if (!client->println()) {
    client->stop();
    logger->error("CONNECTION ERROR");
    valid = false;
    return false;
  }

  char status[32] = {0};
  client->readBytesUntil('\r', status, sizeof(status));
  if (strcmp(status, "HTTP/1.1 200 OK") != 0) {
    client->stop();
    logger->debug("Connection status: %d", status);
    valid = false;
    return false;
  }

  char endOfHeaders[] = "\r\n\r\n";
  if (!client->find(endOfHeaders)) {
    client->stop();
    logger->error("HEADER ERROR");
    valid = false;
    return false;
  }

  auto error = deserializeJson(jsonBuffer, *client);
  client->stop();

  if (error) {
    logger->error("JSON PARSE ERROR");
    valid = false;
    return false;
  }

  JsonObject state = jsonBuffer["state"];
  logger->debug("  --> %s", state["on"] ? "on" : "off");
  valid = true;
  return state["on"];
}


void Hue::update_group(uint8_t group_number, bool on_off, uint8_t brightness)
{
  if (!connect()) {
    return;
  }

  logger->debug("Turning group %d %s", group_number, on_off ? " on" : " off");

  char content[32];
  sprintf(content, "{\"on\":%s,\"bri\":%d}", on_off ? "true" : "false", brightness);

  client->print("PUT /api/");
  client->print(HUE_USER);
  client->print("/groups/");
  client->print(group_number);
  client->println("/action HTTP/1.1");

  client->print("Host: ");
  client->println(hue_ip_string);

  client->println("Connection: close");

  client->print("Content-Type: ");
  client->println("application/json");
  client->println("User-Agent: FeatherM0Sender");
  client->print("Content-Length: ");
  client->println(strlen(content));
  client->println();

  client->println(content);
  client->stop();
}


void Hue::update_light(uint8_t light_number, bool on_off, uint8_t brightness)
{
  if (!connect()) {
    return;
  }

  logger->debug("Turning light %d %s", light_number, on_off ? " on" : " off");

  logger->debug("PUT /api/%s/lights/%d/state HTTP/1.1", HUE_USER, light_number);

  char content[32];
  sprintf(content, "{\"on\":%s,\"bri\":%d}", on_off ? "true" : "false", brightness);

  client->print("PUT /api/");
  client->print(HUE_USER);
  client->print("/lights/");
  client->print(light_number);
  client->println("/state HTTP/1.1");

  client->print("Host: ");
  client->println(hue_ip_string);

  client->println("Connection: close");

  client->print("Content-Type: ");
  client->println("application/json");
  client->println("User-Agent: FeatherM0Sender");
  client->print("Content-Length: ");
  client->println(strlen(content));
  client->println();

  client->println(content);
  client->stop();
}


void Hue::update_all_lights(uint8_t *light_numbers, bool on_off, uint8_t brightness)
{
  if (light_numbers != NULL) {
    uint8_t num_lights = light_numbers[0];
    for (int i = 0; i < num_lights; i++) {
      update_light(light_numbers[i+1], on_off, brightness);
    }
  }
}
