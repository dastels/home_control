package main

import (
	"flag"
	// "github.com/gorilla/handlers"
	// "github.com/gorilla/mux"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"time"
)

var StaticFolder string

func startUpdateTicker() {
	t := time.NewTicker(time.Second * 60)
	go func() {
		for _ = range t.C {
			for nodeid := 1; nodeid < 11; nodeid++ {
				UpdateNode(nodeid)
			}
		}
	}()
}

func main() {
	InitMqtt()

	var port int
	flag.IntVar(&port, "port", 8500, "port to serve on")
	flag.StringVar(&StaticFolder, "folder", "", "folder containing the static files")
	flag.Parse()

	router := NewRouter(StaticFolder)
	startUpdateTicker()

	fmt.Printf("Serving on %d\n", port)
	http.Handle("/", router)
	err := http.ListenAndServe(":"+strconv.Itoa(port), nil)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}
