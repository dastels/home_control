package main

import (
	"database/sql"
	sqlite3 "github.com/mattn/go-sqlite3"
	"log"
	"os"
	"path"
)

type SensorData struct {
	Node  int
	Name  string
	Value int
}

type NodeData struct {
	Id      int
	Sensors []SensorData
}

type Data struct {
	Nodes []NodeData
}

var sensorNames = [5]string{"power", "light", "temperature", "humidity", "motion"}
var sensorInitialValues = [5]int{1024, 600, 2200, 3500, 0}

var db *sqlite3.SQLiteDriver

func InitializeDatabase() {
	dbpath := path.Join(StaticFolder, "sensors.db")
	os.Remove(dbpath)
	db, err := sql.Open("sqlite3", dbpath)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	sqlStmt := `
	create table sensors (node integer not null, name text not null, value integer not null);
	delete from sensors;
	`
	_, err = db.Exec(sqlStmt)
	if err != nil {
		log.Printf("%q: %s\n", err, sqlStmt)
		return
	}
	tx, err := db.Begin()
	if err != nil {
		log.Fatal(err)
	}

	stmt, err := tx.Prepare("insert into sensors(node, name, value) values(?, ?, ?)")
	if err != nil {
		log.Fatal(err)
	}
	defer stmt.Close()

	for node := 1; node < 11; node++ {
		for sensorIndex := 0; sensorIndex < 5; sensorIndex++ {
			_, err = stmt.Exec(node, sensorNames[sensorIndex], sensorInitialValues[sensorIndex])
			if err != nil {
				log.Fatal(err)
			}
		}
	}

	tx.Commit()
}

func LoadData() Data {
	db, err := sql.Open("sqlite3", path.Join(StaticFolder, "sensors.db"))
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()
	rows, err := db.Query("select node, name, value from sensors")
	if err != nil {
		log.Fatal(err)
	}
	d := Data{Nodes: make([]NodeData, 10, 10)}
	for i := 0; i < 10; i++ {
		d.Nodes[i].Id = i + 1
		d.Nodes[i].Sensors = make([]SensorData, 0, 5)
	}
	defer rows.Close()
	for rows.Next() {
		var node int
		var name string
		var value int
		err = rows.Scan(&node, &name, &value)
		if err != nil {
			log.Fatal(err)
		} else {
			sd := SensorData{Node: node, Name: name, Value: value}
			d.Nodes[node-1].Sensors = append(d.Nodes[node-1].Sensors, sd)
		}
	}
	err = rows.Err()
	if err != nil {
		log.Fatal(err)
	}
	return d
}

func UpdateReading(nodeId int, sensor string, newValue int) {
	db, err := sql.Open("sqlite3", path.Join(StaticFolder, "sensors.db"))
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	tx, err := db.Begin()
	if err != nil {
		log.Fatal(err)
	}

	stmt, err := tx.Prepare("update sensors set value = ? where node = ? and name = ?")
	if err != nil {
		log.Fatal(err)
	}
	defer stmt.Close()
	_, err = stmt.Exec(newValue, nodeId, sensor)
	if err != nil {
		log.Fatal(err)
	}
	tx.Commit()
}

func GetNode(nodeId int) []SensorData {
	sensors := make([]SensorData, 0, 5)
	db, err := sql.Open("sqlite3", path.Join(StaticFolder, "sensors.db"))
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()
	rows, err := db.Query("select name, value from sensors where node = ?", nodeId)
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()
	for rows.Next() {
		var name string
		var value int
		err = rows.Scan(&name, &value)
		if err != nil {
			log.Fatal(err)
		} else {
			sd := SensorData{Node: nodeId, Name: name, Value: value}
			sensors = append(sensors, sd)
		}
	}
	err = rows.Err()
	if err != nil {
		log.Fatal(err)
	}

	return sensors
}

func GetNodeSensor(nodeId int, sensorName string) int {
	db, err := sql.Open("sqlite3", path.Join(StaticFolder, "sensors.db"))
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()
	rows, err := db.Query("select value from sensors where node = ? and name = ?", nodeId, sensorName)
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()
	for rows.Next() {
		var value int
		err = rows.Scan(&value)
		if err != nil {
			log.Fatal(err)
		} else {
			return value
		}
	}
	err = rows.Err()
	if err != nil {
		log.Fatal(err)
	}
	return 0
}
