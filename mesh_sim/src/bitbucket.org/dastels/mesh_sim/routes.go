package main

import (
	"net/http"
)

type Route struct {
	Name        string
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
}

type Routes []Route

var routes = Routes{
	Route{
		"Index",
		"GET",
		"/",
		Index,
	},
	Route{
		"InitDB",
		"GET",
		"/initdb",
		InitDb,
	},
	Route{
		"ChangeValue",
		"POST",
		"/node/{nodeid}/sensor/{sensorname}/{value}",
		ChangeValue,
	},
	Route{
		"SendUpdate",
		"POST",
		"/node/{nodeid}/update",
		SendUpdate,
	},
	Route{
		"StartMotion",
		"POST",
		"/node/{nodeid}/startmotion",
		StartMotion,
	},
	Route{
		"EndMotion",
		"POST",
		"/node/{nodeid}/endmotion",
		EndMotion,
	},
}
