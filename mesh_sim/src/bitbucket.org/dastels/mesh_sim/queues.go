package main

import (
	"fmt"
	MQTT "git.eclipse.org/gitroot/paho/org.eclipse.paho.mqtt.golang.git"
	"log"
	"os"
	"strconv"
	"time"
)

var client *MQTT.Client

var f MQTT.MessageHandler = func(client *MQTT.Client, msg MQTT.Message) {
	fmt.Printf("TOPIC: %s\n", msg.Topic())
	fmt.Printf("MSG: %s\n", msg.Payload())
}

func InitMqtt() {
	MQTT.DEBUG = log.New(os.Stdout, "", 0)
	MQTT.ERROR = log.New(os.Stdout, "", 0)
	hostname, _ := os.Hostname()
	server := "tcp://127.0.0.1:1883"
	clientId := hostname + strconv.Itoa(time.Now().Second())

	options := MQTT.NewClientOptions().AddBroker(server).SetClientID(clientId).SetCleanSession(true)
	client = MQTT.NewClient(options)
	if token := client.Connect(); token.Wait() && token.Error() != nil {
		panic(token.Error())
	}
	fmt.Printf("Connected to %s\n", server)
}

func CleanupMqtt() {
	client.Disconnect(250)
	time.Sleep(1 * time.Second)
}

func mqttTopic(nodeId int, sensorName string) string {
	return fmt.Sprintf("shc/sensors/%d/%s", nodeId, sensorName)
}

func PublishSensorValue(nodeId int, sensorName string, value int) {
	topic := mqttTopic(nodeId, sensorName)
	fmt.Fprintf(os.Stdout, "Publishing %d to %s\n", value, topic)
	token := client.Publish(topic, 0, false, fmt.Sprintf("%d", value))
	token.Wait()
}
