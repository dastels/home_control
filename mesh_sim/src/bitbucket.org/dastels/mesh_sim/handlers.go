package main

import (
	"fmt"
	"github.com/gorilla/mux"
	"html/template"
	"log"
	"net/http"
	"os"
	"path"
	"strconv"
)

func InitDb(w http.ResponseWriter, r *http.Request) {
	InitializeDatabase()
	fmt.Fprintf(w, "Database initilized.")
}

func Index(w http.ResponseWriter, r *http.Request) {
	templatePath := path.Join(StaticFolder, "index.template")
	fmt.Fprintf(os.Stdout, "Getting template: %s\n", templatePath)
	t := template.Must(template.ParseFiles(templatePath))

	data := LoadData()
	err := t.Execute(w, data)
	if err != nil {
		log.Fatalf("template execution: %s", err)
	}
}

func ChangeValue(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	nodeId, err := strconv.Atoi(vars["nodeid"])
	if err != nil {
		log.Fatalf("improper node id: %s", vars["nodeid"])
	}
	sensorName := vars["sensorname"]
	value, err := strconv.Atoi(vars["value"])
	if err != nil {
		log.Fatalf("improper node id: %s", vars["value"])
	}
	UpdateReading(nodeId, sensorName, value)
	fmt.Fprintf(os.Stdout, "Setting %d %s to %d\n", nodeId, sensorName, value)
	fmt.Fprintf(w, "OK")
}

func UpdateNode(nodeId int) {
	fmt.Fprintf(os.Stdout, "Updating %d\n", nodeId)
	nodeData := GetNode(nodeId)
	for _, d := range nodeData {
		fmt.Printf("%d %s %d\n", d.Node, d.Name, d.Value)
		PublishSensorValue(d.Node, d.Name, d.Value)
	}
}

func SendUpdate(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	nodeId, err := strconv.Atoi(vars["nodeid"])
	if err != nil {
		log.Fatalf("improper node id: %s", vars["nodeid"])
	}
	UpdateNode(nodeId)
	fmt.Fprintf(w, "OK")
}

func StartMotion(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	nodeId, err := strconv.Atoi(vars["nodeid"])
	if err != nil {
		log.Fatalf("improper node id: %s", vars["nodeid"])
	}
	lightLevel := GetNodeSensor(nodeId, "light")
	UpdateReading(nodeId, "motion", 1)
	PublishSensorValue(nodeId, "motionstart", lightLevel)
	fmt.Fprintf(os.Stdout, "Start motion %d light is %d\n", nodeId, lightLevel)
	fmt.Fprintf(w, "OK")
}

func EndMotion(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	nodeId, err := strconv.Atoi(vars["nodeid"])
	if err != nil {
		log.Fatalf("improper node id: %s", vars["nodeid"])
	}
	lightLevel := GetNodeSensor(nodeId, "light")
	UpdateReading(nodeId, "motion", 0)
	PublishSensorValue(nodeId, "motionend", lightLevel)
	fmt.Fprintf(os.Stdout, "End motion %d\n", nodeId)
	fmt.Fprintf(w, "OK")
}
