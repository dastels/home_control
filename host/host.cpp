/* -*- mode: C++;-*-
 * HomeBase 
 * by Dave Astels
 * 
 * MQTT support based on https://github.com/silentbicycle/mqtt_demo/blob/master/client.c
 */

#include <sys/types.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>

#include <linux/i2c-dev.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <time.h>

#include <mosquitto.h>

#include "i2c.h"
#include "sensor_names.h"

#ifdef DEBUG
#define LOG(...) do { printf(__VA_ARGS__); } while (0)
#else
#define LOG(...)
#endif

/* How many seconds the broker should wait between sending out
 * keep-alive messages. */
#define KEEPALIVE_SECONDS 60

/* Hostname and port for the MQTT broker. */
#define BROKER_IP "10.215.222.134"
#define BROKER_PORT 1883

struct client_info {
    struct mosquitto *broker;
    pid_t pid;
};
struct client_info info;


typedef struct {
  uint16_t node;
  char type[12];
  uint16_t value;
} sensor_data_t;

struct sensor_payload_t {
  uint16_t node;
  float data;
};



/* Fail with an error message. */
static void die(const char *msg) {
    fprintf(stderr, "%s", msg);
    exit(1);
}

/* Initialize a mosquitto client. */
static struct mosquitto *init(const char *which_client, struct client_info *info)
{
    void *udata = (void *)info;
    size_t buf_sz = 32;
    char buf[buf_sz];
    if (buf_sz < (size_t)snprintf(buf, buf_sz, "%s_sensor_%d", which_client, info->pid)) {
        return NULL;
    }
    /* Create a new mosquitto client, with the name "sensor_client_#{PID}". */
    struct mosquitto *m = mosquitto_new(buf, true, udata);

    return m;
}


/* Callback for successful connection: add subscriptions. */
static void on_connect(struct mosquitto *m, void *udata, int res)
{
  if (res != 0) {             /* success */
    die("connection refused\n");
  }
  
  LOG("-- connected successfully\n");
  mosquitto_subscribe(m, NULL, "shc/sensors/command/#", 0);
}

/* A message was successfully published. */
static void on_publish(struct mosquitto *m, void *udata, int m_id)
{
  //    LOG("-- published successfully\n");
}

// static bool match(const char *topic, const char *key) {
//     return 0 == strncmp(topic, key, strlen(key));
// }


/* Handle a message that just arrived via one of the subscriptions. */
static void on_message(struct mosquitto *m, void *udata, const struct mosquitto_message *msg)
{
  if (msg == NULL) {
    return;
  }

  LOG("-- got message @ %s: (%d, QoS %d, %s) '%s'\n",
      msg->topic, msg->payloadlen, msg->qos, msg->retain ? "R" : "!r",
      msg->payload);
  if (strncmp(msg->topic, "shc/sensors/command/", 20) == 0) {
    int node_id;
    char command_name[8];
    if (sscanf(msg->topic, "shc/sensors/command/%d/%s", &node_id, command_name) != 2) {
      die("Malformed command received\n");
    }
    const char *payload = (const char *)(msg->payload);
    if (strcmp(command_name, "led") == 0) {
      int red, green, blue, millis_off, millis_on;
      if (sscanf(payload, "%d %d %d %d %d", &red, &green, &blue, &millis_off, &millis_on) != 5) {
	      LOG("Malformed LED command");
      } else {
        if (!set_led(node_id, red, green, blue, millis_off, millis_on)) {
          LOG("Error sending LED command");
        }
      }
    } else if (strcmp(command_name, "neopixel") == 0) {
      int red, green, blue;
      if (sscanf(payload, "%d %d %d", &red, &green, &blue) != 3) {
	      LOG("Malformed NEOPIXEL command");
      } else {
        if (!set_neopixel(node_id, red, green, blue)) {
          LOG("Error sending NEOPIXEL command");
        }
      }      
    } else if (strcmp(command_name, "proximity") == 0) {
      int enable;
      if (sscanf(payload, "%d", &enable) != 1) {
	      LOG("Malformed PROXIMITY command");
      } else {
        if (!set_proximity(node_id, enable)) {
          LOG("Error sending PROXIMITY command");
        }
      }
    } else if (strcmp(command_name, "features") == 0) {
      if (!request_features(node_id)) {
        LOG("Error sending FEATURES request");
      }
    } else if (strcmp(command_name, "ext-config") == 0) {
      int directions, pullups;
      if (sscanf(payload, "%d %d", &directions, &pullups) != 2) {
	      LOG("Malformed extension port config command");
      } else {
        if (!config_ext(node_id, directions, pullups)) {
          LOG("Error sending CONFIG EXTENSION PORT command");
        }
      }      
    } else if (strcmp(command_name, "ext-read") == 0) {
      int bit;
      if (sscanf(payload, "%d", &bit) != 1) {
	      LOG("Malformed extension port read command");
      } else {
        if (!read_ext(node_id, bit)) {
          LOG("Error sending READ EXTENSION PORT command");
        }
      }
    } else if (strcmp(command_name, "ext-write") == 0) {
      int bit, value;
      if (sscanf(payload, "%d %d", &bit, &value) != 2) {
	      LOG("Malformed extension port write command");
      } else {
        if (!write_ext(node_id, bit, value)) {
          LOG("Error sending WRITE EXTENSION PORT command");
        }
      }      
    }
  }
}

/* Successful subscription hook. */
static void on_subscribe(struct mosquitto *m, void *udata, int mid, int qos_count, const int *granted_qos)
{
  LOG("-- subscribed successfully\n");
}

/* Register the callbacks that the mosquitto connection will use. */
static bool set_callbacks(struct mosquitto *m)
{
    mosquitto_connect_callback_set(m, on_connect);
    mosquitto_publish_callback_set(m, on_publish);
    mosquitto_subscribe_callback_set(m, on_subscribe);
    mosquitto_message_callback_set(m, on_message);
    return true;
}


/* Connect to the MQTT broker. */
static bool connect_mqtt(struct mosquitto *m)
{
    int res = mosquitto_connect(m, BROKER_IP, BROKER_PORT, KEEPALIVE_SECONDS);
    return res == MOSQ_ERR_SUCCESS;
}


bool config_ext(uint16_t node, uint16_t directions, uint16_t pullups)
{
  i2c_ext_config_command_t ext_command_buffer;
  ext_command_buffer.cmd_tag = EXT_CONFIG_COMMAND;
  ext_command_buffer.node = node;
  ext_command_buffer.directions = directions;
  ext_command_buffer.pullups = pullups;
  LOG("Sending EXT CONFIG command (0x%02x, 0x%02x) to node %u\n", directions, pullups, node);
  return write(base_node_fd, (uint8_t*)(&ext_command_buffer), sizeof(i2c_ext_config_command_t)) == sizeof(i2c_ext_config_command_t);
}


bool read_ext(uint16_t node, uint16_t bit)
{
  i2c_ext_read_command_t ext_command_buffer;
  ext_command_buffer.cmd_tag = EXT_READ_COMMAND;
  ext_command_buffer.node = node;
  ext_command_buffer.bit = bit;
  LOG("Sending EXT READ command to node %u\n", node);
  return write(base_node_fd, (uint8_t*)(&ext_command_buffer), sizeof(i2c_ext_read_command_t)) == sizeof(i2c_ext_read_command_t);
}


bool write_ext(uint16_t node, uint16_t bit, uint16_t value)
{
  i2c_ext_write_command_t ext_command_buffer;
  ext_command_buffer.cmd_tag = EXT_WRITE_COMMAND;
  ext_command_buffer.node = node;
  ext_command_buffer.bit = bit;
  ext_command_buffer.value = value;
  LOG("Sending EXT WRITE command (0x%02x, 0x%02x) to node %u\n", bit, value, node);
  return write(base_node_fd, (uint8_t*)(&ext_command_buffer), sizeof(i2c_ext_write_command_t)) == sizeof(i2c_ext_write_command_t);
}


bool set_led(uint16_t node, uint16_t red, uint16_t green, uint16_t blue, uint16_t millis_off, uint16_t millis_on)
{
  i2c_led_command_t led_command_buffer;
  led_command_buffer.cmd_tag = LED_COMMAND;
  led_command_buffer.node = node;
  led_command_buffer.red = red;
  led_command_buffer.green = green;
  led_command_buffer.blue = blue;
  led_command_buffer.millis_off = millis_off;
  led_command_buffer.millis_on = millis_on;
  LOG("Sending LED command (0x%02x, 0x%02x, 0x%02x) %d/%d to node %u\n", red, green, blue, millis_off, millis_on, node);
  return write(base_node_fd, (uint8_t*)(&led_command_buffer), sizeof(i2c_led_command_t)) == sizeof(i2c_led_command_t);
}


bool set_neopixel(uint16_t node, uint16_t red, uint16_t green, uint16_t blue)
{
  i2c_neopixel_command_t neopixel_command_buffer;
  neopixel_command_buffer.cmd_tag = NEOPIXEL_COMMAND;
  neopixel_command_buffer.node = node;
  neopixel_command_buffer.red = red;
  neopixel_command_buffer.green = green;
  neopixel_command_buffer.blue = blue;
  LOG("Sending NEOPIXEL command (0x%02x, 0x%02x, 0x%02x) to node %u\n", red, green, blue, node);
  return write(base_node_fd, (uint8_t*)(&neopixel_command_buffer), sizeof(i2c_neopixel_command_t)) == sizeof(i2c_neopixel_command_t);
}


bool set_proximity(uint16_t node, uint16_t enable)
{
  i2c_proximity_command_t proximity_command_buffer;
  proximity_command_buffer.cmd_tag = PROXIMITY_COMMAND;
  proximity_command_buffer.node = node;
  proximity_command_buffer.enable = enable;
  LOG("Sending PROXIMITY command (%d) to node %u\n", enable, node);
  return write(base_node_fd, (uint8_t*)(&proximity_command_buffer), sizeof(i2c_proximity_command_t)) == sizeof(i2c_proximity_command_t);
}


bool request_features(uint16_t node)
{
  i2c_feature_command_t feature_command_buffer;
  feature_command_buffer.cmd_tag = FEATURE_COMMAND;
  feature_command_buffer.node = node;
  LOG("Sending FEATURE REQUEST command to node %u\n", node);
  return write(base_node_fd, (uint8_t*)(&feature_command_buffer), sizeof(i2c_feature_command_t)) == sizeof(i2c_feature_command_t);
}


bool is_data_valid(sensor_data_t *sensor_data)
{
  return sensor_data->node > 0 && sensor_data->node < 256;
}


// Publish a piece of sensor data to the appropriate channel.
// data contains:
//   type - topic name, e.g. "motion"
//   from - the node that reported the reading
//   value - the actual value of the reading
// Returns whether publishing succeeded

int send_sensor_data(sensor_data_t *data)
{
  char topic_name[128];
  sprintf(topic_name, "shc/sensors/data/%d/%s", data->node, data->type);
  char payload[8];
  sprintf(payload, "%d", data->value);

  char time_data[16];
  time_t rawtime;
  struct tm * timeinfo;
  time (&rawtime);
  timeinfo = localtime (&rawtime);
  strftime(time_data, 16, "%X", timeinfo);
  LOG("%s - Publishing %s to %s\n", time_data, payload, topic_name); 

  int res = mosquitto_publish(info.broker, NULL, topic_name, strlen(payload), &payload, 0, false);
  return (res != MOSQ_ERR_SUCCESS);
}


/* Loop until it is explicitly halted or the network is lost, then clean up. */
int run_loop()
{
  sensor_data_t sensor_data;
  
  while(1) {
    // ask base for data
    if (read(base_node_fd, (uint8_t*)(&sensor_data), sizeof(sensor_data_t)) == sizeof(sensor_data_t)) {
      // process data if there is any
      if (is_data_valid(&sensor_data)) {
        send_sensor_data(&sensor_data);
      }
    }
    mosquitto_loop(info.broker, 0, 1);
  }
  mosquitto_destroy(info.broker);
  (void)mosquitto_lib_cleanup();
}


void setup_mqtt()
{
  pid_t pid = getpid();
  
  memset(&info, 0, sizeof(info));
  info.pid = pid;
    
  struct mosquitto *broker = init("shc", &info);

  if (broker == NULL) {
    die("mqtt init() failure\n");
  }
  info.broker = broker;
  
  if (!set_callbacks(broker)) {
    die("mqtt set_callbacks() failure\n");
  }
  
  if (!connect_mqtt(broker)) {
    die("mqtt connect() failure\n");
  }
}


void setup_i2c()
{
  base_node_fd = open(dev_name, O_RDWR);
  if (base_node_fd < 0) {
    fprintf(stderr, "I2C: Failed to access %s\n", dev_name);
    exit(1);
  }
  
  printf("I2C: acquiring buss to 0x%x\n", BASE_NODE_ADDRESS);
  
  if (ioctl(base_node_fd, I2C_SLAVE, BASE_NODE_ADDRESS) < 0) {
    fprintf(stderr, "I2C: Failed to acquire bus access/talk to slave 0x%x\n", BASE_NODE_ADDRESS);
    exit(1);
  }
}


int main(int argc, char** argv)
{
  setup_mqtt();
  setup_i2c();

  return run_loop();
}

